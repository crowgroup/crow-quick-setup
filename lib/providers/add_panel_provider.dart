import 'dart:async';
import 'dart:convert';
import 'package:crow_panel_quick_setup/models/token.dart';
import 'package:crow_panel_quick_setup/util/app_url.dart';
import 'package:crow_panel_quick_setup/util/local_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:flutter/foundation.dart';

enum AddStatus {
  NotAdded,
  Added,
  Adding,
  Error,
  NotAuthorizedError,
}

class AddPanelProvider with ChangeNotifier {
  AddStatus _status = AddStatus.NotAdded;

  set status(AddStatus status) {
    _status = status;
    notifyListeners();
  }

  get status => _status;



  reset() {
    _status = AddStatus.NotAdded;
  }
}