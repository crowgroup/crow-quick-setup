import 'dart:async';
import 'dart:convert';
import 'package:crow_panel_quick_setup/models/panel.dart';
import 'package:crow_panel_quick_setup/models/panel_picture.dart';
import 'package:crow_panel_quick_setup/models/panel_pictures_list.dart';
import 'package:crow_panel_quick_setup/models/token.dart';
import 'package:crow_panel_quick_setup/util/app_url.dart';
import 'package:crow_panel_quick_setup/util/local_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:flutter/foundation.dart';

enum Status {
  NotLoaded,
  Loaded,
  Loading,
  Error,
  NotAuthorizedError,
}

class PanelPicturesListProvider with ChangeNotifier {
  int _pageSize = 10;
  int _page = 1;
  final Panel panel;
  Status _status = Status.Loading;

  PanelPicturesListProvider(this.panel);

  PanelPicturesList panelPicturesList = PanelPicturesList();

  set page(int page) {
    if (page < _page && panelPicturesList.previous == null || page > _page && panelPicturesList.next == null || _status != Status.Loaded) {
      return;
    }
    _page = page;
    status = Status.Loading;
    getPanelPicturesList();
  }

  get page => _page;

  set status(Status status) {
    _status = status;
    notifyListeners();
  }

  get status => _status;

  set pageSize(int pageSize) {
    _pageSize = pageSize;
    status = Status.Loading;
    getPanelPicturesList();
  }

  get pageSize => _pageSize;

  Future<void> getPanelPicturesList() async {
    Token token = await LocalStorage().getToken();

    var queryParameters = {
      'page': '$_page',
      'page_size': '$_pageSize',
    };
    Uri uri = Uri.https(AppUrls.baseURL, '/panels/${panel.mac}/pictures/', queryParameters);
    Response response = await get(
      uri,
      headers: {
        'Authorization': 'Bearer ${token.accessToken}',
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> panelsData = json.decode(response.body);
      panelPicturesList = PanelPicturesList.fromJson(panelsData);
      status = Status.Loaded;
    } else if (response.statusCode == 401) {
      status = Status.NotAuthorizedError;
     } else {
      status = Status.Error;
     }
  }
}