import 'dart:async';
import 'dart:convert';
import 'package:crow_panel_quick_setup/models/panel_configuration.dart';
import 'package:crow_panel_quick_setup/models/token.dart';
import 'package:crow_panel_quick_setup/util/app_url.dart';
import 'package:crow_panel_quick_setup/util/local_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:flutter/foundation.dart';

enum PanelsConfigurationStatus {
  NotLoaded,
  Loaded,
  Loading,
  Error,
  NotAuthorizedError,
  InstallerCodeError,
}

class PanelsConfigurationProvider with ChangeNotifier {
  PanelsConfigurationStatus _status = PanelsConfigurationStatus.Loading;
  String _installerCode = '';
  bool listView = true;
  PanelConfiguration _panelConfiguration = PanelConfiguration();

  PanelConfiguration get panelConfiguration => _panelConfiguration;

  set status(PanelsConfigurationStatus status) {
    _status = status;
    notifyListeners();
  }

  get status => _status;

  set panelConfiguration(PanelConfiguration panelConfiguration) {
    _panelConfiguration = panelConfiguration;
    notifyListeners();
  }

  void toggleView() {
    listView = !listView;
    notifyListeners();
  }

  void removeAllDevices() {
    _panelConfiguration.removeAllDevices(_panelConfiguration.rawConfig);
    savePanelConfiguration();
  }

  Future<void> getPanelConfiguration(int panelId, String installerCode) async {
    Token token = await LocalStorage().getToken();
    Uri uri = Uri.https(AppUrls.baseURL, "/panels/$panelId/config_export_import/");

    Response response = await get(
      uri,
      headers: {
        'Authorization': 'Bearer ${token.accessToken}',
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-Crow-CP-installer": "$installerCode"
      },
    );

    if (response.statusCode == 200) {
      _installerCode = installerCode;
      final Map<String, dynamic> configurationData = json.decode(response.body);
      _panelConfiguration = PanelConfiguration.fromJson(panelId, configurationData);
      status = PanelsConfigurationStatus.Loaded;
    } else if (response.statusCode == 401) {
      status = PanelsConfigurationStatus.NotAuthorizedError;
    } else if (response.statusCode == 405) {
      status = PanelsConfigurationStatus.InstallerCodeError;
    } else {
      status = PanelsConfigurationStatus.Error;
    }
  }

  Future<bool> enterConfigMode(int panelId) async {
    Token token = await LocalStorage().getToken();
    Uri uri = Uri.https(AppUrls.baseURL, "/panels/$panelId/config_mode/");
    Response response = await post(
        uri,
        headers: {
          'Authorization': 'Bearer ${token.accessToken}',
          "Accept": "application/json",
          "Content-Type": "application/json",
          "X-Crow-CP-installer": "$_installerCode",
        }
    );
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }

  Future<bool> exitConfigMode(int panelId) async {
    Token token = await LocalStorage().getToken();
    Uri uri = Uri.https(AppUrls.baseURL, "/panels/$panelId/config_mode/1/");
    Response response = await delete(
        uri,
        headers: {
          'Authorization': 'Bearer ${token.accessToken}',
          "Accept": "application/json",
          "Content-Type": "application/json"
        }
    );
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }

  Future<void> savePanelConfiguration() async {
    status = PanelsConfigurationStatus.Loading;
    int panelId = _panelConfiguration.panelId;
    if (!await enterConfigMode(panelId)) {
      status = PanelsConfigurationStatus.Error;
    }
    Token token = await LocalStorage().getToken();
    Uri uri = Uri.https(AppUrls.baseURL, "/panels/$panelId/config_export_import/");

    // debugPrint(json.encode(_panelConfiguration.rawConfig));

    Response response = await put(
      uri,
      headers: {
        'Authorization': 'Bearer ${token.accessToken}',
        "Accept": "application/json",
        "Content-Type": "application/json;charset=UTF-8"
      },
      body: jsonEncode(_panelConfiguration.rawConfig)
    );
    await exitConfigMode(panelId);

    if (response.statusCode == 200) {
      final Map<String, dynamic> configurationData = json.decode(response.body);
      _panelConfiguration = PanelConfiguration.fromJson(panelId, configurationData);
      status = PanelsConfigurationStatus.Loaded;
    } else if (response.statusCode == 401) {
      status = PanelsConfigurationStatus.NotAuthorizedError;
    } else if (response.statusCode == 405) {
      status = PanelsConfigurationStatus.InstallerCodeError;
    } else {
      status = PanelsConfigurationStatus.Error;
    }
  }

  reset() {
    _status = PanelsConfigurationStatus.Loading;
    _panelConfiguration = PanelConfiguration();
    notifyListeners();
  }
}