import 'dart:async';
import 'dart:convert';
import 'package:crow_panel_quick_setup/providers/panel_configuration_provider.dart';
import 'package:crow_panel_quick_setup/providers/panels_list_provider.dart';
import 'package:crow_panel_quick_setup/providers/panels_list_search_provider.dart';
import 'package:crow_panel_quick_setup/util/local_storage.dart';
import 'package:provider/provider.dart';
import 'package:crow_panel_quick_setup/models/token.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:crow_panel_quick_setup/util/app_url.dart';


enum AuthStatus {
  NotLoggedIn,
  LoggedIn,
  Authenticating,
  LoggedOut
}

class AuthProvider with ChangeNotifier {

  AuthStatus _loggedInStatus = AuthStatus.NotLoggedIn;
  AuthStatus get loggedInStatus => _loggedInStatus;


  Future<Map<String, dynamic>> login(String email, String password, bool rememberMe) async {
    var result;

    _loggedInStatus = AuthStatus.Authenticating;
    notifyListeners();

    Response response = await post(
      AppUrls.login,
      body: 'grant_type=password&password=$password&username=$email',
      headers: {
        'authorization': 'Basic bVpMbmZNZDRNSGlzTE16STFhY01SMVlIZUNwZlNUUG82WFdGYkU0Yjo5bWpzQTJ5cXhhZFBlZkQ4QkFYa3dpbkEyckNZSFhzZWlxeUhmbmV0T3RVY3lNc0p3VmVGVVRXYmFvcFB1anh3Mnp6S1hDUDk0bGdnaTRkcDRBaDRkOFEwNGVmT01JS2cyU0hFdU9TVXd0T1NWMWh6WG1tdE5tRkRwZjE3cnE0UQ==',
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> tokenData = json.decode(response.body);
      Token authToken = Token.fromJson(tokenData);
      if (rememberMe) {
        await LocalStorage().saveCredentials(email, password);
      } else {
        await LocalStorage().removeCredentials();
      }

      _loggedInStatus = AuthStatus.LoggedIn;
      notifyListeners();

      result = {'status': true, 'message': 'Successful', 'token': authToken};
    } else {
      _loggedInStatus = AuthStatus.NotLoggedIn;
      notifyListeners();
      result = {
        'status': false,
        'message': json.decode(response.body)['error']
      };
    }
    return result;
  }

  Future<void> logout(BuildContext context) async {
    await LocalStorage().removeToken();
    // Provider.of<PanelsConfigurationProvider>(context, listen: false).reset();
    _loggedInStatus = AuthStatus.LoggedOut;
    notifyListeners();
    Navigator.pushReplacementNamed(context, '/login');
  }
}