import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class PanelsListSearchProvider with ChangeNotifier {
  String _search = '';
  String _panelStatus = '';
  bool _isSearchFormVisible = false;

  set isSearchFormVisible(bool isSearchFormVisible) {
    _isSearchFormVisible = isSearchFormVisible;
    notifyListeners();
  }

  get isSearchFormVisible => _isSearchFormVisible;

  set search(String search) {
    _search = search;
  }

  get search => _search;

  set panelStatus(String panelStatus) {
    _panelStatus = panelStatus;
    notifyListeners();
  }

  get panelStatus => _panelStatus;

  toggleSearch() {
    isSearchFormVisible = !isSearchFormVisible;
  }

  reset() {
    _search = '';
    _panelStatus = '';
    _isSearchFormVisible = false;
    notifyListeners();
  }
}