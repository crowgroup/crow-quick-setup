import 'dart:async';
import 'dart:convert';
import 'package:crow_panel_quick_setup/models/panels_list.dart';
import 'package:crow_panel_quick_setup/models/token.dart';
import 'package:crow_panel_quick_setup/util/app_url.dart';
import 'package:crow_panel_quick_setup/util/local_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:flutter/foundation.dart';

enum Status {
  NotLoaded,
  Loaded,
  Loading,
  Error,
  NotAuthorizedError,
}

class PanelsListProvider with ChangeNotifier {
  int _enriched = 1;
  int _pageSize = 10;
  int _page = 1;
  String search = '';
  String _panelStatus = '';
  Status _status = Status.Loading;

  PanelsList panelsList = PanelsList();

  set page(int page) {
    if (page < _page && panelsList.previous == null || page > _page && panelsList.next == null || _status != Status.Loaded) {
      return;
    }
    _page = page;
    status = Status.Loading;
    getPanelsList();
  }

  get page => _page;

  set status(Status status) {
    _status = status;
    notifyListeners();
  }

  get status => _status;

  set pageSize(int pageSize) {
    _pageSize = pageSize;
    status = Status.Loading;
    getPanelsList();
  }

  get pageSize => _pageSize;

  set panelStatus(String panelStatus) {
    _panelStatus = panelStatus;
  }

  Future<void> getPanelsList() async {
    Token token = await LocalStorage().getToken();

    var queryParameters = {
      'enriched': '$_enriched',
      'page': '$_page',
      'page_size': '$_pageSize',
      'search': '$search',
      'panel_status': '$_panelStatus',
    };
    Uri uri = Uri.https(AppUrls.baseURL, AppUrls.panels, queryParameters);

    Response response = await get(
      uri,
      headers: {
        'Authorization': 'Bearer ${token.accessToken}',
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> panelsData = json.decode(response.body);
      panelsList = PanelsList.fromJson(panelsData);
      status = Status.Loaded;
    } else if (response.statusCode == 401) {
      status = Status.NotAuthorizedError;
     } else {
      status = Status.Error;
     }
  }

  reset() {
    _enriched = 1;
    _pageSize = 5;
    _page = 1;
    search = '';
    _panelStatus = '';
    panelsList = PanelsList();
    _status = Status.Loading;
  }
}