import 'package:flutter/material.dart';

class NewDevice extends StatefulWidget {
  @override
  _NewDeviceState createState() => _NewDeviceState();
}

class _NewDeviceState extends State<NewDevice> {
  @override
  Widget build(BuildContext context) {
    List<dynamic> devices = [
      {
        'name': 'Control panel',
        'imageUrl': 'images/shepherd_panel_center.jpg',
        'added': false,
        'status': 'active'
      },
      {
        'name': 'Icon Keypad',
        'imageUrl': 'images/sh_kp_center.jpg',
        'added': false
      },
      {
        'name': 'Magnet Contact',
        'imageUrl': 'images/sh-mag-center.jpg',
        'added': false,
        'status': 'warning'
      },
      {
        'name': 'Mirror Optics PIR',
        'imageUrl': 'images/sh-pir_in-center.jpg',
        'added': false,
        'status': 'not-connected'
      },
      {
        'name': 'Indoor Sounder',
        'imageUrl': 'images/sh-sirint-center.jpg',
        'added': false
      },
      {
        'name': 'Smart Plug',
        'imageUrl': 'images/sh-acp-center.jpg',
        'added': false
      },
      {
        'name': 'Indoor Sounder',
        'imageUrl': 'images/sh-sirint-center.jpg',
        'added': false
      },
      {
        'name': 'Smart Plug',
        'imageUrl': 'images/sh-acp-center.jpg',
        'added': false
      }
    ];

    return Scaffold(
      // appBar: AppBar(
      //   title: Text("NewDevice PAGE"),
      //   elevation: 0.1,
      // ),
      body:
        CustomScrollView(
          slivers: <Widget>[
            const SliverAppBar(
              pinned: false,
              expandedHeight: 50.0,
              flexibleSpace: FlexibleSpaceBar(
                title: Text('Select the device you want to add'),
                titlePadding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 16.0)
              ),
            ),
            SliverGrid(
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 200.0,
                mainAxisSpacing: 2.0,
                crossAxisSpacing: 2.0,
                childAspectRatio: 1.0,
              ),
              delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  return deviceCard(devices[index]);
                },
                childCount: devices.length,
              ),
            ),
            // SliverFixedExtentList(
            //   itemExtent: 50.0,
            //   delegate: SliverChildBuilderDelegate(
            //         (BuildContext context, int index) {
            //       return Container(
            //         alignment: Alignment.center,
            //         color: Colors.lightBlue[100 * (index % 9)],
            //         child: Text('List Item $index'),
            //       );
            //     },
            //   ),
            // ),
          ],
        ),
      );

        // GridView.count(
        //   // Create a grid with 2 columns. If you change the scrollDirection to
        //   // horizontal, this produces 2 rows.
        //     crossAxisCount: 3,
        //     // Generate 100 widgets that display their index in the List.
        //     children: [
        //       deviceCard('images/shepherd_panel_center.jpg', true),
        //       deviceCard('images/sh_kp_center.jpg', false),
        //       deviceCard('images/sh-mag-center.jpg', false),
        //       deviceCard('images/sh-pir_in-center.jpg', true),
        //       deviceCard('images/sh-sirint-center.jpg', false),
        //       deviceCard('images/sh-acp-center.jpg', false),
        //
        //       // RaisedButton(onPressed: (){
        //       //   auth.logout(context);
        //       // }),
        //     ]
        //
        //     // List.generate(100, (index) {
        //     //   return Center(
        //     //     child: Text(
        //     //       'Item $index',
        //     //       style: Theme.of(context).textTheme.headline5,
        //     //     ),
        //     //   );
        //     // })
        // ),
      // Column(
      //   children: [
      //     SizedBox(height: 100,),
      //     Center(child: Text("${token?.accessToken}")),
      //     SizedBox(height: 100),
      //     RaisedButton(onPressed: (){
      //       auth.logout(context);
      //     }, child: Text("Logout"), color: Colors.lightBlueAccent,)
      //   ],
      // ),
    // );
  }

  Widget deviceCard(dynamic device) {
    return Card(
      child: new Container(
        child: Padding(
          padding: EdgeInsets.all(4.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                device['name'],
                style: TextStyle(
                    color: Colors.white
                ),
              ),
              // Row(
              //   crossAxisAlignment: CrossAxisAlignment.center,
              //   mainAxisAlignment: MainAxisAlignment.start,
              //   children: [
              //     IconTheme(
              //       data: IconThemeData(
              //         color: Colors.green),
              //         child: Icon(Icons.cloud_off),
              //     ),
              //     IconTheme(
              //       data: IconThemeData(
              //         color: Colors.green),
              //         child: Icon(Icons.check_circle_sharp),
              //     ),
              //     IconTheme(
              //       data: IconThemeData(
              //         color: Colors.green),
              //         child: Icon(Icons.battery_alert),
              //     ),
              //     IconTheme(
              //       data: IconThemeData(
              //         color: Colors.green),
              //         child: Icon(Icons.circle),
              //     ),
              //     IconTheme(
              //       data: IconThemeData(
              //         color: Colors.green),
              //         child: Icon(Icons.construction),
              //     ),
              //   ],
              // )
            ],
          ),
        ),
        decoration: BoxDecoration(
          color: const Color(0xff7c94b6),
          image: DecorationImage(
            fit: BoxFit.fill,
            // colorFilter: device['added'] ? null : ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
            image: AssetImage(
              device['imageUrl'],
            ),
          ),
        ),
      ),
    );
    // return Card(
    //   semanticContainer: true,
    //   clipBehavior: Clip.antiAliasWithSaveLayer,
    //   child: Image.asset(
    //     imageUrl,
    //     fit: BoxFit.fill,
    //   ),
    //   shape: RoundedRectangleBorder(
    //     borderRadius: BorderRadius.circular(10.0),
    //   ),
    //   elevation: 5,
    //   margin: EdgeInsets.all(2),
    // );
    // return Card(
    //   child: Column(
    //     mainAxisSize: MainAxisSize.min,
    //     children: <Widget>[
    //       Image(image: AssetImage(imageUrl)),
    //     ]
    //   )
    // );
  }
}
