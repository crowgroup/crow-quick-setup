import 'package:crow_panel_quick_setup/providers/auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class CommonDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AuthProvider auth = Provider.of<AuthProvider>(context);
    return Drawer(
      child: ListView(
        // padding: EdgeInsets.zero,
        padding: EdgeInsets.only(top: 20.0),
        children: <Widget>[
          // DrawerHeader(
          //   child: Text('Drawer Header'),
          //   decoration: BoxDecoration(
          //     color: Colors.blue,
          //   ),
          // ),
          ListTile(
            title: Text('Logout'),
            onTap: () {
              Navigator.pop(context);
              auth.logout(context);
            },
          ),
        ],
      ),
    );
  }
}
