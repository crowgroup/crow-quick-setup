import 'package:crow_panel_quick_setup/providers/panels_list_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PanelsListPagination extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsetsDirectional.fromSTEB(16, 2, 16, 32),
      child: Consumer<PanelsListProvider>(
        builder: (context, panelsListProvider, child) =>
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(panelsListProvider.panelsList.getPaginationText(panelsListProvider.page, panelsListProvider.pageSize)),
              Row(children: [
                Text('Per page: '),
                DropdownButton<int>(
                  value: panelsListProvider.pageSize,
                  icon: Icon(Icons.arrow_drop_down),
                  iconSize: 24,
                  elevation: 16,
                  style: TextStyle(color: Colors.black),
                  underline: Container(
                    height: 2,
                    color: Colors.grey,
                  ),
                  onChanged: (int newValue) {
                    panelsListProvider.page = 1;
                    panelsListProvider.pageSize = newValue;
                  },
                  items: <int>[1, 5, 10, 20, 50, 100]
                      .map<DropdownMenuItem<int>>((int value) {
                    return DropdownMenuItem<int>(
                      value: value,
                      child: Text('$value'),
                    );
                  }).toList(),
                ),
                IconButton(
                  icon: Icon(Icons.keyboard_arrow_left),
                  onPressed: panelsListProvider.panelsList.previous != null ? () {
                    panelsListProvider.page--;
                  } : null,
                ),
                IconButton(
                  icon: Icon(Icons.keyboard_arrow_right),
                  onPressed: panelsListProvider.panelsList.next != null ? () {
                    panelsListProvider.page++;
                  } : null,
                ),
              ]
              )
            ]
        )
      )
    );
  }
}