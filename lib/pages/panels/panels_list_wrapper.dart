import 'package:crow_panel_quick_setup/pages/panels/panel_list.dart';
import 'package:crow_panel_quick_setup/providers/panels_list_provider.dart';
import 'package:crow_panel_quick_setup/providers/panels_list_search_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PanelsListWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => PanelsListProvider()),
          ChangeNotifierProvider(create: (_) => PanelsListSearchProvider()),
        ],
        child: PanelsList()
    );
  }
}