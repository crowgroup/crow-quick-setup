import 'package:crow_panel_quick_setup/models/panel.dart';
import 'package:crow_panel_quick_setup/pages/panels/panel_pictures/panel_pictures.dart';
import 'package:crow_panel_quick_setup/providers/panel_pictures_list_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PanelPicturesListWrapper extends StatelessWidget {
  PanelPicturesListWrapper(this.panel);
  final Panel panel;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => PanelPicturesListProvider(this.panel)),
        ],
        child: PanelPicturesList()
    );
  }
}