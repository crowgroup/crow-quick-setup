import 'dart:convert';

import 'package:crow_panel_quick_setup/models/panel_picture.dart';
import 'package:crow_panel_quick_setup/models/token.dart';
import 'package:crow_panel_quick_setup/util/app_url.dart';
import 'package:crow_panel_quick_setup/util/local_storage.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:http/http.dart';

Future<void> showPictureSeries(BuildContext context, PanelPicture panelPicture) async {
  await showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text(panelPicture.zoneName),
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: ConfigWidget(panelPicture),
            ),
          ],
        );
      }
  );
}

class ConfigWidget extends StatefulWidget {
  ConfigWidget(this.panelPicture) : super();

  PanelPicture panelPicture;

  @override
  _ConfigWidgetState createState() => _ConfigWidgetState();
}

class _ConfigWidgetState extends State<ConfigWidget> {
  List<PanelPicture> panelPictures = [];
  bool isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPictures();
  }

  getPictures() async {
    panelPictures = await getPanelPictureSeries(widget.panelPicture.key);
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return !isLoading ? Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CarouselSlider(
            options: CarouselOptions(
              height: MediaQuery.of(context).size.height * 0.35,
              enableInfiniteScroll: false
            ),
            items: panelPictures.map((panelPicture) {
              return Builder(
                builder: (BuildContext context) {
                  return Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.network(panelPicture.url),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('ID: ${panelPicture.id}'),
                              Text('${panelPicture.picIndex} / ${panelPicture.picTotal}'),
                            ]
                          ),
                        ],
                      )
                  );
                },
              );
            }).toList(),
          )
        ]
    ) : Center(child: CircularProgressIndicator());
  }
}


Future<List<PanelPicture>> getPanelPictureSeries(String pictureId) async {
  Token token = await LocalStorage().getToken();

  String uri = "https://${AppUrls.baseURL}/pictures_series/${Uri.encodeComponent(pictureId)}/";

  Response response = await get(
    uri,
    headers: {
      'Authorization': 'Bearer ${token.accessToken}',
      "Accept": "application/json",
      "Content-Type": "application/json"
    },
  );

  if (response.statusCode == 200) {
    final List<dynamic> panelsData = json.decode(response.body);
    return List.from(panelsData.map((dynamic model) => PanelPicture.fromJson(model)));
  }
  return [];
}
