import 'package:crow_panel_quick_setup/models/panel.dart';
import 'package:crow_panel_quick_setup/models/panel_picture.dart';
import 'package:crow_panel_quick_setup/pages/panels/panel_pictures/panel_pictures_list_pagination.dart';
import 'package:crow_panel_quick_setup/pages/panels/panel_pictures/picture_series/picture_series.dart';
import 'package:crow_panel_quick_setup/providers/auth.dart';
import 'package:crow_panel_quick_setup/providers/panel_pictures_list_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class PanelPicturesList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Panel panel = Provider.of<PanelPicturesListProvider>(context, listen: false).panel;
    Provider.of<PanelPicturesListProvider>(context, listen: false).getPanelPicturesList();
    return Scaffold(
        appBar: AppBar(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Panel ${panel.prettyMac}'),
              Text('Pictures'),
            ]
          ),
          actions: <Widget>[
            // IconButton(
            //   icon: Icon(Icons.search),
            //   onPressed: () {
            //
            //   },
            // ),
            // IconButton(
            //   icon: Icon(Icons.add),
            //   onPressed: () {
            //
            //   },
            // ),
          ],
        ),

        body:
        Consumer<PanelPicturesListProvider>(builder: (context, panelPicturesListProvider, child) =>
            getView(panelPicturesListProvider, context)
        )
    );
  }

  getView(PanelPicturesListProvider panelPicturesListProvider, BuildContext context) {
    switch(panelPicturesListProvider.status) {
      case Status.Loading: return Center(child: CircularProgressIndicator()); break;
      case Status.Error: return Center(child: Text('Unknown Error')); break;
      case Status.NotAuthorizedError:
        SchedulerBinding.instance.addPostFrameCallback((_) {
          Provider.of<AuthProvider>(context, listen: false).logout(context);
        });
        return Center(child: Text('Not Authorized')); break;
      case Status.Loaded: return Stack(children: [
        panelPicturesListProvider.panelPicturesList.results.length > 0 ? Column(children: [
          Expanded(child:
          GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > 0) {
                panelPicturesListProvider.page--;
              } else if (details.delta.dx < 0) {
                panelPicturesListProvider.page++;
              }
            },
            child: RefreshIndicator(
                onRefresh: () {
                  return panelPicturesListProvider.getPanelPicturesList();
                },
                child: GridView.count(
                  childAspectRatio: 640/480,
                  crossAxisCount: 2,
                  crossAxisSpacing: 2,
                  mainAxisSpacing: 2,
                  padding: EdgeInsets.all(1.0),
                  children: List.generate(
                    panelPicturesListProvider.panelPicturesList.results.length,
                    (index) {
                      PanelPicture panelPicture = panelPicturesListProvider.panelPicturesList.results[index];
                      return
                        InkWell(
                          splashColor: Theme.of(context).accentColor.withAlpha(30),
                          onTap: () {
                            showPictureSeries(context, panelPicture);
                          },
                        child:
                          Stack(
                            children: [
                              Image.network(panelPicture.url),
                              Container(
                                height: 18,
                                color: Colors.white.withOpacity(0.5),
                                child:
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 4.0),
                                    child:
                                      Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text('ID: ${panelPicture.id}'),
                                              Text('${panelPicture.picIndex} / ${panelPicture.picTotal}'),
                                            ]
                                        ),
                                        // Text(panelPicturesListProvider.panelPicturesList.results[index].zoneName),
                                        // Text('${DateFormat('yyyy-MM-dd kk:mm:ss').format(DateTime.parse(panelPicture.created).toLocal())}')
                                      ],
                                    )
                                ),
                              )
                            ]
                        )
                      );
                    },
                  ),
                ),
            ),
          )
          ),
          PanelPicturesListPagination()
        ],
        ) : Center(child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('No pictures'),
              // MaterialButton(
              //   onPressed: () {
              //
              //   },
              //   child: SizedBox(
              //     width: double.infinity,
              //     child: Text(
              //       "Add new panel",
              //       textAlign: TextAlign.center,
              //     ),
              //   ),
              // ),
            ]
        )
        ),
      ]);
      break;
      default: return Column();
    }
  }
}
