import 'package:crow_panel_quick_setup/providers/panel_pictures_list_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PanelPicturesListPagination extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.fromSTEB(16, 2, 16, 32),
      child: Consumer<PanelPicturesListProvider>(
        builder: (context, panelPicturesListProvider, child) =>
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(panelPicturesListProvider.panelPicturesList.getPaginationText(panelPicturesListProvider.page, panelPicturesListProvider.pageSize)),
              Row(children: [
                Text('Per page: '),
                DropdownButton<int>(
                  value: panelPicturesListProvider.pageSize,
                  icon: Icon(Icons.arrow_drop_down),
                  iconSize: 24,
                  elevation: 16,
                  style: TextStyle(color: Colors.black),
                  underline: Container(
                    height: 2,
                    color: Colors.grey,
                  ),
                  onChanged: (int newValue) {
                    panelPicturesListProvider.page = 1;
                    panelPicturesListProvider.pageSize = newValue;
                  },
                  items: <int>[1, 5, 10, 20, 50, 100]
                      .map<DropdownMenuItem<int>>((int value) {
                    return DropdownMenuItem<int>(
                      value: value,
                      child: Text('$value'),
                    );
                  }).toList(),
                ),
                IconButton(
                  icon: Icon(Icons.keyboard_arrow_left),
                  onPressed: panelPicturesListProvider.panelPicturesList.previous != null ? () {
                    panelPicturesListProvider.page--;
                  } : null,
                ),
                IconButton(
                  icon: Icon(Icons.keyboard_arrow_right),
                  onPressed: panelPicturesListProvider.panelPicturesList.next != null ? () {
                    panelPicturesListProvider.page++;
                  } : null,
                ),
              ]
              )
            ]
        )
      )
    );
  }
}