import 'package:crow_panel_quick_setup/pages/common_drawer.dart';
import 'package:crow_panel_quick_setup/pages/panels/panels_list_item/panel_list_item.dart';
import 'package:crow_panel_quick_setup/pages/panels/panels_list_pagination.dart';
import 'package:crow_panel_quick_setup/pages/panels/panels_list_search.dart';
import 'package:crow_panel_quick_setup/providers/auth.dart';
import 'package:crow_panel_quick_setup/providers/panels_list_provider.dart';
import 'package:crow_panel_quick_setup/providers/panels_list_search_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:provider/provider.dart';

class PanelsList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Provider.of<PanelsListProvider>(context, listen: false).getPanelsList();
    PanelsListSearchProvider panelsListSearchProvider = Provider.of<PanelsListSearchProvider>(context, listen: false);
    return Scaffold(
        appBar: AppBar(
          title: Text('Control panels'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                panelsListSearchProvider.toggleSearch();
              },
            ),
            IconButton(
              icon: Icon(Icons.qr_code),
              onPressed: () {
                findByMac(context);
              },
            ),
            // IconButton(
            //   icon: Icon(Icons.add),
            //   onPressed: () {
            //     addPanel(context);
            //   },
            // ),
          ],
        ),
        drawer: CommonDrawer(),
        body:
        Consumer<PanelsListProvider>(builder: (context, panelsListProvider, child) =>
            getView(panelsListProvider, context)
        )
    );
  }

  getView(PanelsListProvider panelsListProvider, BuildContext context) {
    switch(panelsListProvider.status) {
      case Status.Loading: return Center(child: CircularProgressIndicator()); break;
      case Status.Error: return Center(child: Text('Unknown Error')); break;
      case Status.NotAuthorizedError:
        SchedulerBinding.instance.addPostFrameCallback((_) {
          Provider.of<AuthProvider>(context, listen: false).logout(context);
        });
        return Center(child: Text('Not Authorized')); break;
      case Status.Loaded: return Stack(children: [
        panelsListProvider.panelsList.results.length > 0 ? Column(children: [
          Expanded(child:
          GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > 0) {
                panelsListProvider.page--;
              } else if (details.delta.dx < 0) {
                panelsListProvider.page++;
              }
            },
            child: RefreshIndicator(
                onRefresh: () {
                  return panelsListProvider.getPanelsList();
                },
                child: ListView.builder(
                  itemCount: panelsListProvider.panelsList.results.length,
                  itemBuilder: (BuildContext context, int index) {
                    return PanelListItem(panel: panelsListProvider.panelsList.results[index]);
                  }
                )
            ),
          )
          ),
          PanelsListPagination()
        ],
        ) : Center(child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('No relevant panels'),
              MaterialButton(
                onPressed: () {
                  addPanel(context);
                },
                child: SizedBox(
                  width: double.infinity,
                  child: Text(
                    "Add new panel",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ]
        )
        ),
        PanelsListSearch(),
      ]);
      break;
      default: return Column();
    }
  }

  addPanel(BuildContext context) async {
    Navigator.pushNamed(context, '/addPanel');
  }

  Future<void> findByMac(BuildContext context) async {
    String mac = await scanQR();
    PanelsListProvider panelsListProvider = Provider.of<PanelsListProvider>(context, listen: false);
    PanelsListSearchProvider panelsListSearchProvider = Provider.of<PanelsListSearchProvider>(context, listen: false);
    panelsListSearchProvider.isSearchFormVisible = false;
    panelsListProvider.search = panelsListSearchProvider.search = mac;
    panelsListProvider.page = 1;
  }

  Future<String> scanQR() async {
    String barcodeScanResult = '';
    try {
      barcodeScanResult = await FlutterBarcodeScanner.scanBarcode("#FFFDDD", "Cancel", true, ScanMode.QR);
      barcodeScanResult = barcodeScanResult.replaceAll(new RegExp(':'), '');
    } on PlatformException {
      barcodeScanResult = 'Failed to get platform version.';
    }
    return barcodeScanResult;
  }
}
