import 'package:crow_panel_quick_setup/models/panel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PanelDescription extends StatelessWidget {
  PanelDescription({
    Key key,
    this.panel,
  }) : super(key: key);

  final Panel panel;

  @override
  Widget build(BuildContext context) {
    return Consumer<Panel>(
      builder: (context, panel, child) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  '${panel.prettyName}',
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const Padding(padding: EdgeInsets.only(bottom: 2.0)),
                Text(
                  'MAC: ${panel.prettyMac}',
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    fontSize: 12.0,
                    color: Colors.black54,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  'Version: ${panel.version}',
                  style: const TextStyle(
                    fontSize: 12.0,
                    color: Colors.black54,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      // Build the expensive widget here.
      // child: SomeExpensiveWidget(),
    );
  }
}