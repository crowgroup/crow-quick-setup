import 'package:crow_panel_quick_setup/models/panel.dart';
import 'package:crow_panel_quick_setup/pages/panels/panel_pictures/panel_pictures_list_wrapper.dart';
import 'package:crow_panel_quick_setup/pages/panels/panels_list_item/enter_panel_dialog.dart';
import 'package:flutter/material.dart';

class PopupMenu extends StatelessWidget {
  PopupMenu({
    Key key,
    this.panel
  }) : super(key: key);

  final Panel panel;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      onSelected: (value) {
        switch(value) {
          case 'enterPanel': enterPanelDialog(panel, context); break;
          case 'panelPicturesList': Navigator.push(context, MaterialPageRoute(builder: (context) => PanelPicturesListWrapper(panel))); break;
          default: break;
        }
      },
      itemBuilder: (context) => [
        // PopupMenuItem(
        //     value: 1,
        //     child: Row(
        //       children: <Widget>[
        //         Padding(
        //           padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
        //           child: Icon(Icons.settings),
        //         ),
        //         Text('Default config')
        //       ],
        //     )
        // ),
        // PopupMenuItem(
        //     value: 2,
        //     child: Row(
        //       children: <Widget>[
        //         Padding(
        //           padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
        //           child: Icon(Icons.backup),
        //         ),
        //         Text('Backup config')
        //       ],
        //     )
        // ),
        // PopupMenuItem(
        //     value: 3,
        //     child: Row(
        //       children: <Widget>[
        //         Padding(
        //           padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
        //           child: Icon(Icons.upgrade),
        //         ),
        //         Text('Upgrade panel')
        //       ],
        //     )
        // ),
        // PopupMenuItem(
        //     value: 4,
        //     child: Row(
        //       children: <Widget>[
        //         Padding(
        //           padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
        //           child: Icon(Icons.person),
        //         ),
        //         Text('Personal page')
        //       ],
        //     )
        // ),
        // PopupMenuItem(
        //     value: 5,
        //     child: Row(
        //       children: <Widget>[
        //         Padding(
        //           padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
        //           child: Icon(Icons.article),
        //         ),
        //         Text('Reports')
        //       ],
        //     )
        // ),
        // PopupMenuItem(
        //     value: 6,
        //     child: Row(
        //       children: <Widget>[
        //         Padding(
        //           padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
        //           child: Icon(Icons.remove_red_eye),
        //         ),
        //         Text('Overview')
        //       ],
        //     )
        // ),
        // PopupMenuItem(
        //     value: 7,
        //     child: Row(
        //       children: <Widget>[
        //         Padding(
        //           padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
        //           child: Icon(Icons.refresh),
        //         ),
        //         Text('Restart panel')
        //       ],
        //     )
        // ),
        // PopupMenuItem(
        //     value: 8,
        //     child: Row(
        //       children: <Widget>[
        //         Padding(
        //           padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
        //           child: Icon(Icons.email),
        //         ),
        //         Text('Restart panel')
        //       ],
        //     )
        // ),
        // PopupMenuItem(
        //     value: 9,
        //     child: Row(
        //       children: <Widget>[
        //         Padding(
        //           padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
        //           child: Icon(Icons.network_cell),
        //         ),
        //         Text('Reset connection')
        //       ],
        //     )
        // ),
        PopupMenuItem(
            value: 'enterPanel',
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                  child: Icon(Icons.login),
                ),
                Text('Enter panel configuration')
              ],
            )
        ),
        PopupMenuItem(
            value: 'panelPicturesList',
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                  child: Icon(Icons.photo_album, color: Colors.black),
                ),
                Text('Panel pictures')
              ],
            )
        ),
      ]
    );
  }
}