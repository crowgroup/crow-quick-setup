import 'package:crow_panel_quick_setup/models/panel.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/panel_configuration_arguments.dart';
import 'package:crow_panel_quick_setup/providers/panel_configuration_provider.dart';
import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Future<void> enterPanelDialog(Panel panel, BuildContext context) async {
  final formKey = new GlobalKey<FormState>();
  String installerCode;
  TextEditingController installerCodeController = new TextEditingController();
  final passwordField = TextFormField(
    keyboardType: TextInputType.number,
    maxLength: 8,
    controller: installerCodeController,
    autofocus: true,
    obscureText: true,
    validator: (value) => value.isEmpty ? "Input installer code first" : null,
    onSaved: (value) => installerCode = value,
    decoration: buildInputDecoration("Input installer code here", Icons.lock, Theme.of(context).accentColor),
  );
  await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Input installer code, please'),
          children: [
            Form(
              key: formKey,
              child: Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    passwordField,
                    SizedBox(height: 20.0),
                    longButtons(
                      "Enter panel configuration",
                          () {
                        final form = formKey.currentState;
                        if (form.validate()) {
                          form.save();
                          Provider.of<PanelsConfigurationProvider>(context, listen: false).reset();
                          Navigator.pop(context);
                          Navigator.pushNamed(context, '/panelConfiguration', arguments: PanelConfigurationArguments(panel, installerCode));
                        } else {
                          print("form is invalid");
                        }
                      },
                      color: Theme.of(context).accentColor),
                  ]
                )
              )
            )
          ],
        );
      }
  );
}