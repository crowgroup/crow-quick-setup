import 'package:crow_panel_quick_setup/models/panel.dart';
import 'package:crow_panel_quick_setup/pages/panels/panels_list_item/enter_panel_dialog.dart';
import 'package:crow_panel_quick_setup/pages/panels/panels_list_item/panel_description.dart';
import 'package:crow_panel_quick_setup/pages/panels/panels_list_item/popup_menu.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PanelListItem extends StatelessWidget {
  PanelListItem({
    Key key,
    this.panel,
  }) : super(key: key);

  final Panel panel;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: panel,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 2.0),
        child:
        Card(
          child: InkWell(
            splashColor: Theme.of(context).accentColor.withAlpha(30),
            onTap: () {
              enterPanelDialog(panel, context);
            },
            child:
              SizedBox(
                height: 100,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    AspectRatio(
                      aspectRatio: 1.0,
                      child: Container(
                        decoration: BoxDecoration(
                          color: const Color(0xff7c94b6),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            colorFilter: panel.state == "ONLINE" ? null : ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
                            image: AssetImage('images/shepherd_panel_center.jpg'),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10.0, 8.0, 2.0, 8.0),
                        child: PanelDescription(panel: panel)
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                      child:
                        PopupMenu(panel: panel),
                    )
                  ],
                ),
              ),
          ),
        )
      )
    );
  }
}

