import 'dart:convert';

import 'package:crow_panel_quick_setup/models/token.dart';
import 'package:crow_panel_quick_setup/pages/common_drawer.dart';
import 'package:crow_panel_quick_setup/pages/login.dart';
import 'package:crow_panel_quick_setup/pages/panels/add_new_panel/mac_address_formatter.dart';
import 'package:crow_panel_quick_setup/util/app_url.dart';
import 'package:crow_panel_quick_setup/util/local_storage.dart';
import 'package:flutter/material.dart';
import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';

enum AddStatus {
  NotAdded,
  Added,
  Adding,
  Error,
  NotAuthorizedError,
}

class AddPanel extends StatefulWidget {
  @override
  _AddPanelState createState() => _AddPanelState();
}

class _AddPanelState extends State<AddPanel> {
  AddStatus addStatus = AddStatus.NotAdded;
  final formKey = new GlobalKey<FormState>();
  TextEditingController macController = new TextEditingController(text: '00:13:A1:');
  TextEditingController nameController = new TextEditingController();
  TextEditingController remoteAccessPasswordController = new TextEditingController();
  TextEditingController userCodeController = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add panel to account'),
      ),
      drawer: CommonDrawer(),
      body: getView()
    );
  }

  getView() {
    switch(addStatus) {
      case AddStatus.Adding: return Center(child: CircularProgressIndicator()); break;
      case AddStatus.Error: return Center(child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Unknown Error'),
            MaterialButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: SizedBox(
                width: double.infinity,
                child: Text(
                  "Back to panels list",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ]
      )
      ); break;
      case AddStatus.Added: return Center(child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('The panel was successfully added to your account'),
            MaterialButton(
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/panelsList');
              },
              child: SizedBox(
                width: double.infinity,
                child: Text(
                  "Back to panels list",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ]
      )
      ); break;
      case AddStatus.NotAuthorizedError:
        SchedulerBinding.instance.addPostFrameCallback((_) {
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Login()));
        });
        return Center(child: Text('Not Authorized')); break;
      case AddStatus.NotAdded: return addPanelForm();
      break;
      default: return Column();
    }
  }

  addPanelForm() {
    final macField = TextFormField(
        inputFormatters: <TextInputFormatter>[MacAddressTextFormatter()],
        maxLength: 17,
        controller: macController,
        autofocus: true,
        validator: (value) => value.isEmpty ? "Input panel mac first" : null,
        // onSaved: (value) => mac = value,
        decoration: InputDecoration(
          hintText: "Input panel mac here",
          contentPadding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0),
              borderSide: BorderSide(color: Theme.of(context).accentColor)
          ),
          suffixIcon: IconButton(
              icon: Icon(Icons.qr_code, color: Theme.of(context).accentColor),
              onPressed: () async {
                nameController.text = await scanQR();
              }),
        )
    );

    final nameField = TextFormField(
        maxLength: 32,
        controller: nameController,
        autofocus: true,
        validator: (value) => value.isEmpty ? "Input panel name first" : null,
        // onSaved: (value) => name = value,
        decoration: InputDecoration(
          hintText: "Input panel name here",
          contentPadding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0),
              borderSide: BorderSide(color: Theme.of(context).accentColor)
          ),
        )
    );

    final passwordField = TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 8,
        controller: remoteAccessPasswordController,
        autofocus: true,
        validator: (value) => value.isEmpty ? "Input remote password first" : null,
        // onSaved: (value) => password = value,
        decoration: InputDecoration(
          hintText: "Input remote access password here",
          contentPadding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0),
              borderSide: BorderSide(color: Theme.of(context).accentColor)
          ),
        )
    );

    final codeField = TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 8,
        controller: userCodeController,
        autofocus: true,
        validator: (value) => value.isEmpty ? "Input user code first" : null,
        // onSaved: (value) => code = value,
        decoration: InputDecoration(
          hintText: "Input user code here",
          contentPadding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0),
              borderSide: BorderSide(color: Theme.of(context).accentColor)
          ),
        )
    );

    return Center( child:
      SingleChildScrollView(
        child:
        Container(
          padding: EdgeInsets.all(40.0),
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                label("Mac address"),
                SizedBox(height: 5.0),
                macField,
                SizedBox(height: 5.0),
                label("Panel name"),
                SizedBox(height: 5.0),
                nameField,
                label("Remote access password"),
                SizedBox(height: 5.0),
                passwordField,
                label("User code"),
                SizedBox(height: 5.0),
                codeField,
                SizedBox(height: 20.0),
                longButtons("Next",
                  () {
                    final form = formKey.currentState;
                    if (form.validate()) {
                      form.save();
                      addPanel(
                        macController.text.replaceAll(new RegExp(r'[:]+'), ''),
                        nameController.text,
                        remoteAccessPasswordController.text,
                        userCodeController.text
                      );
                    } else {
                      print("form is invalid");
                    }
                  },
                  color: Theme.of(context).accentColor
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<String> scanQR() async {
    String panelMac = '';
    String barcodeScanRes = '';
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#FFFDDD", "Cancel", true, ScanMode.QR);
      panelMac = barcodeScanRes == '-1' ? '' : barcodeScanRes;
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }
    String noSemicolons = panelMac.replaceAll(new RegExp(r'[:]+'), '');
    panelMac = noSemicolons.replaceAllMapped(RegExp(r".{2}"), (match) => "${match.group(0)}:");
    if(panelMac.endsWith(':')) {
      panelMac = panelMac.substring(0, panelMac.length - 1);
    }
    return panelMac;
  }

  Future<void> addPanel(String mac, String name, String password, String code) async {
    setState(() {
      addStatus = AddStatus.Adding;
    });

    Token token = await LocalStorage().getToken();
    Uri uri = Uri.https(AppUrls.baseURL, AppUrls.addPanel);

    Response response = await post(
        uri,
        headers: {
          'Authorization': 'Bearer ${token.accessToken}',
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: jsonEncode({
          "control_panel": {
            "mac": "$mac",
            "remote_access_password": "$password"
          },
          "user_code": "$code",
          "name": "$name"
        }
        ));

    setState(() {
      if (response.statusCode == 201) {
        addStatus = AddStatus.Added;
      } else if (response.statusCode == 401) {
        addStatus = AddStatus.NotAuthorizedError;
      } else {
        addStatus = AddStatus.Error;
      }
    });
  }
}
