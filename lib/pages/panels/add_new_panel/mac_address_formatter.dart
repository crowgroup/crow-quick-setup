import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MacAddressTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.isEmpty) {
      return newValue.copyWith(text: '');
    } else if (newValue.text.compareTo(oldValue.text) != 0 && !oldValue.text.endsWith(':')) {
      final int selectionIndexFromTheRight = newValue.text.length - newValue.selection.end;
      String noSemicolons = newValue.text.replaceAll(new RegExp(r'[:]+'), '');
      final newString = noSemicolons.replaceAllMapped(RegExp(r".{2}"), (match) => "${match.group(0)}:");
      return TextEditingValue(
        text: newString.toUpperCase(),
        selection: TextSelection.collapsed(offset: newString.length - selectionIndexFromTheRight),
      );
    } else {
      return TextEditingValue(
        text: newValue.text?.toUpperCase(),
        selection: newValue.selection,
      );
    }
  }
}