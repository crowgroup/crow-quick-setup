import 'package:crow_panel_quick_setup/providers/panels_list_provider.dart';
import 'package:crow_panel_quick_setup/providers/panels_list_search_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

enum PanelState { online, offline, all }

class PanelsListSearch extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    PanelsListProvider panelsListProvider = Provider.of<PanelsListProvider>(context, listen: false);
    return Consumer<PanelsListSearchProvider>(builder: (context, panelsListSearchProvider, child) {
          TextEditingController searchTermController = TextEditingController(text: '${panelsListSearchProvider.search}');
          final _searchFormKey = GlobalKey<FormState>();
          PanelState _panelState = panelsListSearchProvider.panelStatus == 'ONLINE' ? PanelState.online : panelsListSearchProvider.panelStatus == 'OFFLINE' ? PanelState.offline : PanelState.all;
          return panelsListSearchProvider.isSearchFormVisible ?
          Form(
            key: _searchFormKey,
            child: Column(
                children: <Widget>[
                  Opacity(
                      opacity: 0.95,
                      child: Container(
                          color: Colors.white,
                          child: Padding(
                              padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 10.0),
                              child: Column(children: [
                                TextFormField(
                                  controller: searchTermController,
                                  decoration: InputDecoration(
                                      hintText: 'Enter a search term'
                                  ),
                                  onChanged: (value) {
                                    panelsListSearchProvider.search = value;
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter some text';
                                    }
                                    return null;
                                  },
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Text('Online'),
                                        Radio(
                                          value: PanelState.online,
                                          groupValue: _panelState,
                                          onChanged: (PanelState value) {
                                            _panelState = value;
                                            panelsListSearchProvider.panelStatus = 'ONLINE';
                                          },
                                        ),
                                      ],
                                    ),
                                    Row(
                                        children: [
                                          Text('Offline'),
                                          Radio(
                                            value: PanelState.offline,
                                            groupValue: _panelState,
                                            onChanged: (PanelState value) {
                                              _panelState = value;
                                              panelsListSearchProvider.panelStatus = 'OFFLINE';                                            },
                                          ),
                                        ]
                                    ),
                                    Row(
                                        children: [
                                          Text('All'),
                                          Radio(
                                            value: PanelState.all,
                                            groupValue: _panelState,
                                            onChanged: (PanelState value) {
                                              _panelState = value;
                                              panelsListSearchProvider.panelStatus = '';
                                            },
                                          ),
                                        ]
                                    )
                                  ],
                                ),
                                SizedBox(
                                  width: double.infinity,
                                  child: RaisedButton(
                                      color: Theme.of(context).accentColor,
                                      child: Text(
                                        'Search',
                                        style: TextStyle(
                                            color: Colors.white
                                        ),
                                      ),
                                      onPressed: () {
                                        panelsListSearchProvider.isSearchFormVisible = false;
                                        panelsListProvider.panelStatus = panelsListSearchProvider.panelStatus;
                                        panelsListProvider.search = panelsListSearchProvider.search;
                                        panelsListProvider.page = 1;
                                      }
                                  ),
                                )
                              ]
                              )
                          )
                      )
                  )
                ]
            )
        ) : Column();
      }
    );
  }
}