import 'package:crow_panel_quick_setup/models/devices/temperature_sensor/temperature_sensor.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/temperature_sensor/temperature_sensor_item.dart';
import 'package:flutter/material.dart';

class TemperatureSensorWidget extends StatelessWidget {
  TemperatureSensorWidget({
    Key key,
    this.temperatureSensors
  }) : super(key: key);

  final List<TemperatureSensor> temperatureSensors;

  @override
  Widget build(BuildContext context) {
    return temperatureSensors.length > 0 ? Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          child: Text(
            "Temperature Sensors",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        ),
        Container(child: GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > 0) {
                print('settings');
              } else if (details.delta.dx < 0) {
                print('delete');
              }
            },
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: temperatureSensors.length,
                itemBuilder: (BuildContext context, int index) {
                  return TemperatureSensorItem(temperatureSensor: temperatureSensors[index]);
                }
            ),
          )
        ),
        SizedBox(height: 20.0),
      ]
    ) : Column();
  }
}