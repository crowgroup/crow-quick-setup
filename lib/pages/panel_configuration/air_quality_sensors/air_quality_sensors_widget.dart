import 'package:crow_panel_quick_setup/models/devices/air_quality_sensor/air_quality_sensor.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/air_quality_sensors/air_quality_sensors_item.dart';
import 'package:flutter/material.dart';

class AirQualitySensorsWidget extends StatelessWidget {
  AirQualitySensorsWidget({
    Key key,
    this.airQualitySensors
  }) : super(key: key);

  final List<AirQualitySensor> airQualitySensors;

  @override
  Widget build(BuildContext context) {
    return airQualitySensors.length > 0 ? Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          child: Text(
            "Air Quality Sensors",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        ),
        Container(child: GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > 0) {
                print('settings');
              } else if (details.delta.dx < 0) {
                print('delete');
              }
            },
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: airQualitySensors.length,
                itemBuilder: (BuildContext context, int index) {
                  return AirQualitySensorsItem(airQualitySensor: airQualitySensors[index]);
                }
            ),
          )
        ),
        SizedBox(height: 20.0),
      ]
    ) : Column();
  }
}