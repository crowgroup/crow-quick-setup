import 'package:crow_panel_quick_setup/models/devices/air_quality_sensor/air_quality_sensor.dart';
import 'package:crow_panel_quick_setup/models/devices/flood_detector/flood_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/glass_break_detector/glass_break_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_pir_camera/indoor_pir_camera.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_sounder/indoor_sounder.dart';
import 'package:crow_panel_quick_setup/models/devices/infrared_motion_detector/infrared_motion_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/led_indication_keypad/led_indication_keypad.dart';
import 'package:crow_panel_quick_setup/models/devices/magnet_contact/magnet_contact.dart';
import 'package:crow_panel_quick_setup/models/devices/smart_plug/smart_plug.dart';
import 'package:crow_panel_quick_setup/models/devices/smoke_detector/smoke_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/temperature_sensor/temperature_sensor.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/devices_configurations/air_quality_sensor_configuration.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/devices_configurations/flood_detector_configuration.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/devices_configurations/glass_break_detector_configuration.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/devices_configurations/indoor_pir_camera_configuration.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/devices_configurations/indoor_sounder_configuration.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/devices_configurations/infrared_motion_detector_configuration.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/devices_configurations/led_indication_keypad_configuration.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/devices_configurations/magnet_contact_configuration.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/devices_configurations/smart_plug_configuration.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/devices_configurations/smoke_detector_configuration.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/devices_configurations/temperature_sensor_configuration.dart';
import 'package:flutter/material.dart';

Future<dynamic> getDeviceConfig(BuildContext context, int deviceType, {dynamic deviceConfig}) async {
  switch(deviceType) {
    case LedIndicationKeypad.type: return getLedIndicationKeypadConfiguration(context, deviceConfig); break;
    case InfraredMotionDetector.type: return getInfraredMotionDetectorConfiguration(context, deviceConfig); break;
    case IndoorPirCamera.type: return getIndoorPirCameraConfiguration(context, deviceConfig); break;
    case MagnetContact.type: return getMagnetContactConfiguration(context, deviceConfig); break;
    case SmartPlug.type: return getSmartPlugConfiguration(context, deviceConfig); break;
    case IndoorSounder.type: return getIndoorSounderConfiguration(context, deviceConfig); break;
    case SmokeDetector.type: return getSmokeDetectorConfiguration(context, deviceConfig); break;
    case FloodDetector.type: return getFloodDetectorConfiguration(context, deviceConfig); break;
    case GlassBreakDetector.type: return getGlassBreakDetectorConfiguration(context, deviceConfig); break;
    case AirQualitySensor.type: return getAirQualitySensorConfiguration(context, deviceConfig); break;
    case TemperatureSensor.type: return getTemperatureSensorConfiguration(context, deviceConfig); break;
    default: return {};
  }
}