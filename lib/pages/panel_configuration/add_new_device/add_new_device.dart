import 'dart:convert';
import 'package:crow_panel_quick_setup/models/device_info.dart';
import 'package:crow_panel_quick_setup/models/devices/air_quality_sensor/air_quality_sensor.dart';
import 'package:crow_panel_quick_setup/models/devices/air_quality_sensor/air_quality_sensor_config.dart';
import 'package:crow_panel_quick_setup/models/devices/flood_detector/flood_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/flood_detector/flood_detector_config.dart';
import 'package:crow_panel_quick_setup/models/devices/glass_break_detector/glass_break_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/glass_break_detector/glass_break_detector_config.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_pir_camera/indoor_pir_camera.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_pir_camera/indoor_pir_camera_config.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_sounder/indoor_sounder.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_sounder/indoor_sounder_config.dart';
import 'package:crow_panel_quick_setup/models/devices/infrared_motion_detector/infrared_motion_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/infrared_motion_detector/infrared_motion_detector_config.dart';
import 'package:crow_panel_quick_setup/models/devices/led_indication_keypad/led_indication_keypad.dart';
import 'package:crow_panel_quick_setup/models/devices/led_indication_keypad/led_indication_keypad_config.dart';
import 'package:crow_panel_quick_setup/models/devices/magnet_contact/magnet_contact.dart';
import 'package:crow_panel_quick_setup/models/devices/magnet_contact/magnet_contact_config.dart';
import 'package:crow_panel_quick_setup/models/devices/smart_plug/smart_plug.dart';
import 'package:crow_panel_quick_setup/models/devices/smart_plug/smart_plug_config.dart';
import 'package:crow_panel_quick_setup/models/devices/smoke_detector/smoke_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/smoke_detector/smoke_detector_config.dart';
import 'package:crow_panel_quick_setup/models/devices/temperature_sensor/temperature_sensor.dart';
import 'package:crow_panel_quick_setup/models/devices/temperature_sensor/temperature_sensor_config.dart';
import 'package:crow_panel_quick_setup/models/devices/verification_camera/verification_camera.dart';
import 'package:crow_panel_quick_setup/models/devices/verification_camera/verification_camera_config.dart';
import 'package:crow_panel_quick_setup/models/token.dart';
import 'package:crow_panel_quick_setup/pages/login.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/get_device_config.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/get_device_jpeg_quality.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/get_device_name.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/get_device_pet_immune.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/get_device_supervision.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/get_device_switch_and.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/get_device_type.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/get_device_zone_led.dart';
import 'package:crow_panel_quick_setup/providers/panel_configuration_provider.dart';
import 'package:crow_panel_quick_setup/util/app_url.dart';
import 'package:crow_panel_quick_setup/util/local_storage.dart';
import 'package:flutter/material.dart';
import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';

class AddDevice extends StatefulWidget {
  @override
  _AddDeviceState createState() => _AddDeviceState();
}

class _AddDeviceState extends State<AddDevice> {
  int deviceId;
  bool isLoading = false;
  final formKey = new GlobalKey<FormState>();
  TextEditingController serialController = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add a new device'),
      ),
      body: getView(),
    );
  }

  getView() {
    if(isLoading) {
      return Center(child: CircularProgressIndicator());
    } else {
      return getSerialForm();
    }
  }

  getSerialForm() {
    final deviceIdField = TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 7,
        controller: serialController,
        autofocus: true,
        validator: (value) => value.isEmpty ? "Input device serial first" : null,
        decoration: InputDecoration(
          hintText: "Input serial number here",
          contentPadding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Theme.of(context).accentColor)
          ),
          suffixIcon: IconButton(
            icon: Icon(Icons.qr_code, color: Theme.of(context).accentColor),
            onPressed: () async {
              serialController.text = await scanQR();
            }
          ),
        )
    );

    return Center(child: SingleChildScrollView(child: Container(
      padding: EdgeInsets.all(40.0),
        child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              label("Serial number"),
              SizedBox(height: 5.0),
              deviceIdField,
              SizedBox(height: 20.0),
              longButtons("Next", () async {
                  final form = formKey.currentState;
                  if (form.validate()) {
                    form.save();
                    DeviceInfo deviceInfo = await getDeviceInfo(serialController.text);
                    int deviceType = deviceInfo?.deviceType != null && deviceInfo?.deviceType != -1 ? deviceInfo?.deviceType : await getDeviceType(context);
                    if (deviceType != -1) {
                      createDevice(serialController.text, deviceType);
                    } else {
                      print('Device type is -1');
                    }
                  } else {
                    print("form is invalid");
                  }
                },
                color: Theme.of(context).accentColor
              )
            ],
          ),
        ),
      ),
    ),
    );
  }

  Future<String> scanQR() async {
    String deviceId = '';
    String barcodeScanRes = '';
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#FFFDDD", "Cancel", true, ScanMode.QR);
      List<String> barcodeScanResSplitted = barcodeScanRes.split('-');
      deviceId = barcodeScanResSplitted.last.length > 7 ? barcodeScanResSplitted.last.substring(3) : barcodeScanResSplitted.last;
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }
    return deviceId;
  }

  Future<DeviceInfo> getDeviceInfo(String serial) async {

    setState(() {
      isLoading = true;
    });


    DeviceInfo deviceInfo;
    Token token = await LocalStorage().getToken();
    Uri uri = Uri.https(AppUrls.baseURL, "/management/serial/$serial/");

    Response response = await get(
      uri,
      headers: {
        'Authorization': 'Bearer ${token.accessToken}',
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> responseData = json.decode(response.body); //isLoading = true;
      deviceInfo = DeviceInfo.fromJson(responseData);
    } else if (response.statusCode == 401) {
      Provider.of<PanelsConfigurationProvider>(context, listen: false).reset();
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Login()));
      });
    }

    setState(() {
      isLoading = false;
    });

    return deviceInfo;
  }

  createDevice(String serial, int deviceType) {
    switch(deviceType) {
      case LedIndicationKeypad.type: addLedIndicationKeypad(); break;
      case InfraredMotionDetector.type: addInfraredMotionDetector(); break;
      case IndoorPirCamera.type: addIndoorPirCamera(); break;
      case MagnetContact.type: addMagnetContact(); break;
      case SmartPlug.type: addSmartPlug(); break;
      case IndoorSounder.type: addIndoorSounder(); break;
      case SmokeDetector.type: addSmokeDetector(); break;
      case FloodDetector.type: addFloodDetector(); break;
      case GlassBreakDetector.type: addGlassBreakDetector(); break;
      case AirQualitySensor.type: addAirQualitySensor(); break;
      case TemperatureSensor.type: addTemperatureSensor(); break;
    }
  }
  
  addLedIndicationKeypad() async {
    Map<String, dynamic> rawConfig = Provider.of<PanelsConfigurationProvider>(context, listen: false).panelConfiguration.rawConfig;
    int emptySlotId = LedIndicationKeypad.getEmptySlotIndex(rawConfig);
    if (emptySlotId != -1) {
      FocusScope.of(context).requestFocus(new FocusNode());
      String name = await getDeviceName(context);
      LedIndicationKeypadConfig ledIndicationKeypadConfig = LedIndicationKeypadConfig();
      ledIndicationKeypadConfig = await getDeviceConfig(context, LedIndicationKeypad.type, deviceConfig: ledIndicationKeypadConfig);
      LedIndicationKeypad(slotId: emptySlotId, serialNumber: int.parse(serialController.text), name: name, ledIndicationKeypadConfig: ledIndicationKeypadConfig).save(rawConfig);
      Provider.of<PanelsConfigurationProvider>(context, listen: false).savePanelConfiguration();
      Navigator.pop(context);
    } else {
      print('No empty slot for keypad..');
    }
  }

  addInfraredMotionDetector() async {
    Map<String, dynamic> rawConfig = Provider.of<PanelsConfigurationProvider>(context, listen: false).panelConfiguration.rawConfig;
    int emptySlotId = InfraredMotionDetector.getEmptySlotIndex(rawConfig);
    if (emptySlotId != -1) {
      FocusScope.of(context).requestFocus(new FocusNode());
      String name = await getDeviceName(context);
      int supervision = await getDeviceSupervision(context);
      bool zoneLed = await getDeviceZoneLed(context);
      bool petImmuni = await getDevicePetImmune(context);
      InfraredMotionDetectorConfig infraredMotionDetectorConfig = InfraredMotionDetectorConfig(supervision: supervision, zoneLed: zoneLed, petImmuni: petImmuni);
      infraredMotionDetectorConfig = await getDeviceConfig(context, InfraredMotionDetector.type, deviceConfig: infraredMotionDetectorConfig);
      InfraredMotionDetector(slotId: emptySlotId, serialNumber: int.parse(serialController.text), name: name, infraredMotionDetectorConfig: infraredMotionDetectorConfig).save(rawConfig);
      Provider.of<PanelsConfigurationProvider>(context, listen: false).savePanelConfiguration();
      Navigator.pop(context);
    } else {
      print('No empty slot for infrared motion detector..');
    }
  }

  addIndoorPirCamera() async {
    Map<String, dynamic> rawConfig = Provider.of<PanelsConfigurationProvider>(context, listen: false).panelConfiguration.rawConfig;
    int emptySlotId = IndoorPirCamera.getEmptySlotIndex(rawConfig);
    if (emptySlotId != -1) {
      FocusScope.of(context).requestFocus(new FocusNode());
      String name = await getDeviceName(context);
      int quality = await getDeviceJpegQuality(context);
      IndoorPirCameraConfig indoorPirCameraConfig = IndoorPirCameraConfig(quality: quality);
      indoorPirCameraConfig = await getDeviceConfig(context, IndoorPirCamera.type, deviceConfig: indoorPirCameraConfig);
      IndoorPirCamera(slotId: emptySlotId, serialNumber: int.parse(serialController.text), name: name, indoorPirCameraConfig: indoorPirCameraConfig).save(rawConfig);
      Provider.of<PanelsConfigurationProvider>(context, listen: false).savePanelConfiguration();
      Navigator.pop(context);
    } else {
      print('No empty slot for indoor pir camera..');
    }
  }

  addSmokeDetector() async {
    Map<String, dynamic> rawConfig = Provider.of<PanelsConfigurationProvider>(context, listen: false).panelConfiguration.rawConfig;
    int emptySlotId = InfraredMotionDetector.getEmptySlotIndex(rawConfig);
    if (emptySlotId != -1) {
      FocusScope.of(context).requestFocus(new FocusNode());
      String name = await getDeviceName(context);
      int supervision = await getDeviceSupervision(context);
      bool zoneLed = await getDeviceZoneLed(context);
      SmokeDetectorConfig smokeDetectorConfig = SmokeDetectorConfig(supervision: supervision, zoneLed: zoneLed);
      smokeDetectorConfig = await getDeviceConfig(context, SmokeDetector.type, deviceConfig: smokeDetectorConfig);
      SmokeDetector(slotId: emptySlotId, serialNumber: int.parse(serialController.text), name: name, smokeDetectorConfig: smokeDetectorConfig).save(rawConfig);
      Provider.of<PanelsConfigurationProvider>(context, listen: false).savePanelConfiguration();
      Navigator.pop(context);
    } else {
      print('No empty slot for smoke detector..');
    }
  }

  addFloodDetector() async {
    Map<String, dynamic> rawConfig = Provider.of<PanelsConfigurationProvider>(context, listen: false).panelConfiguration.rawConfig;
    int emptySlotId = FloodDetector.getEmptySlotIndex(rawConfig);
    if (emptySlotId != -1) {
      FocusScope.of(context).requestFocus(new FocusNode());
      String name = await getDeviceName(context);
      int supervision = await getDeviceSupervision(context);
      bool zoneLed = await getDeviceZoneLed(context);
      FloodDetectorConfig floodDetectorConfig = FloodDetectorConfig(supervision: supervision, zoneLed: zoneLed);
      floodDetectorConfig = await getDeviceConfig(context, FloodDetector.type, deviceConfig: floodDetectorConfig);
      FloodDetector(slotId: emptySlotId, serialNumber: int.parse(serialController.text), name: name, floodDetectorConfig: floodDetectorConfig).save(rawConfig);
      Provider.of<PanelsConfigurationProvider>(context, listen: false).savePanelConfiguration();
      Navigator.pop(context);
    } else {
      print('No empty slot for flood detector..');
    }
  }

  addGlassBreakDetector() async {
    Map<String, dynamic> rawConfig = Provider.of<PanelsConfigurationProvider>(context, listen: false).panelConfiguration.rawConfig;
    int emptySlotId = GlassBreakDetector.getEmptySlotIndex(rawConfig);
    if (emptySlotId != -1) {
      FocusScope.of(context).requestFocus(new FocusNode());
      String name = await getDeviceName(context);
      int supervision = await getDeviceSupervision(context);
      bool zoneLed = await getDeviceZoneLed(context);
      GlassBreakDetectorConfig glassBreakDetectorConfig = GlassBreakDetectorConfig(supervision: supervision, zoneLed: zoneLed);
      glassBreakDetectorConfig = await getDeviceConfig(context, GlassBreakDetector.type, deviceConfig: glassBreakDetectorConfig);
      GlassBreakDetector(slotId: emptySlotId, serialNumber: int.parse(serialController.text), name: name, glassBreakDetectorConfig: glassBreakDetectorConfig).save(rawConfig);
      Provider.of<PanelsConfigurationProvider>(context, listen: false).savePanelConfiguration();
      Navigator.pop(context);
    } else {
      print('No empty slot for glass break detector..');
    }
  }

  addAirQualitySensor() async {
    Map<String, dynamic> rawConfig = Provider.of<PanelsConfigurationProvider>(context, listen: false).panelConfiguration.rawConfig;
    int emptySlotId = AirQualitySensor.getEmptySlotIndex(rawConfig);
    if (emptySlotId != -1) {
      FocusScope.of(context).requestFocus(new FocusNode());
      String name = await getDeviceName(context);
      int supervision = await getDeviceSupervision(context);
      AirQualitySensorConfig airQualitySensorConfig = AirQualitySensorConfig(supervision: supervision);
      airQualitySensorConfig = await getDeviceConfig(context, AirQualitySensor.type, deviceConfig: airQualitySensorConfig);
      AirQualitySensor(slotId: emptySlotId, serialNumber: int.parse(serialController.text), name: name, airQualitySensorConfig: airQualitySensorConfig).save(rawConfig);
      Provider.of<PanelsConfigurationProvider>(context, listen: false).savePanelConfiguration();
      Navigator.pop(context);
    } else {
      print('No empty slot for air quality sensor..');
    }
  }

  addVerificationCamera() async {
    Map<String, dynamic> rawConfig = Provider.of<PanelsConfigurationProvider>(context, listen: false).panelConfiguration.rawConfig;
    int emptySlotId = VerificationCamera.getEmptySlotIndex(rawConfig);
    if (emptySlotId != -1) {
      FocusScope.of(context).requestFocus(new FocusNode());
      String name = await getDeviceName(context);
      VerificationCameraConfig verificationCameraConfig = VerificationCameraConfig();
      VerificationCamera(slotId: emptySlotId, serialNumber: int.parse(serialController.text), name: name, verificationCameraConfig: verificationCameraConfig).save(rawConfig);
      Provider.of<PanelsConfigurationProvider>(context, listen: false).savePanelConfiguration();
      Navigator.pop(context);
    } else {
      print('No empty slot for verification camera..');
    }
  }

  addTemperatureSensor() async {
    Map<String, dynamic> rawConfig = Provider.of<PanelsConfigurationProvider>(context, listen: false).panelConfiguration.rawConfig;
    int emptySlotId = TemperatureSensor.getEmptySlotIndex(rawConfig);
    if (emptySlotId != -1) {
      FocusScope.of(context).requestFocus(new FocusNode());
      String name = await getDeviceName(context);
      TemperatureSensorConfig temperatureSensorConfig = TemperatureSensorConfig();
      temperatureSensorConfig = await getDeviceConfig(context, TemperatureSensor.type, deviceConfig: temperatureSensorConfig);
      TemperatureSensor(slotId: emptySlotId, serialNumber: int.parse(serialController.text), name: name, temperatureSensorConfig: temperatureSensorConfig).save(rawConfig);
      Provider.of<PanelsConfigurationProvider>(context, listen: false).savePanelConfiguration();
      Navigator.pop(context);
    } else {
      print('No empty slot for temperature sensor..');
    }
  }

  addMagnetContact() async {
    Map<String, dynamic> rawConfig = Provider.of<PanelsConfigurationProvider>(context, listen: false).panelConfiguration.rawConfig;
    int emptySlotId = MagnetContact.getEmptySlotIndex(rawConfig);
    if (emptySlotId != -1) {
      FocusScope.of(context).requestFocus(new FocusNode());
      String name = await getDeviceName(context);
      int supervision = await getDeviceSupervision(context);
      bool zoneLed = await getDeviceZoneLed(context);
      bool switchAnd = await getDeviceSwitchAnd(context);
      MagnetContactConfig magnetContactConfig = MagnetContactConfig(supervision: supervision, zoneLed: zoneLed, switchAnd: switchAnd);
      magnetContactConfig = await getDeviceConfig(context, MagnetContact.type, deviceConfig: magnetContactConfig);
      MagnetContact(slotId: emptySlotId, serialNumber: int.parse(serialController.text), name: name, magnetContactConfig: magnetContactConfig).save(rawConfig);
      Provider.of<PanelsConfigurationProvider>(context, listen: false).savePanelConfiguration();
      Navigator.pop(context);
    } else {
      print('No empty slot for magnet contact..');
    }
  }

  addSmartPlug() async {
    Map<String, dynamic> rawConfig = Provider.of<PanelsConfigurationProvider>(context, listen: false).panelConfiguration.rawConfig;
    int emptySlotId = SmartPlug.getEmptySlotIndex(rawConfig);
    if (emptySlotId != -1) {
      FocusScope.of(context).requestFocus(new FocusNode());
      String name = await getDeviceName(context);
      SmartPlugConfig smartPlugConfig = SmartPlugConfig();
      smartPlugConfig = await getDeviceConfig(context, SmartPlug.type, deviceConfig: smartPlugConfig);
      SmartPlug(slotId: emptySlotId, serialNumber: int.parse(serialController.text), name: name, smartPlugConfig: smartPlugConfig).save(rawConfig);
      Provider.of<PanelsConfigurationProvider>(context, listen: false).savePanelConfiguration();
      Navigator.pop(context);
    } else {
      print('No empty slot for smart plug..');
    }
  }

  addIndoorSounder() async {
    Map<String, dynamic> rawConfig = Provider.of<PanelsConfigurationProvider>(context, listen: false).panelConfiguration.rawConfig;
    int emptySlotId = IndoorSounder.getEmptySlotIndex(rawConfig);
    if (emptySlotId != -1) {
      FocusScope.of(context).requestFocus(new FocusNode());
      String name = await getDeviceName(context);
      IndoorSounderConfig indoorSounderConfig = IndoorSounderConfig();
      indoorSounderConfig = await getDeviceConfig(context, IndoorSounder.type, deviceConfig: indoorSounderConfig);
      IndoorSounder(slotId: emptySlotId, serialNumber: int.parse(serialController.text), name: name, indoorSounderConfig: indoorSounderConfig).save(rawConfig);
      Provider.of<PanelsConfigurationProvider>(context, listen: false).savePanelConfiguration();
      Navigator.pop(context);
    } else {
      print('No empty slot for indoor sounder..');
    }
  }
}
