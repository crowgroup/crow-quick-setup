import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/material.dart';

int quality;

Future<int> getDeviceJpegQuality(BuildContext context) async {
  final formKey = new GlobalKey<FormState>();
  quality = 7;

  await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Choose device led condition, please'),
          children: [
            Form(
                key: formKey,
                child:
                Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                        children: [
                          ConfigWidget(),
                          longButtons(
                              "Ok",
                                  () {
                                final form = formKey.currentState;
                                if (form.validate()) {
                                  form.save();
                                  Navigator.pop(context);
                                } else {
                                  print("form is invalid");
                                }
                              },
                              color: Theme.of(context).accentColor),
                        ]
                    )
                )
            )
          ],
        );
      }
  );
  return quality;
}

class ConfigWidget extends StatefulWidget {
  ConfigWidget({Key key}) : super(key: key);

  @override
  _ConfigWidgetState createState() => _ConfigWidgetState();
}

class _ConfigWidgetState extends State<ConfigWidget> {
  String getJpegQualityLevelLabel(int value) {
    return '${value * 10 + 20}%';
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('JPEG Quality level: ${getJpegQualityLevelLabel(quality)}', style: TextStyle(fontWeight: FontWeight.bold)),
          Slider(
            value: quality.toDouble(),
            min: 0,
            max: 7,
            divisions: 7,
            label: getJpegQualityLevelLabel(quality),
            onChanged: (double value) {
              setState(() {
                quality = value.round();
              });
            },
          ),
          SizedBox(height: 20.0),
        ]
    );
  }
}