import 'package:crow_panel_quick_setup/models/devices/temperature_sensor/temperature_sensor_config.dart';
import 'package:crow_panel_quick_setup/util/formatters/range_formatter.dart';
import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

double thresholdTemperatureHigh = 800.0;
double thresholdTemperatureLow = 250.0;
double thresholdTemperatureNormal = 700.0;
int alertTime = 20;

enum AutoConfig { Custom, RefrigeratedStorage, RoomTemperature }

Future<TemperatureSensorConfig> getTemperatureSensorConfiguration(BuildContext context, TemperatureSensorConfig initialDeviceConfig) async {
  final formKey = new GlobalKey<FormState>();

  thresholdTemperatureHigh = initialDeviceConfig.thresholdTemperatureHigh.toDouble();
  thresholdTemperatureLow = initialDeviceConfig.thresholdTemperatureLow.toDouble();
  thresholdTemperatureNormal = initialDeviceConfig.thresholdTemperatureNormal.toDouble();
  alertTime = initialDeviceConfig.alertTime;

  await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Final device configuration'),
          children: [
            Form(
                key: formKey,
                child:
                Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                        children: [
                          ConfigWidget(),
                          longButtons(
                              "Ok",
                                  () {
                                final form = formKey.currentState;
                                if (form.validate()) {
                                  form.save();
                                  Navigator.pop(context);
                                } else {
                                  print("form is invalid");
                                }
                              },
                              color: Theme.of(context).accentColor),
                        ]
                    )
                )
            )
          ],
        );
      }
  );
  print(alertTime);
  return TemperatureSensorConfig(
      thresholdTemperatureHigh: thresholdTemperatureHigh.round(),
      thresholdTemperatureLow: thresholdTemperatureLow.round(),
      thresholdTemperatureNormal: thresholdTemperatureNormal.round(),
      alertTime: alertTime
  );
}

class ConfigWidget extends StatefulWidget {
  ConfigWidget({Key key}) : super(key: key);

  @override
  _ConfigWidgetState createState() => _ConfigWidgetState();
}

class _ConfigWidgetState extends State<ConfigWidget> {
  AutoConfig autoConfig = AutoConfig.Custom;
  TextEditingController alertTimeController = TextEditingController(text: '$alertTime');

  String getAutoConfigValue(AutoConfig value) {
    switch(value) {
      case AutoConfig.Custom: return 'Custom'; break;
      case AutoConfig.RefrigeratedStorage: return 'Refrigerated storage'; break;
      case AutoConfig.RoomTemperature: return 'Room temperature'; break;
      default: return '';
    }
  }

  String getTemperatureLabel(double value) {
    return '${value.round() / 100} ℃';
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('Auto config', style: TextStyle(fontWeight: FontWeight.bold)),
        DropdownButton<AutoConfig>(
          value: autoConfig,
          icon: Icon(Icons.arrow_drop_down),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.black),
          underline: Container(
            height: 2,
            color: Colors.grey,
          ),
          onChanged: (AutoConfig newValue) {
            setState(() {
              autoConfig = newValue;
            });
          },
          items: <AutoConfig>[AutoConfig.Custom, AutoConfig.RefrigeratedStorage, AutoConfig.RoomTemperature]
              .map<DropdownMenuItem<AutoConfig>>((AutoConfig value) {
            return DropdownMenuItem<AutoConfig>(
              value: value,
              child: Text(getAutoConfigValue(value)),
            );
          }).toList(),
        ),
        SizedBox(height: 20.0),
        Text('Low temperature threshold ${getTemperatureLabel(thresholdTemperatureLow)}', style: TextStyle(fontWeight: FontWeight.bold)),
        Slider(
          value: thresholdTemperatureLow,
          min: -2000,
          max: 3000,
          divisions: 100,
          label: getTemperatureLabel(thresholdTemperatureLow),
          onChanged: (double value) {
            setState(() {
              thresholdTemperatureLow = value;
            });
          },
        ),
        SizedBox(height: 20.0),
        Text('High temperature threshold ${getTemperatureLabel(thresholdTemperatureHigh)}', style: TextStyle(fontWeight: FontWeight.bold)),
        Slider(
          value: thresholdTemperatureHigh,
          min: -2000,
          max: 3000,
          divisions: 100,
          label: getTemperatureLabel(thresholdTemperatureHigh),
          onChanged: (double value) {
            setState(() {
              thresholdTemperatureHigh = value;
            });
          },
        ),
        SizedBox(height: 20.0),
        Text('Normal temperature threshold ${getTemperatureLabel(thresholdTemperatureNormal)}', style: TextStyle(fontWeight: FontWeight.bold)),
        Slider(
          value: thresholdTemperatureNormal,
          min: -2000,
          max: 3000,
          divisions: 100,
          label: getTemperatureLabel(thresholdTemperatureNormal),
          onChanged: (double value) {
            setState(() {
              thresholdTemperatureNormal = value;
            });
          },
        ),
        SizedBox(height: 20.0),
        Text('Warning alert time', style: TextStyle(fontWeight: FontWeight.bold)),
        TextFormField(
            inputFormatters: <TextInputFormatter>[RangeTextFormatter(min: 1, max: 100)],
            keyboardType: TextInputType.number,
            maxLength: 3,
            controller: alertTimeController,
            onChanged: (value) {
              int parsed = int.tryParse(value);
              if (parsed == null) {
                alertTime = 20;
              } else {
                alertTime = parsed;
              }
            },
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
              border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  borderSide: BorderSide(color: Theme.of(context).accentColor)
              ),
            )
        ),
        SizedBox(height: 20.0),
      ]
    );
  }
}