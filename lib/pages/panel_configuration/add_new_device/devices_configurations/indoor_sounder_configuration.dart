import 'package:crow_panel_quick_setup/models/devices/indoor_sounder/indoor_sounder_config.dart';
import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/material.dart';

double sndrLoud;
bool zoneLed;

Future<IndoorSounderConfig> getIndoorSounderConfiguration(BuildContext context, IndoorSounderConfig initialDeviceConfig) async {
  final formKey = new GlobalKey<FormState>();

  sndrLoud = initialDeviceConfig.sndrLoud.toDouble();
  zoneLed = initialDeviceConfig.zoneLed;

  await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Final device configuration'),
          children: [
            Form(
                key: formKey,
                child:
                Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                        children: [
                          ConfigWidget(),
                          longButtons(
                              "Ok",
                                  () {
                                final form = formKey.currentState;
                                if (form.validate()) {
                                  form.save();
                                  Navigator.pop(context);
                                } else {
                                  print("form is invalid");
                                }
                              },
                              color: Theme.of(context).accentColor),
                        ]
                    )
                )
            )
          ],
        );
      }
  );
  return IndoorSounderConfig(
      sndrLoud: sndrLoud.round(),
      zoneLed: zoneLed,
  );
}

class ConfigWidget extends StatefulWidget {
  ConfigWidget({Key key}) : super(key: key);

  @override
  _ConfigWidgetState createState() => _ConfigWidgetState();
}

class _ConfigWidgetState extends State<ConfigWidget> {
  String getLoudnessLabel(int value) {
    Map<int, String> labels = {0: 'Silence', 1: 'Low', 2: 'Medium', 3: 'Full'};
    return '${labels[value]}';
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 20.0),
          Text('Siren loudness', style: TextStyle(fontWeight: FontWeight.bold)),
          Slider(
            value: sndrLoud,
            min: 0,
            max: 3,
            divisions: 3,
            label: getLoudnessLabel(sndrLoud.round()),
            onChanged: (double value) {
              setState(() {
                sndrLoud = value;
              });
            },
          )
        ]
    );
  }
}