import 'package:crow_panel_quick_setup/models/devices/infrared_motion_detector/infrared_motion_detector_config.dart';
import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/material.dart';

bool zoneLed = false; //LED
int supervision = 0; //0,1,2,3,4,5,6,7 = 7,1,2,4,10,15,20,30 min Supervision
bool petImmuni = false; //Pet
int numPulses = 1; //0,1,2,3 = 1 pulse count,2,3,1 w/o filtering
double gainLevel = 2.0; //0,1,2,3,4

Future<InfraredMotionDetectorConfig> getInfraredMotionDetectorConfiguration(BuildContext context, InfraredMotionDetectorConfig initialDeviceConfig) async {
  final formKey = new GlobalKey<FormState>();

  zoneLed = initialDeviceConfig.zoneLed;
  supervision = initialDeviceConfig.supervision;
  petImmuni = initialDeviceConfig.petImmuni;
  numPulses = initialDeviceConfig.numPulses;
  gainLevel = initialDeviceConfig.gainLevel.toDouble();

  await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Final device configuration'),
          children: [
            Form(
                key: formKey,
                child:
                Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                        children: [
                          ConfigWidget(),
                          longButtons(
                              "Ok",
                                  () {
                                final form = formKey.currentState;
                                if (form.validate()) {
                                  form.save();
                                  Navigator.pop(context);
                                } else {
                                  print("form is invalid");
                                }
                              },
                              color: Theme.of(context).accentColor),
                        ]
                    )
                )
            )
          ],
        );
      }
  );
  return InfraredMotionDetectorConfig(
      gainLevel: gainLevel.round(),
      numPulses: numPulses,
      petImmuni: petImmuni,
      supervision: supervision,
      zoneLed: zoneLed
  );
}

class ConfigWidget extends StatefulWidget {
  ConfigWidget({Key key}) : super(key: key);

  @override
  _ConfigWidgetState createState() => _ConfigWidgetState();
}

class _ConfigWidgetState extends State<ConfigWidget> {
  String getGainLevelLabel(int value) {
    if (value == 0) {
      return '0 - lowest';
    } else if (value == 4) {
      return '4 - highest';
    }
    return '$value';
  }

  String getNumPulsesLabel(int value) {
    if (value == 0) {
      return '1 pulse count';
    } else if (value == 3) {
      return '1 pulse count w/o filtering';
    }
    return '$value pulse counts';
  }

  String getSupervisionLabel(int value) {
    Map<int, int> labels = {0: 7, 1: 1, 2: 2, 3: 4, 4: 10, 5: 15, 6: 20, 7: 30};
    return '${labels[value]} min';
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(height: 20.0),
        Text('Led', style: TextStyle(fontWeight: FontWeight.bold)),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            zoneLed ? Text('Enabled') : Text('Disabled'),
            Switch(
              value: zoneLed,
              onChanged: (bool newValue) {
                setState(() {
                  zoneLed = newValue;
                });
              },
            ),
          ],
        ),
        SizedBox(height: 20.0),
        Text('Pet immune', style: TextStyle(fontWeight: FontWeight.bold)),
        CheckboxListTile(
          checkColor: Colors.white,
          activeColor: Theme.of(context).accentColor,
          contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 4.0),
          title: petImmuni ? Text('On') : Text('Off'),
          value: petImmuni,
          onChanged: (newValue) {
            setState(() {
              petImmuni = newValue;
            });
          },
          controlAffinity: ListTileControlAffinity.trailing,
        ),
        SizedBox(height: 20.0),
        Text('Supervision', style: TextStyle(fontWeight: FontWeight.bold)),
        DropdownButton<int>(
          value: supervision,
          icon: Icon(Icons.arrow_drop_down),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.black),
          underline: Container(
            height: 2,
            color: Colors.grey,
          ),
          onChanged: (int newValue) {
            setState(() {
              supervision = newValue;
            });
          },
          items: <int>[0, 1, 2, 3, 4, 5, 6, 7]
              .map<DropdownMenuItem<int>>((int value) {
            return DropdownMenuItem<int>(
              value: value,
              child: Text(getSupervisionLabel(value)),
            );
          }).toList(),
        ),
        SizedBox(height: 20.0),
        Text('Number of pulses', style: TextStyle(fontWeight: FontWeight.bold)),
        DropdownButton<int>(
          value: numPulses,
          icon: Icon(Icons.arrow_drop_down),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.black),
          underline: Container(
            height: 2,
            color: Colors.grey,
          ),
          onChanged: (int newValue) {
            setState(() {
              numPulses = newValue;
            });
          },
          items: <int>[0, 1, 2, 3]
              .map<DropdownMenuItem<int>>((int value) {
            return DropdownMenuItem<int>(
              value: value,
              child: Text(getNumPulsesLabel(value)),
            );
          }).toList(),
        ),
        SizedBox(height: 20.0),
        Text('Sensitivity', style: TextStyle(fontWeight: FontWeight.bold)),
        Slider(
          value: gainLevel,
          min: 0,
          max: 4,
          divisions: 4,
          label: getGainLevelLabel(gainLevel.round()),
          onChanged: (double value) {
            setState(() {
              gainLevel = value;
            });
          },
        ),
        SizedBox(height: 20.0),
      ]
    );
  }
}