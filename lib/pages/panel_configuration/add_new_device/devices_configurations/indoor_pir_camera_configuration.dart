import 'package:crow_panel_quick_setup/models/devices/indoor_pir_camera/indoor_pir_camera_config.dart';
import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/material.dart';

bool camera = true;
int confTime1p2p53 = 1;
bool contrast = true;
bool diffJpeg = false;
int gainLevel = 2;
int holdOff = 4;
bool infraredLeds = false;
int numPictur = 2;
int numPulses = 1;
bool petImmuni = false;
int pictRate = 1;
int plsDirMode53 = 1;
int quality = 7;
int resColor = 3;
bool sharpness = true;
int time1p2p53 = 1;
int time2p3p53 = 1;
bool zoneLed = true;

Future<IndoorPirCameraConfig> getIndoorPirCameraConfiguration(BuildContext context, IndoorPirCameraConfig initialDeviceConfig) async {
  final formKey = new GlobalKey<FormState>();

  camera = initialDeviceConfig.camera;
  confTime1p2p53 = initialDeviceConfig.confTime1p2p53;
  contrast = initialDeviceConfig.contrast;
  diffJpeg = initialDeviceConfig.diffJpeg;
  gainLevel = initialDeviceConfig.gainLevel;
  holdOff = initialDeviceConfig.holdOff;
  infraredLeds = initialDeviceConfig.infraredLeds;
  numPictur = initialDeviceConfig.numPictur;
  numPulses = initialDeviceConfig.numPulses;
  petImmuni = initialDeviceConfig.petImmuni;
  pictRate = initialDeviceConfig.pictRate;
  plsDirMode53 = initialDeviceConfig.plsDirMode53;
  quality = initialDeviceConfig.quality;
  resColor = initialDeviceConfig.resColor;
  sharpness = initialDeviceConfig.sharpness;
  time1p2p53 = initialDeviceConfig.time1p2p53;
  time2p3p53 = initialDeviceConfig.time2p3p53;
  zoneLed = initialDeviceConfig.zoneLed;

  await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Final device configuration'),
          children: [
            Form(
                key: formKey,
                child:
                Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                        children: [
                          ConfigWidget(),
                          longButtons(
                              "Ok",
                                  () {
                                final form = formKey.currentState;
                                if (form.validate()) {
                                  form.save();
                                  Navigator.pop(context);
                                } else {
                                  print("form is invalid");
                                }
                              },
                              color: Theme.of(context).accentColor),
                        ]
                    )
                )
            )
          ],
        );
      }
  );
  return IndoorPirCameraConfig(
      camera: camera,
      confTime1p2p53: confTime1p2p53,
      contrast: contrast,
      diffJpeg: diffJpeg,
      gainLevel: gainLevel,
      holdOff: holdOff,
      infraredLeds: infraredLeds,
      numPictur: numPictur,
      numPulses: numPulses,
      petImmuni: petImmuni,
      pictRate: pictRate,
      plsDirMode53: plsDirMode53,
      quality: quality,
      resColor: resColor,
      sharpness: sharpness,
      time1p2p53: time1p2p53,
      time2p3p53: time2p3p53,
      zoneLed: zoneLed
  );
}

class ConfigWidget extends StatefulWidget {
  ConfigWidget({Key key}) : super(key: key);

  @override
  _ConfigWidgetState createState() => _ConfigWidgetState();
}

class _ConfigWidgetState extends State<ConfigWidget> {
  String getGainLevelLabel(int value) {
    if (value == 0) {
      return '0 - lowest';
    } else if (value == 4) {
      return '4 - highest';
    }
    return '$value';
  }

  String getConfTimeLabel(int value) {
    return '${value * 0.5}s';
  }

  String getNumPulsesLabel(int value) {
    if (value == 0) {
      return '1 pulse count';
    } else if (value == 3) {
      return '1 pulse count w/o filtering';
    }
    return '$value pulse counts';
  }

  String getplsDirMode53Label(int value) {
    Map<int, String> labels = {0: 'COMP_P_first', 1: 'Any first', 3: 'COMP_N_first'};
    return '${labels[value]}';
  }

  String getPictRate(int value) {
    Map<int, String> labels = {0: '0.3 second', 1: '0.5 second', 2: '1 second', 3: '1.4 second', 4: '2 second', 5: '5 second'};
    return '${labels[value]}';
  }

  String getResColorLabel(int value) {
    Map<int, String> labels = {0: 'QVGA B&W (320x240)', 1: 'VGA B&W (640x480)', 2: 'QVGA color(320x240)', 3: 'VGA color(640x480)'};
    return '${labels[value]}';
  }

  String getHoldOffLabel(int value) {
    return '${value * 15 + 30} second';
  }

  String getJpegQualityLevelLabel(int value) {
    return '${value * 10 + 20}%';
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('Led', style: TextStyle(fontWeight: FontWeight.bold)),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            zoneLed ? Text('Enabled') : Text('Disabled'),
            Switch(
              value: zoneLed,
              onChanged: (bool newValue) {
                setState(() {
                  zoneLed = newValue;
                });
              },
            ),
          ],
        ),
        SizedBox(height: 20.0),
        Text('Camera state', style: TextStyle(fontWeight: FontWeight.bold)),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            camera ? Text('Enabled') : Text('Disabled'),
            Switch(
              value: camera,
              onChanged: (bool newValue) {
                setState(() {
                  camera = newValue;
                });
              },
            ),
          ],
        ),
        SizedBox(height: 20.0),
        Text('Pet immune', style: TextStyle(fontWeight: FontWeight.bold)),
        CheckboxListTile(
          checkColor: Colors.white,
          activeColor: Theme.of(context).accentColor,
          contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 4.0),
          title: petImmuni ? Text('On') : Text('Off'),
          value: petImmuni,
          onChanged: (newValue) {
            setState(() {
              petImmuni = newValue;
            });
          },
          controlAffinity: ListTileControlAffinity.trailing,
        ),
        SizedBox(height: 20.0),
        Text('Pulse direction mode', style: TextStyle(fontWeight: FontWeight.bold)),
        DropdownButton<int>(
          value: plsDirMode53,
          icon: Icon(Icons.arrow_drop_down),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.black),
          underline: Container(
            height: 2,
            color: Colors.grey,
          ),
          onChanged: (int newValue) {
            setState(() {
              plsDirMode53 = newValue;
            });
          },
          items: <int>[0, 3, 1]
              .map<DropdownMenuItem<int>>((int value) {
            return DropdownMenuItem<int>(
              value: value,
              child: Text(getplsDirMode53Label(value)),
            );
          }).toList(),
        ),
        SizedBox(height: 20.0),
        Text('Infra LED', style: TextStyle(fontWeight: FontWeight.bold)),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            infraredLeds ? Text('Enabled') : Text('Disabled'),
            Switch(
              value: infraredLeds,
              onChanged: (bool newValue) {
                setState(() {
                  infraredLeds = newValue;
                });
              },
            ),
          ],
        ),
        SizedBox(height: 20.0),
        Text('Conf time b/w 1st and 2nd pulses: ${getConfTimeLabel(confTime1p2p53)}', style: TextStyle(fontWeight: FontWeight.bold)),
        Slider(
          value: confTime1p2p53.toDouble(),
          min: 1,
          max: 14,
          divisions: 13,
          label: getConfTimeLabel(confTime1p2p53),
          onChanged: (double value) {
            setState(() {
              confTime1p2p53 = value.round();
            });
          },
        ),
        SizedBox(height: 20.0),
        Text('Time b/w 1st and 2nd pulses: ${getConfTimeLabel(time1p2p53)}', style: TextStyle(fontWeight: FontWeight.bold)),
        Slider(
          value: time1p2p53.toDouble(),
          min: 1,
          max: 6,
          divisions: 5,
          label: getConfTimeLabel(time1p2p53),
          onChanged: (double value) {
            setState(() {
              time1p2p53 = value.round();
            });
          },
        ),
        SizedBox(height: 20.0),
        Text('Time b/w 2nd and 3rd pulses: ${getConfTimeLabel(time2p3p53)}', style: TextStyle(fontWeight: FontWeight.bold)),
        Slider(
          value: time2p3p53.toDouble(),
          min: 1,
          max: 4,
          divisions: 3,
          label: getConfTimeLabel(time2p3p53),
          onChanged: (double value) {
            setState(() {
              time2p3p53 = value.round();
            });
          },
        ),
        SizedBox(height: 20.0),
        Text('Pictures in set: ${numPictur + 1}', style: TextStyle(fontWeight: FontWeight.bold)),
        Slider(
          value: numPictur.toDouble(),
          min: 0,
          max: 4,
          divisions: 4,
          label: '${numPictur + 1}',
          onChanged: (double value) {
            setState(() {
              numPictur = value.round();
            });
          },
        ),
        SizedBox(height: 20.0),
        Text('Number of pulses', style: TextStyle(fontWeight: FontWeight.bold)),
        DropdownButton<int>(
          value: numPulses,
          icon: Icon(Icons.arrow_drop_down),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.black),
          underline: Container(
            height: 2,
            color: Colors.grey,
          ),
          onChanged: (int newValue) {
            setState(() {
              numPulses = newValue;
            });
          },
          items: <int>[0, 1, 2, 3]
              .map<DropdownMenuItem<int>>((int value) {
            return DropdownMenuItem<int>(
              value: value,
              child: Text(getNumPulsesLabel(value)),
            );
          }).toList(),
        ),
        SizedBox(height: 20.0),
        Text('Sensitivity: ${getGainLevelLabel(gainLevel)}', style: TextStyle(fontWeight: FontWeight.bold)),
        Slider(
          value: gainLevel.toDouble(),
          min: 0,
          max: 4,
          divisions: 4,
          label: getGainLevelLabel(gainLevel),
          onChanged: (double value) {
            setState(() {
              gainLevel = value.round();
            });
          },
        ),
        SizedBox(height: 20.0),
        Text('JPEG Mode', style: TextStyle(fontWeight: FontWeight.bold)),
        DropdownButton<bool>(
          value: diffJpeg,
          icon: Icon(Icons.arrow_drop_down),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.black),
          underline: Container(
            height: 2,
            color: Colors.grey,
          ),
          onChanged: (bool newValue) {
            setState(() {
              diffJpeg = newValue;
            });
          },
          items: <bool>[true, false]
              .map<DropdownMenuItem<bool>>((bool value) {
            return DropdownMenuItem<bool>(
              value: value,
              child: Text(value ? 'Differential JPEG' : 'Regular JPEG'),
            );
          }).toList(),
        ),
        SizedBox(height: 20.0),
        Text('Picture rate: ${getPictRate(pictRate)}', style: TextStyle(fontWeight: FontWeight.bold)),
        Slider(
          value: pictRate.toDouble(),
          min: 0,
          max: 5,
          divisions: 5,
          label: getPictRate(pictRate),
          onChanged: (double value) {
            setState(() {
              pictRate = value.round();
            });
          },
        ),
        SizedBox(height: 20.0),
        Text('Hold off time: ${getHoldOffLabel(holdOff)}', style: TextStyle(fontWeight: FontWeight.bold)),
        Slider(
          value: holdOff.toDouble(),
          min: 0,
          max: 6,
          divisions: 6,
          label: getHoldOffLabel(holdOff),
          onChanged: (double value) {
            setState(() {
              holdOff = value.round();
            });
          },
        ),
        SizedBox(height: 20.0),
        Text('Contrast enhancement', style: TextStyle(fontWeight: FontWeight.bold)),
        CheckboxListTile(
          checkColor: Colors.white,
          activeColor: Theme.of(context).accentColor,
          contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 4.0),
          title: contrast ? Text('On (Auto)') : Text('Off'),
          value: contrast,
          onChanged: (newValue) {
            setState(() {
              contrast = newValue;
            });
          },
          controlAffinity: ListTileControlAffinity.trailing,
        ),
        SizedBox(height: 20.0),
        Text('Sharpness enhancement', style: TextStyle(fontWeight: FontWeight.bold)),
        CheckboxListTile(
          checkColor: Colors.white,
          activeColor: Theme.of(context).accentColor,
          contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 4.0),
          title: sharpness ? Text('On') : Text('Off'),
          value: sharpness,
          onChanged: (newValue) {
            setState(() {
              sharpness = newValue;
            });
          },
          controlAffinity: ListTileControlAffinity.trailing,
        ),
        SizedBox(height: 20.0),
        Text('JPEG Quality level: ${getJpegQualityLevelLabel(quality)}', style: TextStyle(fontWeight: FontWeight.bold)),
        Slider(
          value: quality.toDouble(),
          min: 0,
          max: 7,
          divisions: 7,
          label: getJpegQualityLevelLabel(quality),
          onChanged: (double value) {
            setState(() {
              quality = value.round();
            });
          },
        ),
        SizedBox(height: 20.0),
        Text('Picture resolution and color: ${getResColorLabel(resColor)}', style: TextStyle(fontWeight: FontWeight.bold)),
        DropdownButton<int>(
          value: resColor,
          icon: Icon(Icons.arrow_drop_down),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.black),
          underline: Container(
            height: 2,
            color: Colors.grey,
          ),
          onChanged: (int newValue) {
            setState(() {
              resColor = newValue;
            });
          },
          items: <int>[0, 1, 2, 3]
              .map<DropdownMenuItem<int>>((int value) {
            return DropdownMenuItem<int>(
              value: value,
              child: Text('${getResColorLabel(value)}'),
            );
          }).toList(),
        ),
        SizedBox(height: 20.0),
      ]
    );
  }
}