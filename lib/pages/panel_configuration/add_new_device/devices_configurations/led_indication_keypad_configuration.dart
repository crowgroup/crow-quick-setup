import 'package:crow_panel_quick_setup/models/devices/led_indication_keypad/led_indication_keypad_config.dart';
import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/material.dart';

double kpdLoud = 2.0;

Future<LedIndicationKeypadConfig> getLedIndicationKeypadConfiguration(BuildContext context, LedIndicationKeypadConfig initialDeviceConfig) async {
  final formKey = new GlobalKey<FormState>();
  kpdLoud = initialDeviceConfig.kpdLoud.toDouble();

  await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Final device configuration'),
          children: [
            Form(
                key: formKey,
                child:
                Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                        children: [
                          LedIndicationKeypadConfigWidget(),
                          SizedBox(height: 20.0),
                          longButtons(
                              "Ok",
                                  () {
                                final form = formKey.currentState;
                                if (form.validate()) {
                                  form.save();
                                  Navigator.pop(context);
                                } else {
                                  print("form is invalid");
                                }
                              },
                              color: Theme.of(context).accentColor),
                        ]
                    )
                )
            )
          ],
        );
      }
  );
  return LedIndicationKeypadConfig(kpdLoud: kpdLoud.round());
}

class LedIndicationKeypadConfigWidget extends StatefulWidget {
  LedIndicationKeypadConfigWidget({Key key}) : super(key: key);

  @override
  _LedIndicationKeypadConfigWidgetState createState() => _LedIndicationKeypadConfigWidgetState();
}

class _LedIndicationKeypadConfigWidgetState extends State<LedIndicationKeypadConfigWidget> {
  String getLoudnessLabel(int value) {
    Map<int, String> labels = {0: 'Silence', 1: 'Low', 2: 'Medium', 3: 'Full'};
    return '${labels[value]}';
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(height: 20.0),
        Text('Keypad loudness', style: TextStyle(fontWeight: FontWeight.bold)),
        Slider(
          value: kpdLoud,
          min: 0,
          max: 3,
          divisions: 3,
          label: getLoudnessLabel(kpdLoud.round()),
          onChanged: (double value) {
            setState(() {
              kpdLoud = value;
            });
          },
        )
      ]
    );
  }
}