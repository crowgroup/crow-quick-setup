import 'package:crow_panel_quick_setup/models/devices/smart_plug/smart_plug_config.dart';
import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/material.dart';

int outSupervision; //"out_supervision": 0, Supervision
bool blockManualSwitch; //"block_manual_switch": false Disabled Block Manual Switch
int powerUpOutputState; //"power_up_output_state": 0, 0 = Last output state,1 = state ON,2 = state OFF Power up output state

Future<SmartPlugConfig> getSmartPlugConfiguration(BuildContext context, SmartPlugConfig initialDeviceConfig) async {
  final formKey = new GlobalKey<FormState>();

  outSupervision = initialDeviceConfig.outSupervision;
  blockManualSwitch = initialDeviceConfig.blockManualSwitch;
  powerUpOutputState = initialDeviceConfig.powerUpOutputState;

  await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Final device configuration'),
          children: [
            Form(
                key: formKey,
                child:
                Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                        children: [
                          ConfigWidget(),
                          longButtons(
                              "Ok",
                                  () {
                                final form = formKey.currentState;
                                if (form.validate()) {
                                  form.save();
                                  Navigator.pop(context);
                                } else {
                                  print("form is invalid");
                                }
                              },
                              color: Theme.of(context).accentColor),
                        ]
                    )
                )
            )
          ],
        );
      }
  );
  return SmartPlugConfig(
      outSupervision: outSupervision,
      blockManualSwitch: blockManualSwitch,
      powerUpOutputState: powerUpOutputState
  );
}

class ConfigWidget extends StatefulWidget {
  ConfigWidget({Key key}) : super(key: key);

  @override
  _ConfigWidgetState createState() => _ConfigWidgetState();
}

class _ConfigWidgetState extends State<ConfigWidget> {
  String getSupervisionLabel(int value) {
    Map<int, int> labels = {0: 7, 1: 1, 2: 2, 3: 4, 4: 10, 5: 15, 6: 20, 7: 30};
    return '${labels[value]} min';
  }

  String getPowerUpOutputStateLabel(int value) {
    Map<int, String> labels = {0: 'Last output state', 1: 'State ON', 2: 'State OFF'};
    return labels[value];
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Supervision', style: TextStyle(fontWeight: FontWeight.bold)),
          DropdownButton<int>(
            value: outSupervision,
            icon: Icon(Icons.arrow_drop_down),
            iconSize: 24,
            elevation: 16,
            style: TextStyle(color: Colors.black),
            underline: Container(
              height: 2,
              color: Colors.grey,
            ),
            onChanged: (int newValue) {
              setState(() {
                outSupervision = newValue;
              });
            },
            items: <int>[0, 1, 2, 3, 4, 5, 6, 7]
                .map<DropdownMenuItem<int>>((int value) {
              return DropdownMenuItem<int>(
                value: value,
                child: Text(getSupervisionLabel(value)),
              );
            }).toList(),
          ),
          SizedBox(height: 20.0),
          Text('Block manual switch', style: TextStyle(fontWeight: FontWeight.bold)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              blockManualSwitch ? Text('Enabled') : Text('Disabled'),
              Switch(
                value: blockManualSwitch,
                onChanged: (bool newValue) {
                  setState(() {
                    blockManualSwitch = newValue;
                  });
                },
              ),
            ],
          ),
          SizedBox(height: 20.0),
          Text('Power up output state', style: TextStyle(fontWeight: FontWeight.bold)),
          DropdownButton<int>(
            value: powerUpOutputState,
            icon: Icon(Icons.arrow_drop_down),
            iconSize: 24,
            elevation: 16,
            style: TextStyle(color: Colors.black),
            underline: Container(
              height: 2,
              color: Colors.grey,
            ),
            onChanged: (int newValue) {
              setState(() {
                powerUpOutputState = newValue;
              });
            },
            items: <int>[0, 1, 2]
                .map<DropdownMenuItem<int>>((int value) {
              return DropdownMenuItem<int>(
                value: value,
                child: Text(getPowerUpOutputStateLabel(value)),
              );
            }).toList(),
          ),
          SizedBox(height: 20.0),
        ]
    );
  }
}