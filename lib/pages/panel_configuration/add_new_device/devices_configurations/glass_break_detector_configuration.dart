import 'package:crow_panel_quick_setup/models/devices/glass_break_detector/glass_break_detector_config.dart';
import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/material.dart';

bool zoneLed = false;
int supervision = 0;

Future<GlassBreakDetectorConfig> getGlassBreakDetectorConfiguration(BuildContext context, GlassBreakDetectorConfig initialDeviceConfig) async {
  final formKey = new GlobalKey<FormState>();

  zoneLed = initialDeviceConfig.zoneLed;
  supervision = initialDeviceConfig.supervision;

  await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Final device configuration'),
          children: [
            Form(
                key: formKey,
                child:
                Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                        children: [
                          ConfigWidget(),
                          longButtons(
                              "Ok",
                                  () {
                                final form = formKey.currentState;
                                if (form.validate()) {
                                  form.save();
                                  Navigator.pop(context);
                                } else {
                                  print("form is invalid");
                                }
                              },
                              color: Theme.of(context).accentColor),
                        ]
                    )
                )
            )
          ],
        );
      }
  );
  return GlassBreakDetectorConfig(
      supervision: supervision,
      zoneLed: zoneLed
  );
}

class ConfigWidget extends StatefulWidget {
  ConfigWidget({Key key}) : super(key: key);

  @override
  _ConfigWidgetState createState() => _ConfigWidgetState();
}

class _ConfigWidgetState extends State<ConfigWidget> {

  String getSupervisionLabel(int value) {
    Map<int, int> labels = {0: 7, 1: 1, 2: 2, 3: 4, 4: 10, 5: 15, 6: 20, 7: 30};
    return '${labels[value]} min';
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(height: 20.0),
        Text('Led', style: TextStyle(fontWeight: FontWeight.bold)),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            zoneLed ? Text('Enabled') : Text('Disabled'),
            Switch(
              value: zoneLed,
              onChanged: (bool newValue) {
                setState(() {
                  zoneLed = newValue;
                });
              },
            ),
          ],
        ),
        SizedBox(height: 20.0),
        Text('Supervision', style: TextStyle(fontWeight: FontWeight.bold)),
        DropdownButton<int>(
          value: supervision,
          icon: Icon(Icons.arrow_drop_down),
          iconSize: 24,
          elevation: 16,
          style: TextStyle(color: Colors.black),
          underline: Container(
            height: 2,
            color: Colors.grey,
          ),
          onChanged: (int newValue) {
            setState(() {
              supervision = newValue;
            });
          },
          items: <int>[0, 1, 2, 3, 4, 5, 6, 7]
              .map<DropdownMenuItem<int>>((int value) {
            return DropdownMenuItem<int>(
              value: value,
              child: Text(getSupervisionLabel(value)),
            );
          }).toList(),
        ),
        SizedBox(height: 20.0),
      ]
    );
  }
}