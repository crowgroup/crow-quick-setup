import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/material.dart';

Future<String> getDeviceName(BuildContext context) async {
  final formKey = new GlobalKey<FormState>();
  TextEditingController nameController = new TextEditingController(text: 'Default name');

  final nameField = TextFormField(
      maxLength: 16,
      controller: nameController,
      autofocus: true,
      validator: (value) => value.isEmpty ? "Input device name first" : null,
      decoration: InputDecoration(
        hintText: "Input device name here",
        contentPadding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(color: Theme.of(context).accentColor)
        ),
      )
  );

  await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Input device name, please'),
          children: [
            Form(
                key: formKey,
                child:
                Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                        children: [
                          nameField,
                          SizedBox(height: 20.0),
                          longButtons(
                              "Ok",
                                  () {
                                final form = formKey.currentState;
                                if (form.validate()) {
                                  form.save();
                                  Navigator.pop(context);
                                } else {
                                  print("form is invalid");
                                }
                              },
                              color: Theme.of(context).accentColor),
                        ]
                    )
                )
            )
          ],
        );
      }
  );
  return nameController.text;
}