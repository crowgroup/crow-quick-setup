import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/material.dart';

bool switchAnd = false;

Future<bool> getDeviceSwitchAnd(BuildContext context) async {
  final formKey = new GlobalKey<FormState>();
  switchAnd = false;

  await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Choose device logic of switches, please'),
          children: [
            Form(
                key: formKey,
                child:
                Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                        children: [
                          ConfigWidget(),
                          longButtons(
                              "Ok",
                                  () {
                                final form = formKey.currentState;
                                if (form.validate()) {
                                  form.save();
                                  Navigator.pop(context);
                                } else {
                                  print("form is invalid");
                                }
                              },
                              color: Theme.of(context).accentColor),
                        ]
                    )
                )
            )
          ],
        );
      }
  );
  return switchAnd;
}

class ConfigWidget extends StatefulWidget {
  ConfigWidget({Key key}) : super(key: key);

  @override
  _ConfigWidgetState createState() => _ConfigWidgetState();
}

class _ConfigWidgetState extends State<ConfigWidget> {
  String getSupervisionLabel(int value) {
    Map<int, int> labels = {0: 7, 1: 1, 2: 2, 3: 4, 4: 10, 5: 15, 6: 20, 7: 30};
    return '${labels[value]} min';
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 20.0),
          Text('Logic of Switches', style: TextStyle(fontWeight: FontWeight.bold)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              switchAnd ? Text('Logical AND') : Text('Logical OR'),
              Switch(
                value: switchAnd,
                onChanged: (bool newValue) {
                  setState(() {
                    switchAnd = newValue;
                  });
                },
              ),
            ],
          ),
          SizedBox(height: 20.0),
        ]
    );
  }
}