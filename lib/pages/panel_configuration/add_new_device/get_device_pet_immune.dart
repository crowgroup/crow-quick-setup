import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/material.dart';

bool petImmuni = false;

Future<bool> getDevicePetImmune(BuildContext context) async {
  final formKey = new GlobalKey<FormState>();
  petImmuni = false;

  await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Choose device pet immune, please'),
          children: [
            Form(
                key: formKey,
                child:
                Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                        children: [
                          ConfigWidget(),
                          longButtons(
                              "Ok",
                                  () {
                                final form = formKey.currentState;
                                if (form.validate()) {
                                  form.save();
                                  Navigator.pop(context);
                                } else {
                                  print("form is invalid");
                                }
                              },
                              color: Theme.of(context).accentColor),
                        ]
                    )
                )
            )
          ],
        );
      }
  );
  return petImmuni;
}

class ConfigWidget extends StatefulWidget {
  ConfigWidget({Key key}) : super(key: key);

  @override
  _ConfigWidgetState createState() => _ConfigWidgetState();
}

class _ConfigWidgetState extends State<ConfigWidget> {
  String getSupervisionLabel(int value) {
    Map<int, int> labels = {0: 7, 1: 1, 2: 2, 3: 4, 4: 10, 5: 15, 6: 20, 7: 30};
    return '${labels[value]} min';
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 20.0),
          Text('Pet immune', style: TextStyle(fontWeight: FontWeight.bold)),
          CheckboxListTile(
            checkColor: Colors.white,
            activeColor: Theme.of(context).accentColor,
            contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 4.0),
            title: petImmuni ? Text('On') : Text('Off'),
            value: petImmuni,
            onChanged: (newValue) {
              setState(() {
                petImmuni = newValue;
              });
            },
            controlAffinity: ListTileControlAffinity.trailing,
          ),
          SizedBox(height: 20.0),
        ]
    );
  }
}