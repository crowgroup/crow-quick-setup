import 'package:crow_panel_quick_setup/models/devices/air_quality_sensor/air_quality_sensor.dart';
import 'package:crow_panel_quick_setup/models/devices/flood_detector/flood_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/glass_break_detector/glass_break_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_pir_camera/indoor_pir_camera.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_sounder/indoor_sounder.dart';
import 'package:crow_panel_quick_setup/models/devices/infrared_motion_detector/infrared_motion_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/led_indication_keypad/led_indication_keypad.dart';
import 'package:crow_panel_quick_setup/models/devices/magnet_contact/magnet_contact.dart';
import 'package:crow_panel_quick_setup/models/devices/smart_plug/smart_plug.dart';
import 'package:crow_panel_quick_setup/models/devices/smoke_detector/smoke_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/temperature_sensor/temperature_sensor.dart';
import 'package:crow_panel_quick_setup/models/devices/verification_camera/verification_camera.dart';
import 'package:flutter/material.dart';

class Device {
  String title;
  String subtitle;
  String imageUrl;
  int type;

  Device({this.title, this.subtitle, this.imageUrl, this.type});
}

Future<int> getDeviceType(BuildContext context) async {
  int type = -1;

  FocusScope.of(context).requestFocus(new FocusNode());

  List<Device> devices = [
    Device(title: LedIndicationKeypad.title, subtitle: LedIndicationKeypad.subtitle, imageUrl: LedIndicationKeypad.imageUrl, type: LedIndicationKeypad.type),
    Device(title: MagnetContact.title, subtitle: MagnetContact.subtitle, imageUrl: MagnetContact.imageUrl, type: MagnetContact.type),
    Device(title: InfraredMotionDetector.title, subtitle: InfraredMotionDetector.subtitle, imageUrl: InfraredMotionDetector.imageUrl, type: InfraredMotionDetector.type),
    Device(title: IndoorPirCamera.title, subtitle: IndoorPirCamera.subtitle, imageUrl: IndoorPirCamera.imageUrl, type: IndoorPirCamera.type),
    Device(title: SmartPlug.title, subtitle: SmartPlug.subtitle, imageUrl: SmartPlug.imageUrl, type: SmartPlug.type),
    Device(title: IndoorSounder.title, subtitle: IndoorSounder.subtitle, imageUrl: IndoorSounder.imageUrl, type: IndoorSounder.type),
    Device(title: SmokeDetector.title, subtitle: SmokeDetector.subtitle, imageUrl: SmokeDetector.imageUrl, type: SmokeDetector.type),
    Device(title: FloodDetector.title, subtitle: FloodDetector.subtitle, imageUrl: FloodDetector.imageUrl, type: FloodDetector.type),
    Device(title: GlassBreakDetector.title, subtitle: GlassBreakDetector.subtitle, imageUrl: GlassBreakDetector.imageUrl, type: GlassBreakDetector.type),
    Device(title: AirQualitySensor.title, subtitle: AirQualitySensor.subtitle, imageUrl: AirQualitySensor.imageUrl, type: AirQualitySensor.type),
    Device(title: VerificationCamera.title, subtitle: VerificationCamera.subtitle, imageUrl: VerificationCamera.imageUrl, type: VerificationCamera.type),
    Device(title: TemperatureSensor.title, subtitle: TemperatureSensor.subtitle, imageUrl: TemperatureSensor.imageUrl, type: TemperatureSensor.type),
  ];

  await showDialog<String>(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('Choose a device type, please'),
        content: Container(
          height: MediaQuery.of(context).size.height * 1,
          width: MediaQuery.of(context).size.width * 1,
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: devices.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                onTap: () {
                  type = devices[index].type;
                  Navigator.pop(context);
                },
                leading: Image(
                    image: AssetImage(
                      devices[index].imageUrl,
                    ),
                    fit: BoxFit.contain,
                    height: 80,
                    width: 80
                ),
                title: Text(devices[index].title),
                subtitle: Text(devices[index].subtitle),
              );
            },
          ),
        )
      );
    }
  );
  return type;
}
