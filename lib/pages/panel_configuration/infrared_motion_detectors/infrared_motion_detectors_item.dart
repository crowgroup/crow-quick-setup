import 'package:crow_panel_quick_setup/models/devices/infrared_motion_detector/infrared_motion_detector.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/infrared_motion_detectors/popup_menu.dart';
import 'package:flutter/material.dart';

class InfraredMotionDetectorsItem extends StatelessWidget {
  InfraredMotionDetectorsItem({
    Key key,
    this.infraredMotionDetector
  }) : super(key: key);

  final InfraredMotionDetector infraredMotionDetector;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
      child: SizedBox(
        height: 100,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 1.0,
              child: Container(
                decoration: BoxDecoration(
                  color: const Color(0xff7c94b6),
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage(InfraredMotionDetector.imageUrl),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                  padding: const EdgeInsets.fromLTRB(10.0, 8.0, 2.0, 8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("ID: ${infraredMotionDetector.slotId + 1}"),
                      Text("Serial number: ${infraredMotionDetector.serialNumber}"),
                      Text("Name: ${infraredMotionDetector.name}")
                    ],
                  )
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
              child:
              PopupMenu(infraredMotionDetector: infraredMotionDetector),
            ),
          ],
        ),
      )
    );
  }
}