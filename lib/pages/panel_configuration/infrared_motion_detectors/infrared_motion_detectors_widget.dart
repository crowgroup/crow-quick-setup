import 'package:crow_panel_quick_setup/models/devices/infrared_motion_detector/infrared_motion_detector.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/infrared_motion_detectors/infrared_motion_detectors_item.dart';
import 'package:flutter/material.dart';

class InfraredMotionDetectorsWidget extends StatelessWidget {
  InfraredMotionDetectorsWidget({
    Key key,
    this.infraredMotionDetectors
  }) : super(key: key);

  final List<InfraredMotionDetector> infraredMotionDetectors;

  @override
  Widget build(BuildContext context) {
    return infraredMotionDetectors.length > 0 ? Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          child: Text(
            "Infrared Motion Detectors",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        ),
        Container(child: GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > 0) {
                print('settings');
              } else if (details.delta.dx < 0) {
                print('delete');
              }
            },
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: infraredMotionDetectors.length,
                itemBuilder: (BuildContext context, int index) {
                  return InfraredMotionDetectorsItem(infraredMotionDetector: infraredMotionDetectors[index]);
                }
            ),
          )
        ),
        SizedBox(height: 20.0),
      ]
    ) : Column();
  }
}