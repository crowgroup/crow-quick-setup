import 'package:crow_panel_quick_setup/models/devices/indoor_pir_camera/indoor_pir_camera.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/indoor_pir_cameras/indoor_pir_cameras_item.dart';
import 'package:flutter/material.dart';

class IndoorPirCamerasWidget extends StatelessWidget {
  IndoorPirCamerasWidget({
    Key key,
    this.indoorPirCameras
  }) : super(key: key);

  final List<IndoorPirCamera> indoorPirCameras;

  @override
  Widget build(BuildContext context) {
    return indoorPirCameras.length > 0 ? Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          child: Text(
            "Indoor Pir Cameras",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        ),
        Container(child: GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > 0) {
                print('settings');
              } else if (details.delta.dx < 0) {
                print('delete');
              }
            },
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: indoorPirCameras.length,
                itemBuilder: (BuildContext context, int index) {
                  return IndoorPirCamerasItem(indoorPirCamera: indoorPirCameras[index]);
                }
            ),
          )
        ),
        SizedBox(height: 20.0),
      ]
    ) : Column();
  }
}