import 'package:crow_panel_quick_setup/models/panel.dart';

class PanelConfigurationArguments {
  final Panel panel;
  final String installerCode;

  PanelConfigurationArguments(this.panel, this.installerCode);
}