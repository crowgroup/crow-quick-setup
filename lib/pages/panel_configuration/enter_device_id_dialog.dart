import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

Future<String> enterDeviceIdDialog(BuildContext context) async {
  final formKey = new GlobalKey<FormState>();
  String deviceId = '';
  TextEditingController deviceIdController = new TextEditingController();
  final passwordField = TextFormField(
    keyboardType: TextInputType.number,
    maxLength: 7,
    controller: deviceIdController,
    autofocus: true,
    validator: (value) => value.isEmpty ? "Input device id first" : null,
    onSaved: (value) => deviceId = value,
    decoration: InputDecoration(
      hintText: "Input device id here",
      contentPadding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(color: Theme.of(context).accentColor)
      ),
      suffixIcon: IconButton(
        icon: Icon(Icons.qr_code, color: Theme.of(context).accentColor),
        onPressed: () async {
          deviceIdController.text = await scanQR();
        }),
    )
  );
  await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Input device id, please'),
          children: [
            Form(
                key: formKey,
                child:
                Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                        children: [
                          passwordField,
                          SizedBox(height: 20.0),
                          longButtons(
                              "Ok",
                                  () {
                                final form = formKey.currentState;
                                if (form.validate()) {
                                  form.save();
                                  Navigator.pop(context);
                                } else {
                                  print("form is invalid");
                                }
                              },
                              color: Theme.of(context).accentColor),
                        ]
                    )
                )
            )
          ],
        );
      }
  );
  return deviceId;
}


Future<String> scanQR() async {
  String deviceId = '';
  String barcodeScanRes = '';
  try {
    barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
        "#FFFDDD", "Cancel", true, ScanMode.QR);
    List<String> barcodeScanResSplitted = barcodeScanRes.split('-');
    deviceId = barcodeScanResSplitted.last.length > 7 ? barcodeScanResSplitted.last.substring(3) : barcodeScanResSplitted.last;
  } on PlatformException {
    barcodeScanRes = 'Failed to get platform version.';
  }
  return deviceId;
}