import 'package:crow_panel_quick_setup/models/devices/verification_camera/verification_camera.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/verification_cameras/verification_cameras_item.dart';
import 'package:flutter/material.dart';

class VerificationCamerasWidget extends StatelessWidget {
  VerificationCamerasWidget({
    Key key,
    this.verificationCameras
  }) : super(key: key);

  final List<VerificationCamera> verificationCameras;

  @override
  Widget build(BuildContext context) {
    return verificationCameras.length > 0 ? Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          child: Text(
            "Verification Cameras",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        ),
        Container(child: GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > 0) {
                print('settings');
              } else if (details.delta.dx < 0) {
                print('delete');
              }
            },
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: verificationCameras.length,
                itemBuilder: (BuildContext context, int index) {
                  return VerificationCamerasItem(verificationCamera: verificationCameras[index]);
                }
            ),
          )
        ),
        SizedBox(height: 20.0),
      ]
    ) : Column();
  }
}