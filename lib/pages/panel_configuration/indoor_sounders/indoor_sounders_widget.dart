import 'package:crow_panel_quick_setup/models/devices/indoor_sounder/indoor_sounder.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/indoor_sounders/indoor_sounders_item.dart';
import 'package:flutter/material.dart';

class IndoorSoundersWidget extends StatelessWidget {
  IndoorSoundersWidget({
    Key key,
    this.indoorSounders
  }) : super(key: key);

  final List<IndoorSounder> indoorSounders;

  @override
  Widget build(BuildContext context) {
    return indoorSounders.length > 0 ? Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          child: Text(
            "Indoor Sounders",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        ),
        Container(child: GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > 0) {
                print('settings');
              } else if (details.delta.dx < 0) {
                print('delete');
              }
            },
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: indoorSounders.length,
                itemBuilder: (BuildContext context, int index) {
                  return IndoorSoundersItem(indoorSounder: indoorSounders[index]);
                }
            ),
          )
        ),
        SizedBox(height: 20.0),
      ]
    ) : Column();
  }
}