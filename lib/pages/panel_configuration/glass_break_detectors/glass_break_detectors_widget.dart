import 'package:crow_panel_quick_setup/models/devices/glass_break_detector/glass_break_detector.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/glass_break_detectors/glass_break_detectors_item.dart';
import 'package:flutter/material.dart';

class GlassBreakDetectorsWidget extends StatelessWidget {
  GlassBreakDetectorsWidget({
    Key key,
    this.glassBreakDetectors
  }) : super(key: key);

  final List<GlassBreakDetector> glassBreakDetectors;

  @override
  Widget build(BuildContext context) {
    return glassBreakDetectors.length > 0 ? Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          child: Text(
            "Glass Break Detectors",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        ),
        Container(child: GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > 0) {
                print('settings');
              } else if (details.delta.dx < 0) {
                print('delete');
              }
            },
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: glassBreakDetectors.length,
                itemBuilder: (BuildContext context, int index) {
                  return GlassBreakDetectorsItem( glassBreakDetector: glassBreakDetectors[index]);
                }
            ),
          )
        ),
        SizedBox(height: 20.0),
      ]
    ) : Column();
  }
}