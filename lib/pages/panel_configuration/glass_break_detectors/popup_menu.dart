import 'package:crow_panel_quick_setup/models/devices/glass_break_detector/glass_break_detector.dart';
import 'package:crow_panel_quick_setup/providers/panel_configuration_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PopupMenu extends StatelessWidget {
  PopupMenu({
    Key key,
    this.glassBreakDetector
  }) : super(key: key);

  final GlassBreakDetector glassBreakDetector;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      onSelected: (value) {
        if (value == 'delete') {
          PanelsConfigurationProvider panelsConfigurationProvider = Provider.of<PanelsConfigurationProvider>(context, listen: false);
          glassBreakDetector.delete(panelsConfigurationProvider.panelConfiguration.rawConfig);
          panelsConfigurationProvider.savePanelConfiguration();
        }
      },
      itemBuilder: (context) => [
        PopupMenuItem(
            value: 'delete',
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                  child: Icon(Icons.close),
                ),
                Text('Delete detector')
              ],
            )
        ),
      ]
    );
  }
}