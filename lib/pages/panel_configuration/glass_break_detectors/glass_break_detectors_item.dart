import 'package:crow_panel_quick_setup/models/devices/glass_break_detector/glass_break_detector.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/glass_break_detectors/popup_menu.dart';
import 'package:flutter/material.dart';

class GlassBreakDetectorsItem extends StatelessWidget {
  GlassBreakDetectorsItem({
    Key key,
    this.glassBreakDetector
  }) : super(key: key);

  final GlassBreakDetector glassBreakDetector;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
      child: SizedBox(
        height: 100,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 1.0,
              child: Container(
                decoration: BoxDecoration(
                  color: const Color(0xff7c94b6),
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage(GlassBreakDetector.imageUrl),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                  padding: const EdgeInsets.fromLTRB(10.0, 8.0, 2.0, 8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("ID: ${glassBreakDetector.slotId + 1}"),
                      Text("Serial number: ${glassBreakDetector.serialNumber}"),
                      Text("Name: ${glassBreakDetector.name}")
                    ],
                  )
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
              child:
              PopupMenu(glassBreakDetector: glassBreakDetector),
            ),
          ],
        ),
      )
    );
  }
}