import 'package:crow_panel_quick_setup/models/devices/smoke_detector/smoke_detector.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/smoke_detectors/smoke_detectors_item.dart';
import 'package:flutter/material.dart';

class SmokeDetectorsWidget extends StatelessWidget {
  SmokeDetectorsWidget({
    Key key,
    this.smokeDetectors
  }) : super(key: key);

  final List<SmokeDetector> smokeDetectors;

  @override
  Widget build(BuildContext context) {
    return smokeDetectors.length > 0 ? Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          child: Text(
            "Smoke Detectors",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        ),
        Container(child: GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > 0) {
                print('settings');
              } else if (details.delta.dx < 0) {
                print('delete');
              }
            },
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: smokeDetectors.length,
                itemBuilder: (BuildContext context, int index) {
                  return SmokeDetectorsItem(smokeDetector: smokeDetectors[index]);
                }
            ),
          )
        ),
        SizedBox(height: 20.0),
      ]
    ) : Column();
  }
}