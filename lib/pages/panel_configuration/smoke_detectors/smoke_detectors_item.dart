import 'package:crow_panel_quick_setup/models/devices/smoke_detector/smoke_detector.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/smoke_detectors/popup_menu.dart';
import 'package:flutter/material.dart';

class SmokeDetectorsItem extends StatelessWidget {
  SmokeDetectorsItem({
    Key key,
    this.smokeDetector
  }) : super(key: key);

  final SmokeDetector smokeDetector;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
      child: SizedBox(
        height: 100,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 1.0,
              child: Container(
                decoration: BoxDecoration(
                  color: const Color(0xff7c94b6),
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage(SmokeDetector.imageUrl),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                  padding: const EdgeInsets.fromLTRB(10.0, 8.0, 2.0, 8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("ID: ${smokeDetector.slotId + 1}"),
                      Text("Serial number: ${smokeDetector.serialNumber}"),
                      Text("Name: ${smokeDetector.name}")
                    ],
                  )
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
              child:
              PopupMenu(smokeDetector: smokeDetector),
            ),
          ],
        ),
      )
    );
  }
}