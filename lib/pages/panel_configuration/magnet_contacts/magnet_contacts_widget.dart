import 'package:crow_panel_quick_setup/models/devices/magnet_contact/magnet_contact.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/magnet_contacts/magnet_contacts_item.dart';
import 'package:flutter/material.dart';

class MagnetContactsWidget extends StatelessWidget {
  MagnetContactsWidget({
    Key key,
    this.magnetContacts
  }) : super(key: key);

  final List<MagnetContact> magnetContacts;

  @override
  Widget build(BuildContext context) {
    return magnetContacts.length > 0 ? Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          child: Text(
            "Magnet Contacts",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        ),
        Container(child: GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > 0) {
                print('settings');
              } else if (details.delta.dx < 0) {
                print('delete');
              }
            },
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: magnetContacts.length,
                itemBuilder: (BuildContext context, int index) {
                  return MagnetContactItem(magnetContact: magnetContacts[index]);
                }
            ),
          )
        ),
        SizedBox(height: 20.0),
      ]
    ) : Column();
  }
}