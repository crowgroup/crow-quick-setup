import 'package:crow_panel_quick_setup/models/devices/magnet_contact/magnet_contact.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/magnet_contacts/popup_menu.dart';
import 'package:flutter/material.dart';

class MagnetContactItem extends StatelessWidget {
  MagnetContactItem({
    Key key,
    this.magnetContact
  }) : super(key: key);

  final MagnetContact magnetContact;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
      child: SizedBox(
        height: 100,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 1.0,
              child: Container(
                decoration: BoxDecoration(
                  color: const Color(0xff7c94b6),
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage(MagnetContact.imageUrl),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                  padding: const EdgeInsets.fromLTRB(10.0, 8.0, 2.0, 8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("ID: ${magnetContact.slotId + 1}"),
                      Text("Serial number: ${magnetContact.serialNumber}"),
                      Text("Name: ${magnetContact.name}")
                    ],
                  )
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
              child:
              PopupMenu(magnetContact: magnetContact),
            ),
          ],
        ),
      )
    );
  }
}