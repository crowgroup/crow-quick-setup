import 'package:crow_panel_quick_setup/models/devices/base_device.dart';
import 'package:crow_panel_quick_setup/models/panel.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/air_quality_sensors/air_quality_sensors_widget.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/flood_detectors/flood_detectors_widget.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/glass_break_detectors/glass_break_detectors_widget.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/indoor_pir_cameras/indoor_pir_cameras_widget.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/indoor_sounders/indoor_sounders_widget.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/infrared_motion_detectors/infrared_motion_detectors_widget.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/led_indication_keypads/led_indication_keypads_widget.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/magnet_contacts/magnet_contacts_widget.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/panel_configuration_arguments.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/smart_plugs/smart_plugs_widget.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/smoke_detectors/smoke_detectors_widget.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/temperature_sensor/temperature_sensor_widget.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/verification_cameras/verification_cameras_widget.dart';
import 'package:crow_panel_quick_setup/providers/panel_configuration_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PanelConfiguration extends StatelessWidget {
  PanelConfiguration({
    Key key,
    this.panel,
    this.installerCode,
  }) : super(key: key);

  Panel panel;
  String installerCode;

  @override
  Widget build(BuildContext context) {
    final PanelConfigurationArguments panelConfigurationArguments = ModalRoute.of(context).settings.arguments;
    panel = panelConfigurationArguments.panel;
    installerCode = panelConfigurationArguments.installerCode;

    Provider.of<PanelsConfigurationProvider>(context, listen: false).getPanelConfiguration(panel.id, installerCode);
    return Scaffold(
        appBar: AppBar(
          title: Column(
            children: [
              Text('Panel configuration'),
              Text('${panel.prettyMac}'),
            ],
          ),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20.0),
              child:
                PopupMenuButton(
                  onSelected: (value) {
                    switch(value) {
                      case 'add': Navigator.pushNamed(context, '/addDevice'); break;
                      case 'changeView': Provider.of<PanelsConfigurationProvider>(context, listen: false).toggleView(); break;
                      case 'removeAllDevices': deleteAllDevice(context); break;
                      default: break;
                    }
                  },
                  itemBuilder: (context) => [
                    PopupMenuItem(
                        value: 'add',
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                              child: Icon(Icons.add, color: Colors.black),
                            ),
                            Text('Add new device')
                          ],
                        )
                    ),
                    PopupMenuItem(
                        value: 'changeView',
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                              child: Icon(Icons.table_rows, color: Colors.black),
                            ),
                            Text('Change view')
                          ],
                        )
                    ),
                    PopupMenuItem(
                        value: 'removeAllDevices',
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                              child: Icon(Icons.delete_sweep, color: Colors.black),
                            ),
                            Text('Remove all devices')
                          ],
                        )
                    ),
                  ]
                )
            ),
          ],
        ),
        // drawer: CommonDrawer(),
        body:
        Consumer<PanelsConfigurationProvider>(builder: (context, panelsConfigurationProvider, child) =>
            getView(panelsConfigurationProvider, context)
        )
    );
  }

  getView(PanelsConfigurationProvider panelsConfigurationProvider, BuildContext context) {
    switch(panelsConfigurationProvider.status) {
      case PanelsConfigurationStatus.Loading: return Center(child: CircularProgressIndicator()); break;
      case PanelsConfigurationStatus.Error: return Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Unknown Error'),
            MaterialButton(
              onPressed: () {
                panelsConfigurationProvider.reset();
                Navigator.pop(context);
              },
              child: SizedBox(
                width: double.infinity,
                child: Text(
                  "Back to panels list",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      ); break;
      case PanelsConfigurationStatus.NotAuthorizedError: return Center(child: Text('Not Authorized')); break;
      case PanelsConfigurationStatus.InstallerCodeError: return Center(child: Text('Installer code error')); break;
      case PanelsConfigurationStatus.Loaded: return !panelsConfigurationProvider.panelConfiguration.isEmpty() ?
          getBaseView(panelsConfigurationProvider, context) : Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('There are no devices..'),
                Text('Press + to add new device..'),
                Padding(padding: EdgeInsetsDirectional.only(bottom: 20)),
                Ink(
                  decoration: const ShapeDecoration(
                    color: Colors.pink,
                    shape: CircleBorder(),
                  ),
                  child: IconButton(
                    icon: Icon(Icons.add),
                    color: Colors.white,
                    onPressed: () {
                      Navigator.pushNamed(context, '/addDevice');
                    },
                  ),
                )
              ],
            )
          );
      break;
      default: return Column();
    }
  }

  getBaseView(PanelsConfigurationProvider panelsConfigurationProvider, BuildContext context) {
    return panelsConfigurationProvider.listView ? getListView(panelsConfigurationProvider, context)
        : getGridView(panelsConfigurationProvider, context);
  }

  getListView(PanelsConfigurationProvider panelsConfigurationProvider, BuildContext context) {
    return CustomScrollView(
        slivers: <Widget>[
          SliverPadding(padding: const EdgeInsets.only(left: 10.0,right: 10.0,
              top: 10.0,bottom: 0.0),
            sliver: new SliverList(delegate:
            new SliverChildListDelegate([
              LedIndicationKeypadsWidget(ledIndicationKeypads: panelsConfigurationProvider.panelConfiguration.ledIndicationKeypads),
              InfraredMotionDetectorsWidget(infraredMotionDetectors: panelsConfigurationProvider.panelConfiguration.infraredMotionDetectors),
              MagnetContactsWidget(magnetContacts: panelsConfigurationProvider.panelConfiguration.magnetContacts),
              SmartPlugsWidget(smartPlugs: panelsConfigurationProvider.panelConfiguration.smartPlugs),
              IndoorSoundersWidget(indoorSounders: panelsConfigurationProvider.panelConfiguration.indoorSounders),
              SmokeDetectorsWidget(smokeDetectors: panelsConfigurationProvider.panelConfiguration.smokeDetectors),
              FloodDetectorsWidget(floodDetectors: panelsConfigurationProvider.panelConfiguration.floodDetectors),
              GlassBreakDetectorsWidget(glassBreakDetectors: panelsConfigurationProvider.panelConfiguration.glassBreakDetectors),
              VerificationCamerasWidget(verificationCameras: panelsConfigurationProvider.panelConfiguration.verificationCameras),
              AirQualitySensorsWidget(airQualitySensors: panelsConfigurationProvider.panelConfiguration.airQualitySensors),
              TemperatureSensorWidget(temperatureSensors: panelsConfigurationProvider.panelConfiguration.temperatureSensors),
              IndoorPirCamerasWidget(indoorPirCameras: panelsConfigurationProvider.panelConfiguration.indoorPirCameras)
            ]),
            ),
          )
        ]
    );
  }

  getGridView(PanelsConfigurationProvider panelsConfigurationProvider, BuildContext context) {
    List<BaseDevice> devices = panelsConfigurationProvider.panelConfiguration.getAllDevices();

    final widgets = List<Widget>.generate(
      devices.length,
          (i) => Column(
           children: [
             AspectRatio(
               aspectRatio: 1.0,
               child: Container(
                 decoration: BoxDecoration(
                   color: const Color(0xff7c94b6),
                   image: DecorationImage(
                     fit: BoxFit.fill,
                     image: AssetImage(devices[i].baseImageUrl),
                   ),
                 ),
               ),
             ),
             // Text(devices[i].baseTitle)
           ],
          )
    );

    return GridView.count(
      primary: false,
      padding: const EdgeInsets.all(20),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      crossAxisCount: 4,
      children: widgets
    );
  }

  deleteAllDevice(BuildContext context) async {
    bool toDelete = await showConfirmDialog(context);
    if (toDelete)  {
      Provider.of<PanelsConfigurationProvider>(context, listen: false).removeAllDevices();
    }
  }

  showConfirmDialog(BuildContext context) async {
    bool toDelete = false;
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed:  () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Yes"),
      onPressed:  () {
        Navigator.of(context).pop();
        toDelete = true;
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("Delete all devices"),
      content: Text("Are you sure you want to delete all devices?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
    return toDelete;
  }
}
