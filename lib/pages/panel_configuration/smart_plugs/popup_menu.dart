import 'package:crow_panel_quick_setup/models/devices/smart_plug/smart_plug.dart';
import 'package:crow_panel_quick_setup/providers/panel_configuration_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PopupMenu extends StatelessWidget {
  PopupMenu({
    Key key,
    this.smartPlug
  }) : super(key: key);

  final SmartPlug smartPlug;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      onSelected: (value) {
        if (value == 'delete') {
          PanelsConfigurationProvider panelsConfigurationProvider = Provider.of<PanelsConfigurationProvider>(context, listen: false);
          smartPlug.delete(panelsConfigurationProvider.panelConfiguration.rawConfig);
          panelsConfigurationProvider.savePanelConfiguration();
        }
      },
      itemBuilder: (context) => [
        PopupMenuItem(
            value: 'delete',
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                  child: Icon(Icons.close),
                ),
                Text('Delete magnet')
              ],
            )
        ),
      ]
    );
  }
}