import 'package:crow_panel_quick_setup/models/devices/smart_plug/smart_plug.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/smart_plugs/smart_plugs_item.dart';
import 'package:flutter/material.dart';

class SmartPlugsWidget extends StatelessWidget {
  SmartPlugsWidget({
    Key key,
    this.smartPlugs
  }) : super(key: key);

  final List<SmartPlug> smartPlugs;

  @override
  Widget build(BuildContext context) {
    return smartPlugs.length > 0 ? Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          child: Text(
            "Smart Plugs",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        ),
        Container(child: GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > 0) {
                print('settings');
              } else if (details.delta.dx < 0) {
                print('delete');
              }
            },
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: smartPlugs.length,
                itemBuilder: (BuildContext context, int index) {
                  return SmartPlugsItem(smartPlug: smartPlugs[index]);
                }
            ),
          )
        ),
        SizedBox(height: 20.0),
      ]
    ) : Column();
  }
}