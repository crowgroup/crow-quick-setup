import 'package:crow_panel_quick_setup/models/devices/led_indication_keypad/led_indication_keypad.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/led_indication_keypads/led_indication_keypad_item.dart';
import 'package:flutter/material.dart';

class LedIndicationKeypadsWidget extends StatelessWidget {
  LedIndicationKeypadsWidget({
    Key key,
    this.ledIndicationKeypads
  }) : super(key: key);

  final List<LedIndicationKeypad> ledIndicationKeypads;

  @override
  Widget build(BuildContext context) {
    return ledIndicationKeypads.length > 0 ? Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          child: Text(
            "Led Indication Keypads",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        ),
        Container(child: GestureDetector(
            onHorizontalDragUpdate: (details) {
              if (details.delta.dx > 0) {
                print('settings');
              } else if (details.delta.dx < 0) {
                print('delete');
              }
            },
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: ledIndicationKeypads.length,
                itemBuilder: (BuildContext context, int index) {
                  return LedIndicationKeypadItem(ledIndicationKeypad: ledIndicationKeypads[index]);
                }
            ),
          )
        ),
        SizedBox(height: 20.0),
      ]
    ) : Column();
  }
}