import 'package:crow_panel_quick_setup/models/devices/led_indication_keypad/led_indication_keypad.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/led_indication_keypads/popup_menu.dart';
import 'package:flutter/material.dart';

class LedIndicationKeypadItem extends StatelessWidget {
  LedIndicationKeypadItem({
    Key key,
    this.ledIndicationKeypad
  }) : super(key: key);

  final LedIndicationKeypad ledIndicationKeypad;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
      child: SizedBox(
        height: 100,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 1.0,
              child: Container(
                decoration: BoxDecoration(
                  color: const Color(0xff7c94b6),
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage(LedIndicationKeypad.imageUrl),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                  padding: const EdgeInsets.fromLTRB(10.0, 8.0, 2.0, 8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("ID: ${ledIndicationKeypad.slotId + 1}"),
                      Text("Serial number: ${ledIndicationKeypad.serialNumber}"),
                      ledIndicationKeypad.name.length > 0 ? Text("Name: ${ledIndicationKeypad.name}") : Column()
                    ],
                  )
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
              child:
              PopupMenu(ledIndicationKeypad: ledIndicationKeypad),
            ),
          ],
        ),
      )
    );
  }
}