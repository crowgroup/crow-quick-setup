import 'package:crow_panel_quick_setup/models/credentials.dart';
import 'package:crow_panel_quick_setup/models/token.dart';
import 'package:crow_panel_quick_setup/util/local_storage.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:crow_panel_quick_setup/providers/auth.dart';
import 'package:crow_panel_quick_setup/util/validators.dart';
import 'package:crow_panel_quick_setup/util/widgets.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final formKey = new GlobalKey<FormState>();

  String _username, _password;
  bool _rememberMe = false;
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initCredentials();
  }

  Future<void> initCredentials() async {
    Credentials credentials = await LocalStorage().getCredentials();
    if (credentials.email != null) {
      setState(() {
        _username = credentials.email;
        _emailController.text = _username;
        _password = credentials.password;
        _passwordController.text = _password;
        _rememberMe = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    AuthProvider auth = Provider.of<AuthProvider>(context);

    final usernameField = TextFormField(
      controller: _emailController,
      autofocus: false,
      validator: validateEmail,
      onSaved: (value) => _username = value,
      decoration: buildInputDecoration("Enter your email", Icons.email, Theme.of(context).accentColor),
    );

    final passwordField = TextFormField(
      controller: _passwordController,
      autofocus: false,
      obscureText: true,
      validator: (value) => value.isEmpty ? "Please enter password" : null,
      onSaved: (value) => _password = value,
      decoration: buildInputDecoration("Enter your password", Icons.lock, Theme.of(context).accentColor),
    );

    final rememberMeField = CheckboxListTile(
      checkColor: Colors.white,
      activeColor: Theme.of(context).accentColor,
      contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 4.0),
      title: Text("Remember me"),
      value: _rememberMe,
      onChanged: (newValue) {
        setState(() {
          _rememberMe = newValue;
        });
      },
      controlAffinity: ListTileControlAffinity.leading,
    );

    final loading = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CircularProgressIndicator(),
        Text(" Authenticating ... Please wait")
      ],
    );

    final forgotLabel = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
      ],
    );

    void doLogin () async {
      final form = formKey.currentState;

      if (form.validate()) {
        form.save();

        final Map<String, dynamic> response = await auth.login(_username, _password, _rememberMe);

        if (response['status']) {
          Token token = response['token'];
          await LocalStorage().saveToken(token);
          Navigator.pushReplacementNamed(context, '/panelsList');
        } else {
          Flushbar(
            title: "Failed Login",
            message: response['message'].toString(),
            duration: Duration(seconds: 3),
          ).show(context);
        }
      } else {
        print("form is invalid");
      }
    };

    return Scaffold(
      body: Center( child:
        SingleChildScrollView(
          child:
          Container(
            padding: EdgeInsets.all(40.0),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 15.0),
                  label("Email"),
                  SizedBox(height: 5.0),
                  usernameField,
                  SizedBox(height: 20.0),
                  label("Password"),
                  SizedBox(height: 5.0),
                  passwordField,
                  SizedBox(height: 20.0),
                  rememberMeField,
                  SizedBox(height: 20.0),
                  auth.loggedInStatus == AuthStatus.Authenticating
                      ? loading
                      : longButtons("Sign in", doLogin, color: Theme.of(context).accentColor),
                  SizedBox(height: 5.0),
                  forgotLabel
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
