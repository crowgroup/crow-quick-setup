import 'package:crow_panel_quick_setup/pages/panel_configuration/add_new_device/add_new_device.dart';
import 'package:crow_panel_quick_setup/pages/panel_configuration/panel_configuration.dart';
import 'package:crow_panel_quick_setup/pages/panels/add_new_panel/add_new_panel.dart';
import 'package:crow_panel_quick_setup/pages/panels/panels_list_wrapper.dart';
import 'package:crow_panel_quick_setup/providers/panel_configuration_provider.dart';
import 'package:crow_panel_quick_setup/util/local_storage.dart';
import 'package:flutter/material.dart';
import 'package:crow_panel_quick_setup/pages/login.dart';
import 'package:crow_panel_quick_setup/providers/auth.dart';
import 'package:provider/provider.dart';
import 'package:crow_panel_quick_setup/models/token.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Future<Token> getTokenData() => LocalStorage().getToken();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthProvider()),
        ChangeNotifierProvider(create: (_) => PanelsConfigurationProvider()),
      ],
      child: MaterialApp(
          title: 'CROW Quick Setup',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            brightness: Brightness.light,
            primaryColor: Colors.teal,
            accentColor: Colors.teal,
            primarySwatch: Colors.teal,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: FutureBuilder(
              future: getTokenData(),
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                    return CircularProgressIndicator();
                  case ConnectionState.waiting:
                    return CircularProgressIndicator();
                  default:
                    if (snapshot.hasError) {
                      return Text('Error: ${snapshot.error}');
                    } else if (snapshot.data?.accessToken == null) {
                      return Login();
                    } else {
                      return PanelsListWrapper();
                    }
                }
              }),
          routes: {
            '/addPanel': (context) => AddPanel(),
            '/addDevice': (context) => AddDevice(),
            '/panelsList': (context) => PanelsListWrapper(),
            '/panelConfiguration': (context) => PanelConfiguration(),
            '/login': (context) => Login(),
          }),
    );
  }
}