class AppUrls {
  // static const String liveBaseURL = "dev-api.crowcloud.xyz";
  static const String liveBaseURL = "api.crowcloud.xyz";
  static const String baseURL = liveBaseURL;
  static const String login = "https://$baseURL/o/token/";
  static const String panels = "/installer/panels/";
  static const String addPanel = "/panels/";
}
