import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RangeTextFormatter extends TextInputFormatter {
  final int min;
  final int max;

  RangeTextFormatter({this.min, this.max});

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.isEmpty) {
      return newValue.copyWith(text: '');
    } else if (newValue.text.compareTo(oldValue.text) != 0) {
      final int selectionIndexFromTheRight = newValue.text.length - newValue.selection.end;
      String newString = newValue.text;
      int value = int.tryParse(newString);
      value = value == null ? 0 : value;
      if (value < min) {
        newString = '1';
      } else if (value > max) {
        newString = '100';
      }
      return TextEditingValue(
        text: newString,
        selection: TextSelection.collapsed(offset: newString.length - selectionIndexFromTheRight),
      );
    } else {
      return TextEditingValue(
        text: newValue.text,
        selection: newValue.selection,
      );
    }
  }
}