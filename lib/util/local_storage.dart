import 'package:crow_panel_quick_setup/models/credentials.dart';
import 'package:crow_panel_quick_setup/models/token.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

class LocalStorage {
  Future<Token> saveToken(Token token) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("accessToken", token.accessToken);
    await prefs.setInt("expiresIn", token.expiresIn);
    await prefs.setString("refreshToken", token.refreshToken);
    await prefs.setString("scope", token.scope);
    await prefs.setString("tokenType", token.tokenType);
    return token;
  }

  Future<Token> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("accessToken");
    int expiresIn = prefs.getInt("expiresIn");
    String refreshToken = prefs.getString("refreshToken");
    String scope = prefs.getString("scope");
    String tokenType = prefs.getString("tokenType");
    return Token(
        accessToken: accessToken,
        expiresIn: expiresIn,
        refreshToken: refreshToken,
        scope: scope,
        tokenType: tokenType);
  }

  Future<void> removeToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove("accessToken");
    await prefs.remove("expiresIn");
    await prefs.remove("refreshToken");
    await prefs.remove("scope");
    await prefs.remove("tokenType");
  }

  Future<Credentials> saveCredentials(String email, String password) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("credsEmail", email);
    await prefs.setString("credsPassword", password);
    return Credentials(email: email, password: password);
  }

  Future<void> removeCredentials() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove("credsEmail");
    await prefs.remove("credsPassword");
  }

  Future<Credentials> getCredentials() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String email = prefs.getString("credsEmail");
    String password = prefs.getString("credsPassword");
    return Credentials(email: email, password: password);
  }
}
