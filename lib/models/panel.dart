import 'package:crow_panel_quick_setup/models/control_panel_user.dart';
import 'package:flutter/foundation.dart';

class Panel with ChangeNotifier {
  int id;
  String mac;
  String state;
  String version;
  String name;
  List<ControlPanelUser> controlPanelUsers;

  Panel({this.id, this.mac, this.state, this.version, this.name, this.controlPanelUsers});

  factory Panel.fromJson(Map<String, dynamic> responseData) {
    return Panel(
        id: responseData['id'],
        mac: responseData['mac'],
        state: responseData['state'],
        version: responseData['version'],
        name: responseData['name'],
        controlPanelUsers: responseData['control_panel_users'] != null ? List.from(responseData['control_panel_users'].map((dynamic model) {
          return ControlPanelUser.fromJson(model);
        })) : null,
    );
  }

  void updateMac(String newMac) {
    mac = newMac;
    notifyListeners();
  }

  get prettyName => () {
    if (name != null) return name;
    if (controlPanelUsers.length > 0) return controlPanelUsers[0].name;
    return 'undefined';
  }();

  get prettyMac => () {
    final splitSize = 2;
    RegExp exp = new RegExp(r"\w{"+"$splitSize"+"}");
    Iterable<Match> matches = exp.allMatches(mac);
    var list = matches.map((m) => m.group(0));
    return list.join(':').toUpperCase();
  }();
}
