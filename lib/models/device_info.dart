class DeviceInfo {
  String description;
  int deviceType; //device_type
  String imageUrl; //image_url
  String partNumber; //part_number
  String revision;
  String snPrefix; //sn_prefix
  String subtitle;
  String title;


  DeviceInfo({this.description, this.deviceType, this.imageUrl, this.partNumber, this.revision, this.snPrefix, this.subtitle, this.title});

  factory DeviceInfo.fromJson(Map<String, dynamic> responseData) {
    return DeviceInfo(
        description: responseData['description'],
        deviceType: responseData['device_type'],
        imageUrl: responseData['image_url'],
        partNumber: responseData['part_number'],
        revision: responseData['revision'],
        snPrefix: responseData['sn_prefix'],
        subtitle: responseData['subtitle'],
        title: responseData['title']
    );
  }
}
