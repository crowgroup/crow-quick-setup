import 'package:crow_panel_quick_setup/models/panel.dart';

class PanelsList {
  int count = 0;
  String next;
  String previous;
  List<Panel> results = [];

  PanelsList({this.count, this.next, this.previous, this.results});

  factory PanelsList.fromJson(Map<String, dynamic> responseData) {
    return PanelsList(
        count: responseData['count'],
        next: responseData['next'],
        previous: responseData['previous'],
        results: responseData['results'] != null ? List.from(responseData['results'].map((dynamic model) {
          return Panel.fromJson(model);
        })) : null,
    );
  }


  String getPaginationText(int page, pageSize) {
    if (results.length == 0) {
      return '';
    }
    int firstNumber = (page - 1) * pageSize + 1;
    int lastNumber = (page - 1) * pageSize + results.length;
    if (firstNumber == lastNumber) {
      return '$lastNumber of ${count}';
    } else {
      return '$firstNumber - $lastNumber of ${count}';
    }
  }
}
