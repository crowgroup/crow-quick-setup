class User {
  int pk;
  String firstName;
  String lastName;
  String email;

  User({this.pk, this.firstName, this.lastName, this.email});

  factory User.fromJson(Map<String, dynamic> responseData) {
    return User(
        pk: responseData['pk'],
        firstName: responseData['first_name'],
        lastName: responseData['last_name'],
        email: responseData['email']
    );
  }
}
