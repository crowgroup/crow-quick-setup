class ControlPanelUser {
  int id;
  String userCode;
  String name;

  ControlPanelUser({this.id, this.userCode, this.name});

  factory ControlPanelUser.fromJson(Map<String, dynamic> responseData) {
    return ControlPanelUser(
        id: responseData['id'],
        userCode: responseData['user_code'],
        name: responseData['name']
    );
  }
}
