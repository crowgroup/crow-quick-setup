class SmartPlugConfig {
  int outSupervision; //"out_supervision": 0, Supervision
  bool blockManualSwitch; //"block_manual_switch": false Disabled Block Manual Switch
  int powerUpOutputState; //"power_up_output_state": 0, 0 = Last output state,1 = state ON,2 = state OFF Power up output state

  SmartPlugConfig({this.outSupervision = 0, this.blockManualSwitch = false, this.powerUpOutputState = 0});

  factory SmartPlugConfig.fromJson(Map<String, dynamic> responseData) {
    return SmartPlugConfig(
      outSupervision: responseData['out_supervision'],
      blockManualSwitch: responseData['block_manual_switch'],
      powerUpOutputState: responseData['power_up_output_state'],
    );
  }

  Map<String, dynamic> toJson() =>
    {
      "out_supervision": outSupervision,
      "block_manual_switch": blockManualSwitch,
      "power_up_output_state": powerUpOutputState
    };
}