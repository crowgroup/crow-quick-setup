import 'package:crow_panel_quick_setup/models/devices/base_ouput.dart';
import 'package:crow_panel_quick_setup/models/devices/smart_plug/smart_plug_config.dart';

class SmartPlug extends BaseOutput {
  static final String title = 'Smart plug';
  static final String subtitle = 'SH-ACP';
  static final String imageUrl = 'images/sh-acp-center.jpg';
  static const int type = 87;
  SmartPlugConfig smartPlugConfig;

  SmartPlug({int slotId, int serialNumber, String name, this.smartPlugConfig}) :
      super(
        baseTitle: SmartPlug.title,
        baseImageUrl: SmartPlug.imageUrl,
        slotId: slotId,
        serialNumber: serialNumber,
        name: name
      );

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig) {
    return BaseOutput.getEmptySlotIndex(rawPanelConfig);
  }

  SmartPlug save(Map<String, dynamic> rawConfig) {
    super.baseSave(rawConfig, smartPlugConfig, type);
    return this;
  }

  SmartPlug delete(Map<String, dynamic> rawConfig) {
    super.delete(rawConfig);
    return this;
  }
}
