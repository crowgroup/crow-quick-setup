import 'package:crow_panel_quick_setup/models/devices/base_device.dart';
import 'package:crow_panel_quick_setup/models/devices/led_indication_keypad/led_indication_keypad_config.dart';

class LedIndicationKeypad extends BaseDevice {
  static final String title = 'Led Indication Keypad';
  static final String subtitle = 'SH-KP';
  static final String imageUrl = 'images/sh_kp_center.jpg';
  static const int type = 151;
  static final bool needName = true;

  LedIndicationKeypadConfig ledIndicationKeypadConfig;

  LedIndicationKeypad({int slotId, int serialNumber, String name, this.ledIndicationKeypadConfig}) :
      super(
        baseTitle: LedIndicationKeypad.title,
        baseImageUrl: LedIndicationKeypad.imageUrl,
        slotId: slotId,
        serialNumber: serialNumber,
        name: name
      );

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig) {
    int index = -1;
    for (int i = 0; i < 4; i++) {
      if (rawPanelConfig['keypad_ipud'][i]['device_id'] == 0) {
        return i;
      }
    }
    return index;
  }

  LedIndicationKeypad save(Map<String, dynamic> rawConfig) {
    if (rawConfig['keypad_name'] != null) {
      rawConfig['keypad_name'][slotId] = name;
    }
    rawConfig['alt_flags_keypad_link'][slotId] = "1";
    rawConfig['keypad_ipud'][slotId]['device_id'] = serialNumber;
    rawConfig['keypad_ipud'][slotId]['unit_id'] = slotId;
    rawConfig['radio_keypad_config_status'][slotId] = {
      "config": ledIndicationKeypadConfig,
      "type": type,
      "unit_type": null
    };
    return this;
  }

  LedIndicationKeypad delete(Map<String, dynamic> rawConfig) {
    if (rawConfig['keypad_name'] != null) {
      rawConfig['keypad_name'][slotId] = 'Keypad ${slotId + 1}';
    }
    // rawConfig['alt_flags_keypad_link'][slotId] = "0";
    rawConfig['keypad_ipud'][slotId]['device_id'] = 0;
    rawConfig['keypad_ipud'][slotId]['unit_id'] = 0;
    rawConfig['keypad_ipud'][slotId]['ipud'] = "0000000000";
    rawConfig['radio_keypad_config_status'][slotId] = {
      "config": {},
      "type": null,
      "unit_type": null
    };
    return this;
  }
}
