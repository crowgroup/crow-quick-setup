class LedIndicationKeypadConfig {
  int kpdLoud; //kpd_loud
  LedIndicationKeypadConfig({this.kpdLoud = 2});

  factory LedIndicationKeypadConfig.fromJson(Map<String, dynamic> responseData) {
    return LedIndicationKeypadConfig(
        kpdLoud: responseData['kpd_loud']
    );
  }

  Map<String, dynamic> toJson() =>
    {
      "kpd_loud": kpdLoud
    };
}