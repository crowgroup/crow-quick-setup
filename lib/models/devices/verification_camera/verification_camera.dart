import 'package:crow_panel_quick_setup/models/devices/base_zone.dart';
import 'package:crow_panel_quick_setup/models/devices/verification_camera/verification_camera_config.dart';

class VerificationCamera extends BaseZone {
  static final String title = 'Verification Camera';
  static final String subtitle = 'SH-CAM';
  static final String imageUrl = 'images/sh-cam.jpg';
  static const int type = 85;
  VerificationCameraConfig verificationCameraConfig;

  VerificationCamera({int slotId, int serialNumber, String name, this.verificationCameraConfig}) :
      super(
        baseTitle: VerificationCamera.title,
        baseImageUrl: VerificationCamera.imageUrl,
        slotId: slotId,
        serialNumber: serialNumber,
        name: name
      );

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig) {
    return BaseZone.getEmptySlotIndex(rawPanelConfig);
  }

  VerificationCamera save(Map<String, dynamic> rawConfig) {
    rawConfig['alt_flags_working_mode'][slotId] = 1;
    super.baseSave(rawConfig, verificationCameraConfig, type);
    return this;
  }

  VerificationCamera delete(Map<String, dynamic> rawConfig) {
    super.delete(rawConfig);
    return this;
  }
}
