import 'package:crow_panel_quick_setup/models/devices/base_zone.dart';
import 'package:crow_panel_quick_setup/models/devices/magnet_contact/magnet_contact_config.dart';

class MagnetContact extends BaseZone {
  static final String title = 'Magnet Contact';
  static final String subtitle = 'SH-MAG';
  static final String imageUrl = 'images/sh-mag-center.jpg';
  static const int type = 50;
  MagnetContactConfig magnetContactConfig;

  MagnetContact({int slotId, int serialNumber, String name, this.magnetContactConfig}) :
      super(
        baseTitle: MagnetContact.title,
        baseImageUrl: MagnetContact.imageUrl,
        slotId: slotId,
        serialNumber: serialNumber,
        name: name
      );

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig) {
    return BaseZone.getEmptySlotIndex(rawPanelConfig);
  }

  MagnetContact save(Map<String, dynamic> rawConfig) {
    super.baseSave(rawConfig, magnetContactConfig, type);
    return this;
  }

  MagnetContact delete(Map<String, dynamic> rawConfig) {
    super.delete(rawConfig);
    return this;
  }
}
