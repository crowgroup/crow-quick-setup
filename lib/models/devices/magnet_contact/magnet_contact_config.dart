class MagnetContactConfig {
  bool zoneLed; //"zone_led": true Enabled LED
  int supervision; //"supervision": 0, Supervision
  bool intSwitch; // int_switch true Enabled Internal Switch
  bool extSwitch; // ext_switch false Disabled External Switch
  bool switchAnd; // switch_and false = OR true = and Logic of Switches

  MagnetContactConfig({this.zoneLed = true, this.supervision = 0, this.intSwitch = true, this.extSwitch = false, this.switchAnd = false});

  factory MagnetContactConfig.fromJson(Map<String, dynamic> responseData) {
    return MagnetContactConfig(
      zoneLed: responseData['zone_led'],
      supervision: responseData['supervision'],
      intSwitch: responseData['int_switch'],
      extSwitch: responseData['ext_switch'],
      switchAnd: responseData['switch_and']
    );
  }

  Map<String, dynamic> toJson() =>
    {
      "zone_led": zoneLed,
      "supervision": supervision,
      "int_switch": intSwitch,
      "ext_switch": extSwitch,
      "switch_and": switchAnd
    };
}