class BaseDevice {
  final String baseTitle;
  final String baseImageUrl;
  final int slotId;
  final int serialNumber;
  String name;

  BaseDevice({this.baseTitle, this.baseImageUrl, this.slotId, this.serialNumber, this.name});
}