class SmokeDetectorConfig {
  int supervision;
  bool zoneLed;

  SmokeDetectorConfig({this.supervision = 0, this.zoneLed = false});

  factory SmokeDetectorConfig.fromJson(Map<String, dynamic> responseData) {
    return SmokeDetectorConfig(
      supervision: responseData['supervision'],
      zoneLed: responseData['zone_led']
    );
  }

  Map<String, dynamic> toJson() =>
      {
        "supervision": supervision,
        "zone_led": zoneLed
      };
}