import 'package:crow_panel_quick_setup/models/devices/base_zone.dart';
import 'package:crow_panel_quick_setup/models/devices/smoke_detector/smoke_detector_config.dart';

class SmokeDetector extends BaseZone {
  static final String title = 'Smoke Detector';
  static final String subtitle = 'FW2-SMOKE';
  static final String imageUrl = 'images/fw2-smk.jpg';
  static const int type = 52;
  SmokeDetectorConfig smokeDetectorConfig;

  SmokeDetector({int slotId, int serialNumber, String name, this.smokeDetectorConfig}) :
      super(
        baseTitle: SmokeDetector.title,
        baseImageUrl: SmokeDetector.imageUrl,
        slotId: slotId,
        serialNumber: serialNumber,
        name: name
      );

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig) {
    return BaseZone.getEmptySlotIndex(rawPanelConfig);
  }

  SmokeDetector save(Map<String, dynamic> rawConfig) {
    rawConfig['alt_flags_working_mode'][slotId] = 8;
    super.baseSave(rawConfig, smokeDetectorConfig, type);
    return this;
  }

  SmokeDetector delete(Map<String, dynamic> rawConfig) {
    super.delete(rawConfig);
    return this;
  }
}
