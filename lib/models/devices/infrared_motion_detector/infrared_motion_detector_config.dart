class InfraredMotionDetectorConfig {
  int gainLevel; // "gain_level": 2,
  int numPulses; //"num_pulses": 1,
  bool petImmuni; //"pet_immuni": false,
  int supervision; //"supervision": 0,
  bool zoneLed; //"zone_led": true

  InfraredMotionDetectorConfig({this.gainLevel = 2, this.numPulses = 1, this.petImmuni = false, this.supervision = 0, this.zoneLed = false});

  factory InfraredMotionDetectorConfig.fromJson(Map<String, dynamic> responseData) {
    return InfraredMotionDetectorConfig(
      gainLevel: responseData['gain_level'],
      numPulses: responseData['num_pulses'],
      petImmuni: responseData['pet_immuni'],
      supervision: responseData['supervision'],
      zoneLed: responseData['zone_led']
    );
  }

  Map<String, dynamic> toJson() =>
      {
        "gain_level": gainLevel,
        "num_pulses": numPulses,
        "pet_immuni": petImmuni,
        "supervision": supervision,
        "zone_led": zoneLed
      };
}