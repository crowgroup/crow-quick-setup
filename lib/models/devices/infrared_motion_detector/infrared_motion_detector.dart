import 'package:crow_panel_quick_setup/models/devices/base_zone.dart';
import 'package:crow_panel_quick_setup/models/devices/infrared_motion_detector/infrared_motion_detector_config.dart';

class InfraredMotionDetector extends BaseZone {
  static final String title = 'Infrared Motion Detector';
  static final String subtitle = 'SH-PIR';
  static final String imageUrl = 'images/sh-pir_in-center.jpg';
  static const int type = 49;
  InfraredMotionDetectorConfig infraredMotionDetectorConfig;

  InfraredMotionDetector({int slotId, int serialNumber, String name, this.infraredMotionDetectorConfig}) :
      super(
        baseTitle: InfraredMotionDetector.title,
        baseImageUrl: InfraredMotionDetector.imageUrl,
        slotId: slotId,
        serialNumber: serialNumber,
        name: name
      );

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig) {
    return BaseZone.getEmptySlotIndex(rawPanelConfig);
  }

  InfraredMotionDetector save(Map<String, dynamic> rawConfig) {
    super.baseSave(rawConfig, infraredMotionDetectorConfig, type);
    return this;
  }

  InfraredMotionDetector delete(Map<String, dynamic> rawConfig) {
    super.delete(rawConfig);
    return this;
  }
}
