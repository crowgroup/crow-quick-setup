class AirQualitySensorConfig {
  int supervision;

  AirQualitySensorConfig({this.supervision = 0});

  factory AirQualitySensorConfig.fromJson(Map<String, dynamic> responseData) {
    return AirQualitySensorConfig(
      supervision: responseData['supervision']
    );
  }

  Map<String, dynamic> toJson() =>
      {
        "supervision": supervision
      };
}