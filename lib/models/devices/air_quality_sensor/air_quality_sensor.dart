import 'package:crow_panel_quick_setup/models/devices/air_quality_sensor/air_quality_sensor_config.dart';
import 'package:crow_panel_quick_setup/models/devices/base_zone.dart';

class AirQualitySensor extends BaseZone {
  static final String title = 'Air Quality Sensor';
  static final String subtitle = 'SH-AQ';
  static final String imageUrl = 'images/sh-aq-center.jpg';
  static const int type = 61;
  AirQualitySensorConfig airQualitySensorConfig;

  AirQualitySensor({int slotId, int serialNumber, String name, this.airQualitySensorConfig}) :
      super(
        baseTitle: AirQualitySensor.title,
        baseImageUrl: AirQualitySensor.imageUrl,
        slotId: slotId,
        serialNumber: serialNumber,
        name: name
      );

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig) {
    return BaseZone.getEmptySlotIndex(rawPanelConfig);
  }

  AirQualitySensor save(Map<String, dynamic> rawConfig) {
    rawConfig['alt_flags_working_mode'][slotId] = 1;
    super.baseSave(rawConfig, airQualitySensorConfig, type);
    return this;
  }

  AirQualitySensor delete(Map<String, dynamic> rawConfig) {
    super.delete(rawConfig);
    return this;
  }
}
