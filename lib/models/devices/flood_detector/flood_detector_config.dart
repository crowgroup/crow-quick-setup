class FloodDetectorConfig {
  int supervision;
  bool zoneLed;

  FloodDetectorConfig({this.supervision = 0, this.zoneLed = false});

  factory FloodDetectorConfig.fromJson(Map<String, dynamic> responseData) {
    return FloodDetectorConfig(
      supervision: responseData['supervision'],
      zoneLed: responseData['zone_led']
    );
  }

  Map<String, dynamic> toJson() =>
      {
        "supervision": supervision,
        "zone_led": zoneLed
      };
}