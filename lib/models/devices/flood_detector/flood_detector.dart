import 'package:crow_panel_quick_setup/models/devices/base_zone.dart';
import 'package:crow_panel_quick_setup/models/devices/flood_detector/flood_detector_config.dart';

class FloodDetector extends BaseZone {
  static final String title = 'Flood Detector';
  static final String subtitle = 'SH-FLOOD';
  static final String imageUrl = 'images/sh-flood_front.jpg';
  static const int type = 56;
  FloodDetectorConfig floodDetectorConfig;

  FloodDetector({int slotId, int serialNumber, String name, this.floodDetectorConfig}) :
      super(
        baseTitle: FloodDetector.title,
        baseImageUrl: FloodDetector.imageUrl,
        slotId: slotId,
        serialNumber: serialNumber,
        name: name
      );

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig) {
    return BaseZone.getEmptySlotIndex(rawPanelConfig);
  }

  FloodDetector save(Map<String, dynamic> rawConfig) {
    rawConfig['alt_flags_working_mode'][slotId] = 16;
    super.baseSave(rawConfig, floodDetectorConfig, type);
    return this;
  }

  FloodDetector delete(Map<String, dynamic> rawConfig) {
    super.delete(rawConfig);
    return this;
  }
}
