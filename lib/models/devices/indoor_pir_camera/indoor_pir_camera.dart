import 'package:crow_panel_quick_setup/models/devices/base_zone.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_pir_camera/indoor_pir_camera_config.dart';

class IndoorPirCamera extends BaseZone {
  static final String title = 'Indoor Pir Camera';
  static final String subtitle = 'SH-PIRCAM-IN';
  static final String imageUrl = 'images/sh-pircam_in-center.jpg';
  static const int type = 83;
  IndoorPirCameraConfig indoorPirCameraConfig;

  IndoorPirCamera({int slotId, int serialNumber, String name, this.indoorPirCameraConfig}) :
      super(
        baseTitle: IndoorPirCamera.title,
        baseImageUrl: IndoorPirCamera.imageUrl,
        slotId: slotId,
        serialNumber: serialNumber,
        name: name
      );

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig) {
    return BaseZone.getEmptySlotIndex(rawPanelConfig);
  }

  IndoorPirCamera save(Map<String, dynamic> rawConfig) {
    super.baseSave(rawConfig, indoorPirCameraConfig, type);
    return this;
  }

  IndoorPirCamera delete(Map<String, dynamic> rawConfig) {
    super.delete(rawConfig);
    return this;
  }
}
