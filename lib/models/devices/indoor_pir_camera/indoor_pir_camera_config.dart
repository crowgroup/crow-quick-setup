class IndoorPirCameraConfig {
  bool camera;
  int confTime1p2p53;
  bool contrast;
  bool diffJpeg;
  int gainLevel;
  int holdOff;
  bool infraredLeds;
  int numPictur;
  int numPulses;
  bool petImmuni;
  int pictRate;
  int plsDirMode53;
  int quality;
  int resColor;
  bool sharpness;
  int time1p2p53;
  int time2p3p53;
  bool zoneLed;

  IndoorPirCameraConfig({
    this.camera = true,
    this.confTime1p2p53 = 1,
    this.contrast = true,
    this.diffJpeg = false,
    this.gainLevel = 2,
    this.holdOff = 4,
    this.infraredLeds = false,
    this.numPictur = 2,
    this.numPulses = 1,
    this.petImmuni = false,
    this.pictRate = 1,
    this.plsDirMode53 = 1,
    this.quality = 7,
    this.resColor = 3,
    this.sharpness = true,
    this.time1p2p53 = 1,
    this.time2p3p53 = 1,
    this.zoneLed = true
  });

  factory IndoorPirCameraConfig.fromJson(Map<String, dynamic> responseData) {
    return IndoorPirCameraConfig(
      camera: responseData['camera'],
      confTime1p2p53: responseData['conf_time_1p_2p_53'],
      contrast: responseData['contrast'],
      diffJpeg: responseData['diff_jpeg'],
      gainLevel: responseData['gain_level'],
      holdOff: responseData['hold_off'],
      infraredLeds: responseData['infrared_leds'],
      numPictur: responseData['num_pictur'],
      numPulses: responseData['num_pulses'],
      petImmuni: responseData['pet_immuni'],
      pictRate: responseData['pict_rate'],
      plsDirMode53: responseData['pls_dir_mode_53'],
      quality: responseData['quality'],
      resColor: responseData['res_color'],
      sharpness: responseData['sharpness'],
      time1p2p53: responseData['time_1p_2p_53'],
      time2p3p53: responseData['time_2p_3p_53'],
      zoneLed: responseData['zone_led']
    );
  }

  Map<String, dynamic> toJson() =>
      {
        "camera": camera,
        "conf_time_1p_2p_53": confTime1p2p53,
        "contrast": contrast,
        "diff_jpeg": diffJpeg,
        "gain_level": gainLevel,
        "hold_off": holdOff,
        "infrared_leds": infraredLeds,
        "num_pictur": numPictur,
        "num_pulses": numPulses,
        "pet_immuni": petImmuni,
        "pict_rate": pictRate,
        "pls_dir_mode_53": plsDirMode53,
        "quality": quality,
        "res_color": resColor,
        "sharpness": sharpness,
        "time_1p_2p_53": time1p2p53,
        "time_2p_3p_53": time2p3p53,
        "zone_led": zoneLed
      };
}