class TemperatureSensorConfig {
  int alertTime;
  int supervision;
  bool enableExternal;
  int thresholdTemperatureHigh;
  int thresholdTemperatureLow;
  int thresholdTemperatureNormal;

  TemperatureSensorConfig({this.alertTime = 20, this.supervision = 1, this.enableExternal = true, this.thresholdTemperatureHigh = 800, this.thresholdTemperatureLow = 250, this.thresholdTemperatureNormal = 700});

  factory TemperatureSensorConfig.fromJson(Map<String, dynamic> responseData) {
    return TemperatureSensorConfig(
      alertTime: responseData['alert_time'],
      supervision: responseData['supervision'],
      enableExternal: responseData['enable_external'],
      thresholdTemperatureHigh: responseData['threshold_temperature_high'],
      thresholdTemperatureLow: responseData['threshold_temperature_low'],
      thresholdTemperatureNormal: responseData['threshold_temperature_normal'],
    );
  }

  Map<String, dynamic> toJson() =>
      {
        "alert_time": alertTime,
        "supervision": supervision,
        "enable_external": enableExternal,
        "threshold_temperature_high": thresholdTemperatureHigh,
        "threshold_temperature_low": thresholdTemperatureLow,
        "threshold_temperature_normal": thresholdTemperatureNormal
      };
}