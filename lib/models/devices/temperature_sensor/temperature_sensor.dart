import 'package:crow_panel_quick_setup/models/devices/base_zone.dart';
import 'package:crow_panel_quick_setup/models/devices/temperature_sensor/temperature_sensor_config.dart';

class TemperatureSensor extends BaseZone {
  static final String title = 'Temperature Sensor';
  static final String subtitle = 'SH-Temp';
  static final String imageUrl = 'images/DECT-ULE-TMP-1.jpg';
  static const int type = 65;
  TemperatureSensorConfig temperatureSensorConfig;

  TemperatureSensor({int slotId, int serialNumber, String name, this.temperatureSensorConfig}) :
      super(
        baseTitle: TemperatureSensor.title,
        baseImageUrl: TemperatureSensor.imageUrl,
        slotId: slotId, serialNumber: serialNumber, name: name
      );

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig) {
    return BaseZone.getEmptySlotIndex(rawPanelConfig);
  }

  TemperatureSensor save(Map<String, dynamic> rawConfig) {
    rawConfig['alt_flags_working_mode'][slotId] = 4;
    super.baseSave(rawConfig, temperatureSensorConfig, type);
    return this;
  }

  TemperatureSensor delete(Map<String, dynamic> rawConfig) {
    super.delete(rawConfig);
    return this;
  }
}
