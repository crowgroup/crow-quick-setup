import 'package:crow_panel_quick_setup/models/devices/base_zone.dart';
import 'package:crow_panel_quick_setup/models/devices/glass_break_detector/glass_break_detector_config.dart';

class GlassBreakDetector extends BaseZone {
  static final String title = 'Glass Break Detector';
  static final String subtitle = 'SH-GBD';
  static final String imageUrl = 'images/sh-gbd-center.jpg';
  static const int type = 54;
  GlassBreakDetectorConfig glassBreakDetectorConfig;

  GlassBreakDetector({int slotId, int serialNumber, String name, this.glassBreakDetectorConfig}) :
      super(
        baseTitle: GlassBreakDetector.title,
        baseImageUrl: GlassBreakDetector.imageUrl,
        slotId: slotId,
        serialNumber: serialNumber,
        name: name
      );

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig) {
    return BaseZone.getEmptySlotIndex(rawPanelConfig);
  }

  GlassBreakDetector save(Map<String, dynamic> rawConfig) {
    rawConfig['alt_flags_working_mode'][slotId] = 4;
    super.baseSave(rawConfig, glassBreakDetectorConfig, type);
    return this;
  }

  GlassBreakDetector delete(Map<String, dynamic> rawConfig) {
    super.delete(rawConfig);
    return this;
  }
}
