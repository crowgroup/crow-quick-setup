class GlassBreakDetectorConfig {
  int supervision;
  bool zoneLed;

  GlassBreakDetectorConfig({this.supervision = 0, this.zoneLed = false});

  factory GlassBreakDetectorConfig.fromJson(Map<String, dynamic> responseData) {
    return GlassBreakDetectorConfig(
      supervision: responseData['supervision'],
      zoneLed: responseData['zone_led']
    );
  }

  Map<String, dynamic> toJson() =>
      {
        "supervision": supervision,
        "zone_led": zoneLed
      };
}