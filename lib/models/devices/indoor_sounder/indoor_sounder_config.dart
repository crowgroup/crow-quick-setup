class IndoorSounderConfig {
  int sndrLoud;
  bool zoneLed;

  IndoorSounderConfig({this.sndrLoud = 3, this.zoneLed = true});

  factory IndoorSounderConfig.fromJson(Map<String, dynamic> responseData) {
    return IndoorSounderConfig(
      sndrLoud: responseData['sndr_loud'],
      zoneLed: responseData['zone_led'],
    );
  }

  Map<String, dynamic> toJson() =>
    {
      "sndr_loud": sndrLoud,
      "zone_led": zoneLed
    };
}
