import 'package:crow_panel_quick_setup/models/devices/base_ouput.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_sounder/indoor_sounder_config.dart';

class IndoorSounder extends BaseOutput {
  static final String title = 'Wireless Indoor Sounder';
  static final String subtitle = 'SH-SIRINT';
  static final String imageUrl = 'images/sh-sirint-center.jpg';
  static const int type = 69;
  IndoorSounderConfig indoorSounderConfig;

  IndoorSounder({int slotId, int serialNumber, String name, this.indoorSounderConfig}) :
      super(
        baseTitle: IndoorSounder.title,
        baseImageUrl: IndoorSounder.imageUrl,
        slotId: slotId,
        serialNumber: serialNumber,
        name: name
      );

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig) {
    return BaseOutput.getEmptySlotIndex(rawPanelConfig);
  }

  IndoorSounder save(Map<String, dynamic> rawConfig) {
    for (int i = 0; i < 64; i++) {
      rawConfig['zone_alarm_assigned_to_output']['$slotId'][i] = true;
      rawConfig['zone_stay_alarm_assigned_to_output']['$slotId'][i] = true;
      rawConfig['zone_24_hour_alarm_assigned_to_output']['$slotId'][i] = true;
    }
    super.baseSave(rawConfig, indoorSounderConfig, type);
    return this;
  }

  IndoorSounder delete(Map<String, dynamic> rawConfig) {
    super.delete(rawConfig);
    return this;
  }
}
