import 'package:crow_panel_quick_setup/models/devices/base_device.dart';

class BaseOutput extends BaseDevice {

  BaseOutput({String baseTitle, String baseImageUrl, int slotId, int serialNumber, String name}) :
        super(baseTitle: baseTitle, baseImageUrl: baseImageUrl, slotId: slotId, serialNumber: serialNumber, name: name);

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig, {int minIndex = 0, int maxIndex = 16}) {
    int index = -1;
    for (int i = minIndex; i < maxIndex; i++) {
      if (rawPanelConfig['output_ipud'][i]['device_id'] == 0) {
        return i;
      }
    }
    return index;
  }

  BaseOutput baseSave(Map<String, dynamic> rawConfig, dynamic outputConfig, int type) {
    rawConfig['output_name'][slotId] = name;
    rawConfig['alt_flags_output_link'][slotId] = 1;
    rawConfig['output_ipud'][slotId]['device_id'] = serialNumber;
    rawConfig['output_ipud'][slotId]['unit_id'] = slotId;
    rawConfig['radio_output_config_status'][slotId] = {
      "config": outputConfig,
      "type": type,
      "unit_type": null
    };
    return this;
  }

  BaseOutput delete(Map<String, dynamic> rawConfig) {
    rawConfig['output_name'][slotId] = 'Output ${slotId + 1}';
    // rawConfig['alt_flags_output_link'][slotId] = 1;
    rawConfig['output_ipud'][slotId]['device_id'] = 0;
    rawConfig['output_ipud'][slotId]['unit_id'] = 0;
    rawConfig['output_ipud'][slotId]['ipud'] = "0000000000";
    rawConfig['radio_output_config_status'][slotId] = {
      "config": {},
      "type": null,
      "unit_type": null
    };
    return this;
  }
}