import 'package:crow_panel_quick_setup/models/devices/base_device.dart';

class BaseZone extends BaseDevice {

  BaseZone({String baseTitle, String baseImageUrl, int slotId, int serialNumber, String name}) :
        super(baseTitle: baseTitle, baseImageUrl: baseImageUrl, slotId: slotId, serialNumber: serialNumber, name: name);

  static int getEmptySlotIndex(Map<String, dynamic> rawPanelConfig, {int minIndex = 0, int maxIndex = 32}) {
    int index = -1;
    for (int i = minIndex; i < maxIndex; i++) {
      if (rawPanelConfig['zone_ipud'][i]['device_id'] == 0) {
        return i;
      }
    }
    return index;
  }

  BaseZone baseSave(Map<String, dynamic> rawConfig, dynamic zoneConfig, int type) {
    if (rawConfig['zone_name'] != null) {
      rawConfig['zone_name'][slotId] = name;
    }
    rawConfig['alt_flags_zone_link'][slotId] = 1;
    rawConfig['zone_ipud'][slotId]['device_id'] = serialNumber;
    rawConfig['zone_ipud'][slotId]['unit_id'] = slotId;
    rawConfig['radio_zone_config_status'][slotId] = {
      "config": zoneConfig,
      "type": type,
      "unit_type": null
    };
    rawConfig['zone_is_active'][slotId] = true;
    return this;
  }

  BaseZone delete(Map<String, dynamic> rawConfig) {
    if (rawConfig['zone_name'] != null) {
      rawConfig['zone_name'][slotId] = 'Zone ${slotId + 1}';
    }
    // rawConfig['alt_flags_zone_link'][slotId] = 1;
    rawConfig['zone_ipud'][slotId]['device_id'] = 0;
    rawConfig['zone_ipud'][slotId]['unit_id'] = 0;
    rawConfig['zone_ipud'][slotId]['ipud'] = "0000000000";
    rawConfig['radio_zone_config_status'][slotId] = {
      "config": {},
      "type": null,
      "unit_type": null
    };
    rawConfig['zone_is_active'][slotId] = false;
    return this;
  }
}