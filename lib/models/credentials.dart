class Credentials {
  String email;
  String password;

  Credentials({this.email, this.password});

  factory Credentials.fromJson(Map<String, dynamic> responseData) {
    return Credentials(
        email: responseData['email'],
        password: responseData['password']
    );
  }
}
