import 'package:crow_panel_quick_setup/models/devices/air_quality_sensor/air_quality_sensor.dart';
import 'package:crow_panel_quick_setup/models/devices/air_quality_sensor/air_quality_sensor_config.dart';
import 'package:crow_panel_quick_setup/models/devices/base_device.dart';
import 'package:crow_panel_quick_setup/models/devices/flood_detector/flood_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/flood_detector/flood_detector_config.dart';
import 'package:crow_panel_quick_setup/models/devices/glass_break_detector/glass_break_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/glass_break_detector/glass_break_detector_config.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_pir_camera/indoor_pir_camera.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_pir_camera/indoor_pir_camera_config.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_sounder/indoor_sounder.dart';
import 'package:crow_panel_quick_setup/models/devices/indoor_sounder/indoor_sounder_config.dart';
import 'package:crow_panel_quick_setup/models/devices/infrared_motion_detector/infrared_motion_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/infrared_motion_detector/infrared_motion_detector_config.dart';
import 'package:crow_panel_quick_setup/models/devices/led_indication_keypad/led_indication_keypad.dart';
import 'package:crow_panel_quick_setup/models/devices/led_indication_keypad/led_indication_keypad_config.dart';
import 'package:crow_panel_quick_setup/models/devices/magnet_contact/magnet_contact.dart';
import 'package:crow_panel_quick_setup/models/devices/magnet_contact/magnet_contact_config.dart';
import 'package:crow_panel_quick_setup/models/devices/smart_plug/smart_plug.dart';
import 'package:crow_panel_quick_setup/models/devices/smart_plug/smart_plug_config.dart';
import 'package:crow_panel_quick_setup/models/devices/smoke_detector/smoke_detector.dart';
import 'package:crow_panel_quick_setup/models/devices/smoke_detector/smoke_detector_config.dart';
import 'package:crow_panel_quick_setup/models/devices/temperature_sensor/temperature_sensor.dart';
import 'package:crow_panel_quick_setup/models/devices/temperature_sensor/temperature_sensor_config.dart';
import 'package:crow_panel_quick_setup/models/devices/verification_camera/verification_camera.dart';
import 'package:crow_panel_quick_setup/models/devices/verification_camera/verification_camera_config.dart';
import 'package:flutter/material.dart';

class PanelConfiguration with ChangeNotifier {
  int panelId;
  Map<String, dynamic> rawConfig;
  List<LedIndicationKeypad> ledIndicationKeypads = [];
  List<InfraredMotionDetector> infraredMotionDetectors = [];
  List<MagnetContact> magnetContacts = [];
  List<SmartPlug> smartPlugs = [];
  List<IndoorSounder> indoorSounders = [];
  List<SmokeDetector> smokeDetectors = [];
  List<FloodDetector> floodDetectors = [];
  List<GlassBreakDetector> glassBreakDetectors = [];
  List<VerificationCamera> verificationCameras = [];
  List<AirQualitySensor> airQualitySensors = [];
  List<TemperatureSensor> temperatureSensors = [];
  List<IndoorPirCamera> indoorPirCameras = [];

  PanelConfiguration({this.panelId, this.rawConfig,
    this.ledIndicationKeypads,
    this.infraredMotionDetectors,
    this.magnetContacts,
    this.smartPlugs,
    this.indoorSounders,
    this.smokeDetectors,
    this.floodDetectors,
    this.glassBreakDetectors,
    this.verificationCameras,
    this.airQualitySensors,
    this.temperatureSensors,
    this.indoorPirCameras
  });

  factory PanelConfiguration.fromJson(int panelId, Map<String, dynamic> responseData) {
    return PanelConfiguration(
      panelId: panelId,
      rawConfig: responseData,
      ledIndicationKeypads: _getPaneLedIndicationKeypads(responseData),
      infraredMotionDetectors: _getPanelInfraredMotionDetectors(responseData),
      magnetContacts: _getPanelMagnetContacts(responseData),
      smartPlugs: _getPanelSmartPlugs(responseData),
      indoorSounders: _getPanelIndoorSounders(responseData),
      smokeDetectors: _getPanelSmokeDetectors(responseData),
      floodDetectors: _getPanelFloodDetectors(responseData),
      glassBreakDetectors: _getGlassBreakDetectors(responseData),
      verificationCameras: _getVerificationCameras(responseData),
      airQualitySensors: _getAirQualitySensors(responseData),
      temperatureSensors: _getPanelTemperatureSensors(responseData),
      indoorPirCameras: _getPanelIndoorPirCameras(responseData),
    );
  }

  bool isEmpty() {
    return ledIndicationKeypads.isEmpty &&
    infraredMotionDetectors.isEmpty &&
    magnetContacts.isEmpty &&
    smartPlugs.isEmpty &&
    indoorSounders.isEmpty &&
    smokeDetectors.isEmpty &&
    floodDetectors.isEmpty &&
    glassBreakDetectors.isEmpty &&
    verificationCameras.isEmpty &&
    airQualitySensors.isEmpty &&
    temperatureSensors.isEmpty &&
    indoorPirCameras.isEmpty;
  }

  List<BaseDevice> getAllDevices() {
    List<BaseDevice> devices = [];
    devices.addAll(ledIndicationKeypads);
    devices.addAll(infraredMotionDetectors);
    devices.addAll(magnetContacts);
    devices.addAll(smartPlugs);
    devices.addAll(smokeDetectors);
    devices.addAll(floodDetectors);
    devices.addAll(glassBreakDetectors);
    devices.addAll(verificationCameras);
    devices.addAll(airQualitySensors);
    devices.addAll(temperatureSensors);
    devices.addAll(indoorPirCameras);
    return devices;
  }

  void removeAllDevices(rawConfig) {
    ledIndicationKeypads.forEach((element) {
      element.delete(rawConfig);
    });
    infraredMotionDetectors.forEach((element) {
      element.delete(rawConfig);
    });
    magnetContacts.forEach((element) {
      element.delete(rawConfig);
    });
    smartPlugs.forEach((element) {
      element.delete(rawConfig);
    });
    indoorSounders.forEach((element) {
      element.delete(rawConfig);
    });
    smokeDetectors.forEach((element) {
      element.delete(rawConfig);
    });
    floodDetectors.forEach((element) {
      element.delete(rawConfig);
    });
    glassBreakDetectors.forEach((element) {
      element.delete(rawConfig);
    });
    verificationCameras.forEach((element) {
      element.delete(rawConfig);
    });
    airQualitySensors.forEach((element) {
      element.delete(rawConfig);
    });
    temperatureSensors.forEach((element) {
      element.delete(rawConfig);
    });
    indoorPirCameras.forEach((element) {
      element.delete(rawConfig);
    });
  }

  static List<LedIndicationKeypad> _getPaneLedIndicationKeypads(rawConfig) {
    List<LedIndicationKeypad> ledIndicationKeypads = [];
    for (int i = 0; i < 4; i++) {
      if (rawConfig['keypad_ipud'][i]['device_id'] != 0) {
        ledIndicationKeypads.add(LedIndicationKeypad(
          slotId: i,
          serialNumber: rawConfig['keypad_ipud'][i]['device_id'],
          name: rawConfig['keypad_name'] != null ? rawConfig['keypad_name'][i] : '',
          ledIndicationKeypadConfig: LedIndicationKeypadConfig.fromJson(rawConfig['radio_keypad_config_status'][i]['config'])
        ));
      }
    }
    return ledIndicationKeypads;
  }

  static List<InfraredMotionDetector> _getPanelInfraredMotionDetectors(rawConfig) {
    List<InfraredMotionDetector> infraredMotionDetectors = [];
    for (int i = 0; i < 64; i++) {
      if (rawConfig['zone_ipud'][i]['device_id'] != 0 && rawConfig['radio_zone_config_status'][i]['type'] == InfraredMotionDetector.type) {
        infraredMotionDetectors.add(InfraredMotionDetector(
            slotId: i,
            serialNumber: rawConfig['zone_ipud'][i]['device_id'],
            name: rawConfig['zone_name'][i],
            infraredMotionDetectorConfig: InfraredMotionDetectorConfig.fromJson(rawConfig['radio_zone_config_status'][i]['config']))
        );
      }
    }
    return infraredMotionDetectors;
  }

  static List<SmokeDetector> _getPanelSmokeDetectors(rawConfig) {
    List<SmokeDetector> smokeDetectors = [];
    for (int i = 0; i < 64; i++) {
      if (rawConfig['zone_ipud'][i]['device_id'] != 0 && rawConfig['radio_zone_config_status'][i]['type'] == SmokeDetector.type) {
        smokeDetectors.add(SmokeDetector(
            slotId: i,
            serialNumber: rawConfig['zone_ipud'][i]['device_id'],
            name: rawConfig['zone_name'][i],
            smokeDetectorConfig: SmokeDetectorConfig.fromJson(rawConfig['radio_zone_config_status'][i]['config']))
        );
      }
    }
    return smokeDetectors;
  }

  static List<FloodDetector> _getPanelFloodDetectors(rawConfig) {
    List<FloodDetector> floodDetectors = [];
    for (int i = 0; i < 64; i++) {
      if (rawConfig['zone_ipud'][i]['device_id'] != 0 && rawConfig['radio_zone_config_status'][i]['type'] == FloodDetector.type) {
        floodDetectors.add(FloodDetector(
            slotId: i,
            serialNumber: rawConfig['zone_ipud'][i]['device_id'],
            name: rawConfig['zone_name'][i],
            floodDetectorConfig: FloodDetectorConfig.fromJson(rawConfig['radio_zone_config_status'][i]['config']))
        );
      }
    }
    return floodDetectors;
  }

  static List<GlassBreakDetector> _getGlassBreakDetectors(rawConfig) {
    List<GlassBreakDetector> glassBreakDetectors = [];
    for (int i = 0; i < 64; i++) {
      if (rawConfig['zone_ipud'][i]['device_id'] != 0 && rawConfig['radio_zone_config_status'][i]['type'] == GlassBreakDetector.type) {
        glassBreakDetectors.add(GlassBreakDetector(
            slotId: i,
            serialNumber: rawConfig['zone_ipud'][i]['device_id'],
            name: rawConfig['zone_name'][i],
            glassBreakDetectorConfig: GlassBreakDetectorConfig.fromJson(rawConfig['radio_zone_config_status'][i]['config']))
        );
      }
    }
    return glassBreakDetectors;
  }

  static List<VerificationCamera> _getVerificationCameras(rawConfig) {
    List<VerificationCamera> verificationCameras = [];
    for (int i = 0; i < 64; i++) {
      if (rawConfig['zone_ipud'][i]['device_id'] != 0 && rawConfig['radio_zone_config_status'][i]['type'] == VerificationCamera.type) {
        verificationCameras.add(VerificationCamera(
            slotId: i,
            serialNumber: rawConfig['zone_ipud'][i]['device_id'],
            name: rawConfig['zone_name'][i],
            verificationCameraConfig: VerificationCameraConfig.fromJson(rawConfig['radio_zone_config_status'][i]['config']))
        );
      }
    }
    return verificationCameras;
  }

  static List<AirQualitySensor> _getAirQualitySensors(rawConfig) {
    List<AirQualitySensor> airQualitySensors = [];
    for (int i = 0; i < 64; i++) {
      if (rawConfig['zone_ipud'][i]['device_id'] != 0 && rawConfig['radio_zone_config_status'][i]['type'] == AirQualitySensor.type) {
        airQualitySensors.add(AirQualitySensor(
            slotId: i,
            serialNumber: rawConfig['zone_ipud'][i]['device_id'],
            name: rawConfig['zone_name'][i],
            airQualitySensorConfig: AirQualitySensorConfig.fromJson(rawConfig['radio_zone_config_status'][i]['config']))
        );
      }
    }
    return airQualitySensors;
  }

  static List<TemperatureSensor> _getPanelTemperatureSensors(rawConfig) {
    List<TemperatureSensor> temperatureSensors = [];
    for (int i = 0; i < 64; i++) {
      if (rawConfig['zone_ipud'][i]['device_id'] != 0 && rawConfig['radio_zone_config_status'][i]['type'] == TemperatureSensor.type) {
        temperatureSensors.add(TemperatureSensor(
            slotId: i,
            serialNumber: rawConfig['zone_ipud'][i]['device_id'],
            name: rawConfig['zone_name'][i],
            temperatureSensorConfig: TemperatureSensorConfig.fromJson(rawConfig['radio_zone_config_status'][i]['config']))
        );
      }
    }
    return temperatureSensors;
  }

  static List<IndoorPirCamera> _getPanelIndoorPirCameras(rawConfig) {
    List<IndoorPirCamera> indoorPirCameras = [];
    for (int i = 0; i < 64; i++) {
      if (rawConfig['zone_ipud'][i]['device_id'] != 0 && rawConfig['radio_zone_config_status'][i]['type'] == IndoorPirCamera.type) {
        indoorPirCameras.add(IndoorPirCamera(
            slotId: i,
            serialNumber: rawConfig['zone_ipud'][i]['device_id'],
            name: rawConfig['zone_name'][i],
            indoorPirCameraConfig: IndoorPirCameraConfig.fromJson(rawConfig['radio_zone_config_status'][i]['config']))
        );
      }
    }
    return indoorPirCameras;
  }

  static List<MagnetContact> _getPanelMagnetContacts(rawConfig) {
    List<MagnetContact> magnetContacts = [];
    for (int i = 0; i < 64; i++) {
      if (rawConfig['zone_ipud'][i]['device_id'] != 0 && rawConfig['radio_zone_config_status'][i]['type'] == MagnetContact.type) {
        magnetContacts.add(MagnetContact(
            slotId: i,
            serialNumber: rawConfig['zone_ipud'][i]['device_id'],
            name: rawConfig['zone_name'][i],
            magnetContactConfig: MagnetContactConfig.fromJson(rawConfig['radio_zone_config_status'][i]['config']))
        );
      }
    }
    return magnetContacts;
  }

  static List<SmartPlug> _getPanelSmartPlugs(rawConfig) {
    List<SmartPlug> smartPlugs = [];
    for (int i = 0; i < 16; i++) {
      if (rawConfig['output_ipud'][i]['device_id'] != 0 && rawConfig['radio_output_config_status'][i]['type'] == SmartPlug.type) {
        smartPlugs.add(SmartPlug(
            slotId: i,
            serialNumber: rawConfig['output_ipud'][i]['device_id'],
            name: rawConfig['output_name'][i],
            smartPlugConfig: SmartPlugConfig.fromJson(rawConfig['radio_output_config_status'][i]['config']))
        );
      }
    }
    return smartPlugs;
  }

  static List<IndoorSounder> _getPanelIndoorSounders(rawConfig) {
    List<IndoorSounder> indoorSounders = [];
    for (int i = 0; i < 16; i++) {
      if (rawConfig['output_ipud'][i]['device_id'] != 0 && rawConfig['radio_output_config_status'][i]['type'] == IndoorSounder.type) {
        indoorSounders.add(IndoorSounder(
            slotId: i,
            serialNumber: rawConfig['output_ipud'][i]['device_id'],
            name: rawConfig['output_name'][i],
            indoorSounderConfig: IndoorSounderConfig.fromJson(rawConfig['radio_output_config_status'][i]['config']))
        );
      }
    }
    return indoorSounders;
  }
}
