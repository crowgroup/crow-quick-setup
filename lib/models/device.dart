enum DeviceType {keypad, zone, output}

class Device {
  String name;
  String imageUrl;
  DeviceType deviceType;
  int deviceId;

  Device({this.name, this.imageUrl, this.deviceType, this.deviceId});
}
