import 'package:flutter/foundation.dart';

class PanelPicture with ChangeNotifier {
  String created;
  int id;
  String key;
  String panelTime;
  int picIndex;
  int picTotal;
  String url;
  int zone;
  String zoneName;

  PanelPicture({this.created, this.id, this.key, this.panelTime, this.picIndex, this.picTotal, this.url, this.zone, this.zoneName});

  factory PanelPicture.fromJson(Map<String, dynamic> responseData) {
    return PanelPicture(
      created: responseData['created'],
      id: responseData['id'],
      key: responseData['key'],
      panelTime: responseData['panel_time'],
      picIndex: responseData['pic_index'],
      picTotal: responseData['pic_total'],
      url: responseData['url'],
      zone: responseData['zone'],
      zoneName: responseData['zone_name'],
    );
  }
}
