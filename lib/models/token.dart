class Token {
  String accessToken;
  int expiresIn;
  String refreshToken;
  String scope;
  String tokenType;

  Token({this.accessToken, this.expiresIn, this.refreshToken, this.scope, this.tokenType});

  factory Token.fromJson(Map<String, dynamic> responseData) {
    return Token(
        accessToken: responseData['access_token'],
        expiresIn: responseData['expires_in'],
        refreshToken: responseData['refresh_token'],
        scope: responseData['scope'],
        tokenType: responseData['token_type']
    );
  }
}
